import time
import django_otp
from django_otp.plugins.otp_static.models import StaticDevice
from django.urls import reverse
from django.views.generic import FormView
from social_django.utils import load_strategy
from two_factor.forms import AuthenticationTokenForm
from two_factor.plugins.phonenumber.utils import backup_phones
from two_factor.utils import default_device
from two_factor.views import LoginView


class LoginView(LoginView):
    template_name = "two_factor/core/login.html"
    form = AuthenticationTokenForm

    def get(self, request, *args, **kwargs):
        partial = self.get_partial()
        user = partial.kwargs['user']
        user.backend = 'electeez_auth.backends.SiteModelBackend'

        self.storage.reset()
        self.storage.authenticated_user = user
        self.storage.data["authentication_time"] = int(time.time())
        self.storage.current_step = 'token'

        return self.render(self.get_form())

    def get_success_url(self):
        partial = self.get_partial()
        self.request.session["tfa_completed"] = True
        self.request.user = self.get_partial().kwargs["user"]
        django_otp.login(self.request, self.device)
        return (
            reverse("social:complete", kwargs={"backend": partial.backend})
            + f"?partial_token={partial.token}"
        )

    def get_partial(self):
        strategy = load_strategy()
        partial_token = self.request.GET.get("partial_token")
        partial = strategy.partial_load(partial_token)
        return partial

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        user = self.get_partial().kwargs["user"]
        kwargs["user"] = user
        self.device = default_device(user)
        kwargs["initial_device"] = self.device
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user = self.storage.authenticated_user

        if 'device' not in context:
            context['device'] = default_device(user)
        context['other_devices'] = [
            phone for phone in backup_phones(user)
            if phone != context['device']]
        try:
            context['backup_tokens'] = user.staticdevice_set\
                .get(name='backup').token_set.count()
        except StaticDevice.DoesNotExist:
            context['backup_tokens'] = 0

        return context
