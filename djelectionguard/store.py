import pickle

from pathlib import Path
from django.conf import settings
from django.apps import apps
from django.core.files.base import ContentFile


class FileStore:
    def __init__(self, contest):
        path = f'{settings.MEDIA_ROOT}/stores/{contest.pk}'
        self._path = Path(path)
        self._path.mkdir(parents=True, exist_ok=True)
        self._items = None

    def __len__(self):
        if self._items is None:
            return len(list(self._path.iterdir()))
        return len(self._get_items())

    def __iter__(self):
        return iter(self.items())

    def all(self):
        return self._get_items().values()

    def keys(self):
        return self._get_items().keys()

    def items(self):
        return self._get_items().items()

    def get(self, key):
        if self._items is None:
            path = self._path / f'{key}.pkl'
            return self._get_value_from_path(path)

        return self._items.get(key, None)

    def _set_items(self):
        if self._items is None:
            self._items = dict()
            for f in self._path.iterdir():
                value = self._get_value_from_path(f)
                key = str(f).rsplit('.', 1)[0]
                self._items[key] = value

    def _get_items(self):
        self._set_items()
        return self._items

    def set(self, key, value):
        (self._path / f'{key}.pkl').write_bytes(pickle.dumps(value))
        self._get_items()[key] = value

    def pop(self, key):
        value = None
        if key in self._get_items():
            value = self._items.pop(key)
            file_p = self._path / f'{key}.pkl'
            file_p.unlink()

        return value

    def _get_value_from_path(self, path):
        if path.is_file():
            return pickle.loads(path.read_bytes())
        return None


class SubmittedBallotStore(FileStore):
    def __init__(self, path):
        super().__init__(path)


class DBBallotStore:
    def __init__(self, contest):
        self.store_class = apps.get_model('djelectionguard', 'Store')
        self.ballot_class = apps.get_model('djelectionguard', 'Ballot')

        self.db_store, created = self.store_class.objects.get_or_create(
            contest_id=contest.id
        )

    def __len__(self):
        return self.all().count()

    def __iter__(self):
        ballots = list(self.all())
        for i, b in enumerate(ballots):
            yield i, b

    def all(self):
        pkl_ballots = self.db_store.ballot_set.all()
        ballots = [pickle.loads(b.ballot.read()) for b in pkl_ballots]
        return ballots

    def keys(self):
        return self.db_store.ballot_set.values_list('pk', flat=True)

    def items(self):
        ballot_dict = {b.object_id: b for b in self.all()}

        return ballot_dict.items()

    def get(self, key):
        ballot = self.db_store.ballot_set.filter(pk=key).first()
        if ballot:
            return pickle.loads(ballot.ballot.read())

    def set(self, key, value):
        bobj = self.ballot_class(pk=key, store=self.db_store)
        bobj.ballot.save(str(key), ContentFile(pickle.dumps(value)))
        bobj.save()

    def pop(self, key):
        if bobj := self.db_store.ballot_set.filter(pk=key).first():
            value = pickle.loads(bobj.ballot.read())
            self.db_store.ballot_set.filter(pk=key).delete()

        return value
