import json

from electionguard import *


class CiphertextBallotDeserializer:
    def decode(self, json_ballot_str):
        json_ballot = json.loads(json_ballot_str)
        return CiphertextBallot(
            object_id=parse_raw_or_data(json_ballot['object_id']),
            style_id=parse_raw_or_data(json_ballot['style_id']),
            manifest_hash=parse_element_mod_q(json_ballot['manifest_hash']),
            code_seed=parse_element_mod_q(json_ballot['code_seed']),
            contests=parse_contest_list(json_ballot['contests']),
            nonce=parse_element_mod_q(json_ballot['nonce']),
            timestamp=parse_raw_or_data(json_ballot['timestamp']),
            crypto_hash=parse_element_mod_q(json_ballot['crypto_hash']),
            code=parse_element_mod_q(json_ballot['code'])
        )


def parse_raw_or_data(elem):
    if isinstance(elem, dict):
        return 'data' in elem and elem['data']
    return elem


def parse_element_mod_q(json_elem):
    return ElementModQ(parse_raw_or_data(json_elem))


def parse_element_mod_p(json_elem):
    return ElementModP(parse_raw_or_data(json_elem))


def parse_constant_chaum_pedersen_proof(json_elem):
    return ConstantChaumPedersenProof(
        pad=parse_element_mod_p(json_elem['pad']),
        data=parse_element_mod_p(json_elem['data']),
        challenge=parse_element_mod_q(json_elem['challenge']),
        response=parse_element_mod_q(json_elem['response']),
        constant=int(parse_raw_or_data(json_elem['constant']))
    )


def parse_disjunctive_chaum_pedersen_proof(json_elem):
    return DisjunctiveChaumPedersenProof(
        proof_zero_pad=parse_element_mod_p(json_elem['proof_zero_pad']),
        proof_zero_data=parse_element_mod_p(json_elem['proof_zero_data']),
        proof_one_pad=parse_element_mod_p(json_elem['proof_one_pad']),
        proof_one_data=parse_element_mod_p(json_elem['proof_one_data']),
        proof_zero_challenge=parse_element_mod_q(
            json_elem['proof_zero_challenge']),
        proof_one_challenge=parse_element_mod_q(
            json_elem['proof_one_challenge']),
        challenge=parse_element_mod_q(json_elem['challenge']),
        proof_zero_response=parse_element_mod_q(
            json_elem['proof_zero_response']),
        proof_one_response=parse_element_mod_q(json_elem['proof_one_response'])
    )


def parse_elgamal_ciphertext(json_elem):
    if json_elem:
        return ElGamalCiphertext(
            pad=parse_element_mod_p(json_elem['pad']),
            data=parse_element_mod_p(json_elem['data'])
        )


def parse_placeholder_selection(json_elem):
    value = parse_raw_or_data(json_elem)
    return True if value == "01" else False


def parse_selection_list(json_list):
    return [
        CiphertextBallotSelection(
            object_id=parse_raw_or_data(json_elem['object_id']),
            sequence_order=int(parse_raw_or_data(json_elem['sequence_order'])),
            description_hash=parse_element_mod_q(
                json_elem['description_hash']),
            ciphertext=parse_elgamal_ciphertext(json_elem['ciphertext']),
            crypto_hash=parse_element_mod_q(json_elem['crypto_hash']),
            is_placeholder_selection=parse_placeholder_selection(
                json_elem['is_placeholder_selection']),
            nonce=parse_element_mod_q(json_elem['nonce']),
            proof=parse_disjunctive_chaum_pedersen_proof(json_elem['proof']),
            extended_data=parse_elgamal_ciphertext(
                json_elem.get('extended_data', None))
        ) for json_elem in json_list
    ]


def parse_contest_list(json_list):
    return [
        CiphertextBallotContest(
            object_id=parse_raw_or_data(json_elem['object_id']),
            sequence_order=int(parse_raw_or_data(
                json_elem['sequence_order'])),
            description_hash=parse_element_mod_q(
                json_elem['description_hash']),
            ballot_selections=parse_selection_list(
                json_elem['ballot_selections']),
            ciphertext_accumulation=parse_elgamal_ciphertext(
                json_elem['ciphertext_accumulation']),
            crypto_hash=parse_element_mod_q(json_elem['crypto_hash']),
            nonce=parse_element_mod_q(json_elem['nonce']),
            proof=parse_constant_chaum_pedersen_proof(json_elem['proof']),
        ) for json_elem in json_list
    ]
