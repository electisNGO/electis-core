from django.db.models import Q, F, Count
from django.conf import settings

from electeez_sites.models import Site

from .models import (
    Contest,
    Guardian,
    Scrutator,
    Candidate,
    Voter
)


class PermissionMixin:
    permission_chain = []

    def default_qs(self):
        return self.model.objects.all()

    def get_queryset(self):
        qs = self.default_qs()
        for permission in self.permission_chain:
            print(permission)
            qs = permission(self, qs)

        return qs


class ContestPermissionMixin(PermissionMixin):
    model = Contest

    def default_qs(self):
        return self.model.objects.filter(archived=False)


class GuardianPermissionMixin(PermissionMixin):
    model = Guardian


class CandidatePermissionMixin(PermissionMixin):
    model = Candidate


class VoterPermissionMixin(PermissionMixin):
    model = Voter


def user_can_see_contest(view, qs=None):
    site = Site.objects.get_current()

    qs = qs.filter(Q(site=site) | Q(site=None))

    qs = qs.filter(
        (Q(voter__user=view.request.user) & ~Q(actual_start=None))
        | (Q(scrutator__user=view.request.user) & ~Q(joint_public_key=None))
        | (Q(guardian__user=view.request.user) & ~Q(joint_public_key=None))
        | Q(mediator=view.request.user)
    ).select_related('mediator').order_by('-start', 'name').distinct()

    if settings.MANAGED_CONTESTS:
        return contest_is_opened(view, qs)
    return qs


def user_is_guardian_of_contest(view, qs=None):
    return qs.filter(guardian__user=view.request.user).exclude(joint_public_key=None)


def user_is_voter_for_contest(view, qs=None):
    if view.request.user.is_superuser:
        return qs
    return qs.filter(voter__user=view.request.user)


def user_is_mediator_of_contest(view, qs=None):
    return qs.filter(mediator=view.request.user)


def user_is_scrutator_of_contest(view, qs=None):
    return qs.filter(scrutator__user=view.request.user)


def contest_is_not_opened_yet(view, qs=None):
    return qs.filter(actual_start=None)


def contest_is_opened(view, qs=None):
    return qs.exclude(actual_start=None)


def contest_is_closed(view, qs=None):
    return qs.exclude(actual_end=None)


def contest_is_not_closed(view, qs=None):
    return qs.filter(actual_end=None)


def contest_key_ceremony_not_started(view, qs=None):
    return qs.filter(joint_public_key=None)


def contest_key_ceremony_started(view, qs=None):
    return qs.exclude(joint_public_key=None)


def contest_key_ceremony_complete(view, qs=None):
    return qs.exclude(joint_public_key=None)


def quorum_guardian_have_verified_key(view, qs=None):
    return qs.annotate(
        Count('guardian__verified')
    ).filter(
        quorum__lte=F('guardian__verified__count')
    )


def quorum_guardian_have_uploaded_key(view, qs=None):
    return qs.annotate(
        Count('guardian__uploaded')
    ).filter(
        quorum__lte=F('guardian__uploaded__count')
    )


def contest_is_not_decrypting(view, qs=None):
    return qs.exclude(decrypting=True)


def contest_is_not_decrypted(view, qs=None):
    return qs.filter(plaintext_tally=None)


def contest_is_decrypted(view, qs=None):
    return qs.exclude(plaintext_tally=None)


def contract_is_valid(view, qs=None):
    return qs.filter(electioncontract__is_valid=True)


def contest_contract_created(view, qs=None):
    return qs.exclude(electioncontract=None)


def user_is_guardian(view, qs=None):
    return qs.filter(user=view.request.user)


def user_is_guardian_or_mediator(view, qs=None):
    return qs.filter(
        Q(mediator=view.request.user)
        | (Q(guardian__user=view.request.user) & ~Q(joint_public_key=None))
        | (Q(scrutator__user=view.request.user) & ~Q(joint_public_key=None))
    ).distinct()


def guardian_key_ceremony_started(view, qs=None):
    return qs.exclude(contest__joint_public_key=None)


def guardian_contest_not_opened_yet(view, qs=None):
    return qs.filter(contest__actual_start=None)


def guardian_contest_is_closed(view, qs=None):
    return qs.exclude(contest__actual_end=None)


def guardian_key_not_erased(view, qs=None):
    return qs.filter(erased=None)


def guardian_key_downloaded(view, qs=None):
    return qs.exclude(downloaded=None)


def guardian_key_not_verified(view, qs=None):
    return qs.filter(verified=None)


def guardian_key_not_uploaded(view, qs=None):
    return qs.filter(uploaded=None)


def guardian_contest_is_not_decrypting(view, qs=None):
    return qs.exclude(contest__decrypting=True)


def guardian_contest_is_not_decrypted(view, qs=None):
    return qs.filter(contest__plaintext_tally=None)


def candidate_user_is_mediator(view, qs=None):
    return qs.filter(contest__mediator=view.request.user)


def candidate_contest_not_opened(view, qs=None):
    return qs.filter(contest__actual_start=None)


def voter_is_user(view, qs=None):
    return qs.filter(user=view.request.user)


def voter_has_casted(view, qs=None):
    return qs.exclude(casted_at=None)
