from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

from electeez_common.components import *
from djlang.utils import gettext as _

from .models import Contest


class Separator(Li):
    def __init__(self, inset=False):
        cls = 'mdc-deprecated-list-divider'
        if inset:
            cls += ' mdc-deprecated-list-divider--inset'
        super().__init__(role='separator', cls=cls)


class ListItem(CList):
    def __init__(self, component, separator=True):
        content = [component]
        if separator:
            content.append(Separator())
        super().__init__(*content)


class ListActionItem(Li):
    def __init__(self, title, txt, icon_comp, btn_comp, **kwargs):
        self.action_btn = btn_comp

        subitem_cls = 'mdc-deprecated-list-item__primary-text list-action-row'
        if not txt:
            subitem_cls = 'list-action-row'
        subitem = Span(cls=subitem_cls)

        if icon_comp:
            subitem.addchild(icon_comp)
        subitem.addchild(H5(title))
        if btn_comp:
            subitem.addchild(btn_comp)

        item = Span(subitem, cls='mdc-deprecated-list-item__text list-action-column')
        if txt:
            item.addchild(
                Span(
                    txt,
                    cls='mdc-deprecated-list-item__secondary-text'
                        ' list-action-text body-2'
                )
            )

        super().__init__(item, cls='mdc-deprecated-list-item list-action-item')


class ListAction(ListItem):
    def __init__(self, *args, **kwargs):
        super().__init__(ListActionItem(*args), **kwargs)


class CircleIcon(Span):
    def __init__(self, icon, color='', small=False, **kw):
        base_cls = f'icon {icon} {"small " if small else ""}'
        super().__init__(
            cls=base_cls + color,
            **kw
        )


class TodoIcon(CircleIcon):
    def __init__(self, **kw):
        super().__init__('empty-icon', 'yellow', **kw)


class DoneIcon(CircleIcon):
    def __init__(self):
        super().__init__('check-icon', 'green')


class TezosIcon(CircleIcon):
    def __init__(self, **kw):
        super().__init__('tezos-icon', 'white', **kw)


class OnGoingIcon(CircleIcon):
    def __init__(self):
        super().__init__('ongoing-icon', 'yellow')


class EmailIcon(CircleIcon):
    def __init__(self):
        super().__init__('email-icon', 'yellow')


class SimpleCheckIcon(CircleIcon):
    def __init__(self):
        super().__init__('simple-check-icon', 'green')


class WorldIcon(CircleIcon):
    def __init__(self):
        super().__init__('world-icon', 'black', small=True)


class CheckedIcon(MDCIcon):
    def __init__(self):
        super().__init__('check_circle', cls='material-icons icon green2')


class DownloadBtnMixin:

    async def get_file(event):
        event.preventDefault()
        elem = event.currentTarget
        response = await fetch(elem.href)
        if response.status == 200:
            file = await response.blob()
            url = URL.createObjectURL(file)
            link = document.createElement('a')
            link.download = elem.dataset.filename
            link.href = url
            link.click()
            URL.revokeObjectURL(url)
        else:
            if elem.dataset.errMsg:
                msg = elem.dataset.errMsg
            else:
                msg = 'Server error'
            alert(msg)

    def py2js(self):
        elem = getElementByUuid(self.id)
        elem.onclick = self.get_file


class DownloadBtnOutlined(DownloadBtnMixin, MDCButtonOutlined):
    pass


class DownloadBtn(DownloadBtnMixin, MDCTextButton):
    pass


class ChooseBlockchainAction(ListAction):
    def __init__(self, **context):
        user = context['user']
        contest = context['contest']
        num_voters = context['num_voter']
        num_candidates = context['num_candidate']
        separator = (
            contest.publish_state != contest.PublishStates.ELECTION_NOT_DECENTRALIZED
            and num_voters
            and num_candidates > contest.votes_allowed
        )
        if contest.publish_state != contest.PublishStates.ELECTION_NOT_DECENTRALIZED:
            txt = ''
            icon = DoneIcon()
        else:
            txt = _('Choose the blockchain you want to deploy'
                    ' your election smart contract to')
            icon = TodoIcon()

        try:
            has_contract = contest.electioncontract is not None
        except Contest.electioncontract.RelatedObjectDoesNotExist:
            has_contract = False

        super().__init__(
            _('Add the election smart contract'),
            txt, icon,
            MDCButtonOutlined(
                _('add'),
                icon='add',
                tag='a',
                p=False,
                href=reverse('electioncontract_create', args=[contest.id])
            ) if not has_contract else None,
            separator=separator
        )


class Section(Div):
    pass


class TezosSecuredCard(Section):
    def __init__(self, contest, user):
        link = None
        blockchain = None
        decentralized = contest.PublishStates.ELECTION_NOT_DECENTRALIZED
        if contest.publish_state != decentralized:
            try:
                contract = contest.electioncontract
                if contract.contract_address:
                    blockchain = contract.blockchain
                    link = A(
                        contract.contract_address,
                        href=getattr(contract, 'explorer_link', ''),
                        target='_blank',
                        style=dict(
                            text_overflow='ellipsis',
                            overflow='hidden',
                            width='100%'
                        )
                    )
                else:
                    link = None
            except ObjectDoesNotExist:
                pass  # no contract

        def step(s):
            return Span(
                Span(s, style='width: 100%'),
                link,
                style='display: flex; flex-flow: column wrap'
            )

        super().__init__(
            Ul(
                ListAction(
                    _('Secured and decentralised with Tezos'),
                    Span(
                        _(
                            'Your election data and results will be published'
                            ' on Tezos’ %(blockchain)s blockchain.',
                            blockchain=blockchain
                        ),
                        PublishProgressBar([
                            step(_('Election contract created')),
                            step(_('Election opened')),
                            step(_('Election closed')),
                            step(_('Election Results available')),
                            step(_('Election contract updated')),
                        ], contest.publish_state - 1),
                    ) if contest.publish_state else None,
                    TezosIcon(),
                    None,
                    separator=False
                ),
                cls='mdc-deprecated-list action-list',
            ),
            cls='setting-section', style='background-color: aliceblue;'
        )


class ClipboardCopy(MDCTextButton):
    def onclick(target):
        target.previousElementSibling.select()
        document.execCommand('copy')


class PaginationRowsSelect(Div):
    cls = 'mdc-data-table__pagination-rows-per-page'

    class HTMLElement:
        def connectedCallback(self):
            this.addEventListener('MDCSelect:change', this.change.bind(this))

        def change(self, event):
            urlparams = new.URLSearchParams(location.search)
            urlparams.set('psize', event.detail.value)
            window.location.search = urlparams.toString()


class PaginationNavigation(Div):
    def __init__(self, *content, **context):
        super().__init__(*content, cls='mdc-data-table__pagination-navigation')


class NavigationButton(Button):
    sass = '''
    .mdc-icon-button[disabled]
        cursor: default
        pointer-events: none
        color: rgba(0, 0, 0, 0.38)
    '''

    class HTMLElement:
        def connectedCallback(self):
            this.addEventListener('click', this.click.bind(this))

        def click(self, event):
            urlparams = new.URLSearchParams(location.search)
            urlparams.set('p', event.currentTarget.dataset.target)
            window.location.search = urlparams.toString()


class Pagination(Div):
    def __init__(self):
        self.values = [
            dict(index=1, label='100', value=100),
            dict(index=2, label='250', value=250),
            dict(index=3, label='500', value=500),
        ]
        self.row_per_page = PaginationRowsSelect()
        self.navigation = PaginationNavigation()
        self.total = Div(cls='mdc-data-table__pagination-total')
        super().__init__(
            Div(
                self.row_per_page,
                self.total,
                self.navigation,
                cls='mdc-data-table__pagination-trailing'
            ),
            cls='mdc-data-table__pagination',
            style=dict(padding_top='12px')
        )

    def set_parameters(self, opts):
        if opts['psize'] <= 100:
            self.values[0]['selected'] = True
            selected = 100
        elif opts['psize'] <= 250:
            self.values[1]['selected'] = True
            selected = 250
        else:
            self.values[2]['selected'] = True
            selected = 500

        self.row_per_page.addchild(
            MDCSelectOutlined(
                label=_('Rows per page'),
                name='psize',
                value=[selected],
                selected=selected,
                template_name='',
                optgroups=[
                    [None, self.values, 0]
                ],
                addcls='mdc-data-table__pagination-rows-per-page-select'
            )
        )

        is_last_page = opts['last_page'] == opts['p']
        total = opts['total']
        start = opts['start']
        size = opts['psize']
        end = start + (total - opts['p'] * size if is_last_page else size)
        self.total.addchild(
            _('%(start)s-%(end)s of %(total)s',
                start=start + 1, end=end, total=total),
        )
        first_page_btn = NavigationButton(
            Div('first_page', cls='mdc-button__icon'),
            data_first_page='true',
            data_target=0,
            cls='mdc-icon-button material-icons'
                ' mdc-data-table__pagination-button'
        )
        prev_page_btn = NavigationButton(
            Div('chevron_left', cls='mdc-button__icon'),
            data_prev_page='true',
            data_target=opts['p'] - 1,
            cls='mdc-icon-button material-icons'
                ' mdc-data-table__pagination-button'
        )
        last_page_btn = NavigationButton(
            Div('last_page', cls='mdc-button__icon'),
            data_last_page='true',
            data_target=opts['last_page'],
            cls='mdc-icon-button material-icons'
                ' mdc-data-table__pagination-button'
        )
        next_page_btn = NavigationButton(
            Div('chevron_right', cls='mdc-button__icon'),
            data_next_page='true',
            data_target=opts['p'] + 1,
            cls='mdc-icon-button material-icons'
                ' mdc-data-table__pagination-button'
        )
        if opts['p'] == 0:
            first_page_btn.attrs['disabled'] = True
            prev_page_btn.attrs['disabled'] = True
        if is_last_page:
            last_page_btn.attrs['disabled'] = True
            next_page_btn.attrs['disabled'] = True

        self.navigation.addchildren([
            first_page_btn,
            prev_page_btn,
            next_page_btn,
            last_page_btn
        ])


class PublishProgressBar(Div):
    def __init__(self, _steps, step=0):
        self.nsteps = len(_steps)
        self.step = step
        steps = [
            Span(
                cls='progress-step progress-step--disabled',
                **{'data-step': s})
            for s in range(0, self.nsteps)
        ]
        if 0 <= step < self.nsteps:
            steps[step].attrs['class'] += ' progress-step--active'

        super().__init__(
            MDCLinearProgress(),
            Div(
                *steps,
                cls='progress-bar__steps'
            ),
            Span(_steps[step], cls='center-text overline', id='progress-label'),
            aria_labelledby='progress-label',
            aria_hidden='true',
            cls='progress-bar',
            style='margin: 24px auto'
        )

    def set_progress(current_step, total_steps):
        bar_container = document.querySelector('.progress-bar')
        bar = bar_container.querySelector('.mdc-linear-progress')

        mdcbar = new.mdc.linearProgress.MDCLinearProgress(bar)
        bar.MDCLinearProgress = mdcbar

        def step(step):
            progress = step / (total_steps - 1)

            steps = bar_container.querySelectorAll('.progress-step')
            for n in range(total_steps):
                s = steps.item(n)
                if s.dataset.step > step:
                    s.classList.remove('progress-step--active')
                    s.classList.add('progress-step--disabled')
                elif s.dataset.step == step:
                    s.classList.remove('progress-step--disabled')
                    s.classList.add('progress-step--active')
                else:
                    s.classList.remove('progress-step--active')
                    s.classList.remove('progress-step--disabled')

            bar.MDCLinearProgress.foundation.setProgress(progress)

        bar.setStep = step
        bar.setStep(current_step)

    def py2js(self):
        self.set_progress(self.step, self.nsteps)
