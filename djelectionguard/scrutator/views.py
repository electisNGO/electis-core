import codecs
import io

from django import http
from django.conf import settings
from django.contrib import messages
from django.views import generic
from django.views.generic.edit import FormMixin
from django.utils import timezone
from django.urls import reverse

from ryzom_django_channels.views import ReactiveMixin, register

from djelectionguard.views import Enforce2FAMixin
from djlang.utils import gettext as _

from ..permissions import *
from .forms import (
    ScrutatorCreateForm,
)


class ScrutatorListView(
    Enforce2FAMixin,
    ReactiveMixin,
    ContestPermissionMixin,
    generic.DetailView
):
    template_name = 'scrutator_list'
    permission_chain = [
        user_can_see_contest
    ]


class ScrutatorCreateView(
    Enforce2FAMixin,
    ContestPermissionMixin,
    FormMixin,
    generic.DetailView
):
    template_name = 'scrutator_create'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_not_opened_yet,
    ]
    form_class = ScrutatorCreateForm

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_form(self):
        contest = self.get_object()
        self.contest = contest
        form = super().get_form()
        form.instance = Scrutator(contest=contest)
        return form

    def form_valid(self, form):
        form.save()
        messages.success(
            self.request,
            _('You have invited %(obj)s as scrutator', obj=form.instance),
        )
        self.form = form
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('contest_scrutator_create', args=[self.contest.id])


class ScrutatorDeleteView(Enforce2FAMixin, generic.DeleteView):
    pk_url_kwarg = 'gpk'

    def get_queryset(self):
        return Scrutator.objects.filter(
            contest__mediator=self.request.user,
            contest__actual_start=None,
        )

    def dispatch(self, request, *args, **kwargs):
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)
        return self.delete(request, *args, **kwargs)

    def get_success_url(self):
        contest = self.get_object().contest
        return reverse('contest_scrutator_create', args=(contest.id,))
