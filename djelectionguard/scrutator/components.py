from django.db.models import Case, When, Value
from django.utils import timezone

from ryzom_django_channels.components import (
    ReactiveComponentMixin,
    SubscribeComponentMixin,
    model_template
)

from djlang.utils import gettext as _

from electeez_auth.models import Token

from djelectionguard.components import *


class ScrutatorDeleteBtn(A):
    def __init__(self, scrutator):
        self.scrutator = scrutator

        super().__init__(
            MDCIcon(
                'delete',
                cls='material-icons'),
            tag='a',
            href=reverse(
                'contest_scrutator_delete',
                args=[scrutator.contest.id, scrutator.id]
            )
        )


class ScrutatorListComp(MDCList):
    tag = 'scrutator-list'

    def __init__(self, contest, editable=False):
        scrutators = contest.scrutator_set.select_related('user').all()
        super().__init__(
            *(
                MDCListItem(scrutator.user)
                for scrutator in scrutators
            ) if scrutators.count()
            else [_('No scrutator yet.')],
            role='list'
        )


class ScrutatorsSettingsCard(Div):
    def __init__(self, view, **context):
        contest = view.object
        editable = not contest.joint_public_key
        kwargs = dict(p=False, tag='a')
        if editable:
            kwargs['href'] = reverse(
                'contest_scrutator_create',
                args=[contest.id]
            )
            btn = MDCButtonOutlined(_('view all/edit'), **kwargs)
        else:
            kwargs['href'] = reverse(
                'contest_scrutator_list',
                args=[contest.id]
            )
            btn = MDCButtonOutlined(_('view all'), **kwargs)

        super().__init__(
            H5(_('Scrutators')),
            ScrutatorListComp(contest, editable),
            btn,
            cls='setting-section'
        )


@template('scrutator_list', Document, Card)
class ScrutatorListCard(Div):
    def to_html(self, *content, view, **context):
        contest = context['contest']
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )

        return super().to_html(
            Script(src=Static('ryzom.js')),
            Script(mark_safe(view.get_token())),
            H4(
                _('Scrutators'),
                cls='center-text',
                style=dict(
                    margin_bottom='12px'
                )
            ),
            MDCList(
                *(
                    MDCListItem(
                        scrutator.user,
                        cls='mdc-list-item--activated' if scrutator.user.is_active else ''
                    )
                    for scrutator in contest.scrutator_set.all()
                ) if contest.scrutator_set.count()
                else [MDCListItem(_('No scrutator yet.'))],
                role='list'
            ),
            cls='card',
            view=view
        )


@template('scrutator_create', Document, Card)
class ScrutatorCreateCard(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )
        table_head_row = Tr(cls='mdc-data-table__header-row')
        for th in (_('Scrutators'), ''):
            table_head_row.addchild(
                Th(
                    th,
                    role='columnheader',
                    scope='col',
                    cls='mdc-data-table__header-cell overline',
                )
            )

        table_content = Tbody(cls='mdc-data-table__content')
        cls = 'mdc-data-table__cell'
        for scrutator in contest.scrutator_set.all():
            activated = scrutator.user and scrutator.user.is_active
            table_content.addchild(Tr(
                Td(scrutator.user, cls=cls),
                Td(
                    ScrutatorDeleteBtn(scrutator),
                    cls=cls,
                    style='text-align:right'),
                cls='mdc-data-table__row',
                style='opacity: 0.5;' if not activated else '',
            ))

        table = Table(
            Thead(table_head_row),
            table_content,
            **{
                'class': 'mdc-data-table__table',
                'aria-label': _('Voters')
            }
        )

        return super().to_html(
            H4(_('Add scrutators'), cls='center-text'),
            Div(
                _(
                    'Scrutators are responsible for locking and unlocking'
                    ' of the ballot box with their private keys.'
                ),
                cls='center-text body-1'
            ),
            Div(
                B(_('GUARDIAN_HELP_TEXT')),
                cls='red-section'),
            Form(
                Div(
                    form['email'],
                    cls='checkbox-field'
                ),
                CSRFInput(view.request),
                MDCButtonOutlined(
                    _('add scrutator'),
                    p=False,
                    icon='person_add'
                ),
                table,
                method='POST',
                cls='form'
            ),
            cls='card'
        )
