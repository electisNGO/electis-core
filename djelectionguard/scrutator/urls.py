from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import (
    ScrutatorListView,
    ScrutatorCreateView,
    ScrutatorDeleteView,
)


urlpatterns = [
    path(
        '',
        login_required(ScrutatorListView.as_view()),
        name='contest_scrutator_list'
    ),
    path(
        'create/',
        login_required(ScrutatorCreateView.as_view()),
        name='contest_scrutator_create'
    ),
    path(
        '<uuid:gpk>/delete/',
        login_required(ScrutatorDeleteView.as_view()),
        name='contest_scrutator_delete'
    ),
]
