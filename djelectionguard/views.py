from django import http
from django.urls import reverse
from django.contrib import messages

from electeez_sites.models import Site

from djlang.utils import gettext as _


class Enforce2FAMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.need_verification(request):
            messages.info(
                request,
                _('You have to setup two factor authentication to access this page')
            )
            return http.HttpResponseRedirect(
                reverse('two_factor:setup')
            )
        return super().dispatch(request, *args, **kwargs)

    def need_verification(self, request):
        site = Site.objects.get_current()
        return site.enforce_2fa and not request.user.is_verified()
