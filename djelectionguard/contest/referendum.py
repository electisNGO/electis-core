import pdfkit
from django.templatetags.static import static
from django.conf import settings
from django.http import HttpRequest
import django

def get_static_file_url(file_path):
    request = HttpRequest()
    request.META['SERVER_NAME'] = 'localhost'
    request.META['SERVER_PORT'] = '8000'
    return request.build_absolute_uri(static(file_path))

def contest_to_company_data(contest):
    from electeez_sites.models import CompanyInformations
    from textwrap import wrap
    from django.db.models import Sum
    from datetime import datetime

    info = CompanyInformations.objects.first()

    return dict(
        contest_name=contest.name,
        denomination_sociale=info.name,
        adresse=f'{info.address}, {info.postal_code} {info.city}',
        rcs=" ".join(wrap(info.siren, 3)),
        rcs_ville="",
        date_envoi=contest.start.strftime('%d-%m-%Y'),
        date_scrutin=contest.start.strftime("%d-%m-%Y"),
        heure_scrutin=contest.start.strftime('%Hh%M'),
        heure_fin_scrutin=contest.end.strftime('%Hh%M'),
        prenom_nom=[f'{g.user.first_name} {g.user.last_name}' for g in contest.guardian_set.all()],
        nombre_electeurs=contest.voter_set.count(),
        votants=contest.voter_set.exclude(casted_at=None).count(),
        candidates=contest.candidate_set.all(),
        fait_a=info.city,
        fait_le=datetime.now().strftime('%d-%m-%Y'),
        diffuse_le=datetime.now().strftime('%d-%m-%Y'),
        signataires=[f'{g.user.first_name} {g.user.last_name}' for g in contest.guardian_set.all()],
        winner=contest.candidate_set.order_by('-score').first().name
    )


def HTMLToPDF(company):
    liste_signataires = ""
    for signataire in company["signataires"]:
        liste_signataires += f"""<tr>
            <td style="height: 3em;">{signataire}<br/></td>
            <td style="height: 3em;"><br/></td>
        </tr>"""
    candidate_rows = [
        f"""
        <tr >
            <td>{candidate.name}</td>
            <td>{candidate.score}</td>
        </tr>
        """ for candidate in company['candidates']
    ]
    html = f"""
<html>
  <head>
    <title>RESULTAT DE LA CONSULTATION</title>
    <meta charset="UTF-8" />
  </head>
  <body>
    <header>
      <img src="file://{settings.BASE_DIR}/electis-core/static/images/logo-electis.svg" alt=""> 
      </br>
      <h1>PROCES-VERBAL</h1>
      <h1>RESULTAT DE LA CONSULTATION</h1>
      <h2>{company['denomination_sociale']}</h2>
      <h2>{company['adresse']}</h2>
      <h2>{company['rcs']} {company['rcs_ville']}</h2>
    </header>
    <div class="table">
      <table class="infos" style="width: 100%">
        <caption>Information sur l'élection</caption>
        <tr>
          <th>Date et heure d'ouverture</th>
          <td>{company['date_scrutin']} à {company['heure_scrutin']}</td>
        </tr>
        <tr>
          <th>Date et heure de fermeture</th>
          <td>{company['date_scrutin']} à {company['heure_fin_scrutin']}</td>
        </tr>
        <tr>
          <th>Mode de scrutin</th>
          <td>Par voie électronique</td>
        </tr>
        <tr>
          <th>Solution retenue</th>
          <td>Electis Solutions</td>
        </tr>
      </table>
      <table class="resultats" style="width: 100%">
        <caption>Statistiques de vote</caption>
        <tr>
          <th>Nombre d'électeurs inscrits</th>
          <td>{company['nombre_electeurs']}</td>
        </tr>
        <tr>
          <th>Nombre de votants</th>
          <td>{company['votants']}</td>
        </tr>
      </table>
      <table class="candidats" style="width: 100%">
        <caption>Résultats</caption>
        <tr>
          <th>Prénom Nom</th>
          <th>Nombre de voix obtenues</th>
        </tr>
        {''.join(candidate_rows)}
      </table>
   <br /> 
    <div class="remarques" style="width: 100%;">
      <table>
        <caption>Remarques</caption>
        <tr>
          <td class="remarque"></td>
        </tr>
      </table>
    </div>
    <br\><br\> <br\> <br\>
    <div class="location">
      <h2>Fait à : {company['fait_a']}</h2>
      <h2>Le : {company['fait_le']}</h2>
    </div>
    <br><br>
    <table class="signatures" style="width: 100%">
      <caption>Signatures des membres du bureau de vote</caption>
      <tr>
        <th>Membre du bureau de vote</th>
        <th>Signature</th>
      </tr>
      {liste_signataires}
    </table>
    </div>
  </body>
  <footer>
      <img src="file://{settings.BASE_DIR}/electis-core/static/images/logo-electis.svg" alt=""> 
      <p>49 avenue d'Iena 75016 Paris</p>
  </footer>
</html>
    """
    return html

options = {
    'dpi': 900,
    'page-size': 'A4',
    'margin-top': '0.0in',
    'margin-right': '0.0in',
    'margin-bottom': '0.0in',
    'margin-left': '0.0in',
    'no-outline': None,
    'enable-local-file-access': None,
}


def make_synthese(contest):
    import os
    content = HTMLToPDF(contest_to_company_data(contest))
    pdf = pdfkit.PDFKit(
        content,
        'string',
        options=options,
        css=os.path.dirname(__file__) + '/../../static/css/resultat_referendum.css'
    ).to_pdf()
    return pdf
