import pytz 

from datetime import datetime, timedelta

from django import forms
from django.utils import timezone

from ryzom_django.forms import widget_template
from ryzom_django_mdc.html import *

from djlang.utils import gettext as _

from ..models import Contest


@widget_template('django/forms/widgets/splitdatetime.html')
class SplitDateTimeWidget(SplitDateTimeWidget):
    date_label = _('Date')
    date_style = 'margin-top: 0; margin-bottom: 32px;'
    time_label = _('Time')
    time_style = 'margin: 0;'


class ContestFormBase(forms.ModelForm):
    class Meta:
        model = Contest
        fields = []


class ContestForm(forms.ModelForm):
    def now():
        _now = datetime.now() + timedelta(days=1)
        _now = _now.replace(second=0, microsecond=0)
        _now = _now.astimezone(pytz.timezone('Europe/Paris'))
        return _now.replace(tzinfo=None)

    def tomorrow():
        _tomorrow = datetime.now() + timedelta(days=2)
        _tomorrow = _tomorrow.replace(second=0, microsecond=0)
        _tomorrow = _tomorrow.astimezone(pytz.timezone('Europe/Paris'))
        return _tomorrow.replace(tzinfo=None)

    about = forms.CharField(
        label=_('FORM_ABOUT_ELECTION_CREATE'),
        widget=forms.Textarea,
        required=False
    )

    picture = forms.ImageField(
        widget=forms.FileInput(
            attrs=dict(
                label=_('Select file'),
                empty_value=_('No file selected')
            ),
        ),
        label=_('Image of the contest'),
        required=False
    )

    delete_picture = forms.BooleanField(
        required=False,
        label=_('Delete picture')
    )

    votes_allowed = forms.IntegerField(
        label=_('FORM_VOTES_ALLOWED_ELECTION_CREATE'),
        initial=1,
        help_text=_(
            'The maximum number of choice a voter can make for this election'
        )
    )

    info = forms.DateField(
        label='Date',
        required=False,
        widget=forms.DateInput(
            format='%Y-%m-%d'
        )
    )

    start = forms.SplitDateTimeField(
        label='',
        initial=now,
        widget=forms.SplitDateTimeWidget(
            date_format='%Y-%m-%d',
            date_attrs={'type': 'date', 'label': _('Date')},
            time_attrs={'type': 'time', 'label': _('Time')},
        ),
    )
    end = forms.SplitDateTimeField(
        label='',
        initial=tomorrow,
        widget=forms.SplitDateTimeWidget(
            date_format='%Y-%m-%d',
            date_attrs={'type': 'date', 'label': _('Date')},
            time_attrs={'type': 'time', 'label': _('Time')},
        )
    )

    open_email_title = forms.CharField(
        label=_('FORM_OPEN_EMAIL_TITLE_ELECTION_CREATE'),
        help_text=_('FORM_OPEN_EMAIL_TITLE_HELP_TEXT'),
        required=False
    )

    open_email = forms.CharField(
        label=_('FORM_OPEN_EMAIL_ELECTION_CREATE'),
        help_text=_('FORM_OPEN_EMAIL_HELP_TEXT'),
        widget=forms.Textarea,
        required=False
    )

    close_email_title = forms.CharField(
        label=_('FORM_CLOSE_EMAIL_TITLE_ELECTION_CREATE'),
        help_text=_('FORM_CLOSE_EMAIL_TITLE_HELP_TEXT'),
        required=False,
    )

    close_email = forms.CharField(
        label=_('FORM_CLOSE_EMAIL_ELECTION_CREATE'),
        help_text=_('FORM_CLOSE_EMAIL_HELP_TEXT'),
        widget=forms.Textarea,
        required=False
    )

    def clean_start(self):
        start = self.cleaned_data['start']
        if start < timezone.now().astimezone(self.cleaned_data['timezone']).replace(tzinfo=pytz.utc):
            raise forms.ValidationError(_('Start cannot be in the past'))
        return start.replace(second=0, microsecond=0)

    def clean_end(self):
        end = self.cleaned_data['end']
        if start := self.cleaned_data.get('start', None):
            if end < start:
                raise forms.ValidationError(_('End cannot be before start'))
        return end.replace(second=0, microsecond=0)

    def clean(self):
        data = self.cleaned_data
        for field_name in [
            'open_email',
            'open_email_title',
            'close_email',
            'close_email_title'
        ]:
            data[field_name] = data[field_name].replace('NAME', data['name'])

        return data

    class Meta:
        model = Contest
        fields = [
            'name',
            'about',
            'picture',
            'delete_picture',
            'votes_allowed',
            'exact_scores',
            'shuffle_candidates',
            'redirect_to_vote',
            'link_to_contest_list',
            'timezone',
            'start',
            'end',
            'consent_provider',
            'open_email_title',
            'open_email',
            'close_email_title',
            'close_email',
            'send_sms',
        ]
        labels = {
            'name': _('FORM_TITLE_ELECTION_CREATE'),
            'about': _('FORM_ABOUT_ELECTION_CREATE'),
            'picture': _('Contest picture'),
            'delete_picture': _('Delete picture'),
            'votes_allowed': _('FORM_VOTES_ALLOWED_ELECTION_CREATE'),
            'start': _('FORM_START_ELECTION_CREATE'),
            'end': _('FORM_END_ELECTION_CREATE'),
            'timezone': _('FORM_TIMEZONE_ELECTION_CREATE'),
            'consent_provider': _('FORM_EMAIL_COLLECTION_ELECTION_CREATE'),
            'open_email_title': _('FORM_OPEN_EMAIL_TITLE_ELECTION_CREATE'),
            'open_email': _('FORM_OPEN_EMAIL_ELECTION_CREATE'),
            'close_email_title': _('FORM_CLOSE_EMAIL_TITLE_ELECTION_CREATE'),
            'close_email': _('FORM_CLOSE_EMAIL_ELECTION_CREATE'),
            'send_sms': _('FORM_SEND_SMS_ELECTION_CREATE')
        }


class EmailForm(forms.ModelForm):
    open_email_title = forms.CharField(
        label=_('Email title'),
        help_text=_('Title of the email that will be sent to each voter'),
    )
    open_email = forms.CharField(
        widget=forms.Textarea,
        label=_('Email message'),
        help_text=_(
            'Body of the email that will be sent,'
            ' LINK will be replaced by the voting link'
        ),
    )

    class Meta:
        model = Contest
        fields = ['open_email_title', 'open_email']


class CloseEmailForm(forms.ModelForm):
    email_title = forms.CharField(
        label=_('Email title'),
        help_text=_('Title of the email that will be sent to each voter'),
    )
    email_message = forms.CharField(
        widget=forms.Textarea,
        label=_('Email message'),
        help_text=_(
            'Body of the email that will be sent,'
            ' LINK will be replaced by the result link'
        ),
    )

    send_email = forms.BooleanField(
        required=False,
        initial=True,
        label=_('Send email to voters'),
        widget=forms.CheckboxInput
    )

    class Meta:
        model = Contest
        fields = ['email_title', 'email_message', 'send_email']


class ContestOpenForm(forms.ModelForm):
    open_email_title = forms.CharField(
        label=_('Email title'),
        help_text=_('Title of the email that will be sent to each voter'),
    )
    open_email = forms.CharField(
        widget=forms.Textarea,
        label=_('Email message'),
        help_text=_(
            'Body of the email that will be sent,'
            ' LINK will be replaced by the voting link'
        ),
    )

    send_open_email = forms.BooleanField(
        required=False,
        initial=True,
        label=_('Send email to voters'),
        widget=forms.CheckboxInput
    )

    auto_mode = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput
    )

    submit_label = _('Open votes')
    help_text = _(
        'Create the Encrypter and BallotBox and open contest for voting'
    )

    class Meta:
        model = Contest
        fields = ['send_open_email', 'auto_mode', 'open_email_title', 'open_email']

    def clean_email_title(self):
        if self.cleaned_data['send_open_email'] and not self.cleaned_data['open_email_title']:
            raise forms.ValidationError(_('This field is required'))
        return self.cleaned_data['open_email_title']

    def clean_email_message(self):
        if self.cleaned_data['send_open_email'] and not self.cleaned_data['open_email']:
            raise forms.ValidationError(_('This field is required'))
        return self.cleaned_data['open_email']

    def clean_auto_mode(self):
        if self.cleaned_data['auto_mode'] and self.instance.local_start < self.instance.local_now:
            raise forms.ValidationError(_('Cannot schedule past events'))
        return self.cleaned_data['auto_mode']

    def clean(self):
        if self.instance.candidate_set.count() < self.instance.number_elected:
            raise forms.ValidationError(
                _('Must have at least as many candidates than votes allowed')
            )


class ContestAutoOpenForm(forms.ModelForm):
    auto_mode = forms.BooleanField(
        required=False,
        initial=True,
        widget=forms.CheckboxInput
    )

    class Meta:
        model = Contest
        fields = ['auto_mode']

    def clean_auto_mode(self):
        if self.cleaned_data['auto_mode'] == True and self.instance.local_start < self.instance.local_now:
            raise forms.ValidationError('')
        return self.cleaned_data['auto_mode']


class ContestAutoCloseForm(forms.ModelForm):
    auto_mode = forms.BooleanField(
        required=False,
        initial=True,
        widget=forms.CheckboxInput
    )

    class Meta:
        model = Contest
        fields = ['auto_mode']

    def clean_auto_mode(self):
        if self.cleaned_data['auto_mode'] == True and self.instance.local_end < self.instance.local_now:
            raise forms.ValidationError('')
        return self.cleaned_data['auto_mode']


class EmailVotersForm(EmailForm):
    submit_label = _('Send invite')
