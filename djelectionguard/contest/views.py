import io
import json

from django import http
from django.contrib import messages
from django.views import generic

from ryzom_django_channels.views import ReactiveMixin

from djlang.utils import gettext as _

from electionguard.serialize import to_raw

from electeez_consent.models import Provider
from electeez_sites.models import Site, CompanyInformations

from ..permissions import *

from ..models import Contest
from ..models import (
    get_open_email_title,
    get_open_email_message,
    get_close_email_title,
    get_close_email_message,
    send_guardian_mail,
    async_tally
)

from .forms import (
    ContestForm,
    ContestFormBase,
    ContestOpenForm,
    ContestAutoOpenForm,
    ContestAutoCloseForm,
    EmailForm,
    EmailVotersForm,
    CloseEmailForm
)

from djelectionguard.views import Enforce2FAMixin

from .components import *

from django.db.models import Q


class ContestFormViewMixin:
    model = Contest
    form_class = ContestForm

    def get_initial(self):
        return dict(
            open_email_title=get_open_email_title(self.object),
            open_email=get_open_email_message(self.object),
            close_email_title=get_close_email_title(self.object),
            close_email=get_close_email_message(self.object)
        )

    def get_form(self):
        form = super().get_form()
        accessible_providers = Provider.objects.filter(
            Q(public=True)
            | Q(provideraccess__user=self.request.user)
        ).distinct().order_by('id')
        form.fields['consent_provider'].queryset = accessible_providers
        form.fields['consent_provider'].initial = accessible_providers.first()
        return form


class ContestListView(ContestPermissionMixin, generic.ListView):
    permission_chain = [
        user_can_see_contest
    ]
    model = Contest

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).only(
            'id', 'name', 'actual_start', 'actual_end',
            'plaintext_tally', 'mediator'
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_voters = self.request.user.voter_set.all().values('contest_id', 'casted_at')
        user_voters = {v['contest_id']: v['casted_at'] for v in user_voters}
        context['user_voters'] = user_voters
        return context


class ContestCreateView(Enforce2FAMixin, ContestFormViewMixin, generic.CreateView):
    def dispatch(self, request, *args, **kwargs):
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)

        all_users_can_create = Site.objects.get_current().all_users_can_create
        user_is_staff = self.request.user.is_staff

        if all_users_can_create or user_is_staff:
            return super(generic.CreateView, self).dispatch(request, *args, **kwargs)

        return http.HttpResponseRedirect(reverse('contest_list'))

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def form_valid(self, form):
        form.instance.mediator = self.request.user
        form.instance.site = Site.objects.get_current()
        if not self.request.user.can_send_sms or not settings.TWILIO_ENABLED:
            if form.cleaned_data['send_sms']:
                form.add_error(
                    'send_sms',
                    _('You are not allowed to send sms')
                )
                return self.form_invalid(form)
        response = super().form_valid(form)
        form.instance.guardian_set.create(
            user=self.request.user,
            sequence=form.instance.guardian_sequence
        )
        messages.success(
            self.request,
            _('You have created contest %(obj)s', obj=form.instance)
        )
        return response


class ContestDetailView(
    ReactiveMixin,
    ContestPermissionMixin,
    generic.DetailView
):
    permission_chain = [
        user_can_see_contest
    ]

    def dispatch(self, request, *args, **kwargs):
        contest = self.get_object()
        if contest.redirect_to_vote:
            if user := request.user:
                if (
                    contest.mediator == user
                    or contest.guardian_set.filter(user=user).first()
                    or not contest.actual_start
                ):
                    return super().dispatch(request, *args, **kwargs)

                if voter := user.voter_set.filter(contest=contest).first():
                    if not voter.casted_at:
                        return http.HttpResponseRedirect(
                            reverse('contest_vote', args=[contest.id])
                        )
                    else:
                        return http.HttpResponseRedirect(
                            reverse('contest_list')
                        )

        return super().dispatch(request, *args, **kwargs)


class ContestUpdateView(
    Enforce2FAMixin,
    ContestPermissionMixin,
    ContestFormViewMixin,
    generic.UpdateView
):
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_not_opened_yet
    ]

    def form_valid(self, form):
        if form.cleaned_data['delete_picture']:
            form.instance.picture.delete()
            form.instance.picture = None

        response = super().form_valid(form)
        messages.success(
            self.request,
            _('You have updated contest %(obj)s', obj=form.instance)
        )
        return response


class ContestKeyCeremonyView(
    Enforce2FAMixin,
    ContestPermissionMixin,
    generic.UpdateView
):
    permission_chain = [
        user_is_mediator_of_contest,
        contest_key_ceremony_not_started
    ]
    model = Contest
    form_class = ContestFormBase

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def form_valid(self, form):
        self.object.key_ceremony()
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()


class EmailBaseView(generic.UpdateView):
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = dict(
            open_email_title=get_open_email_title(self.object),
            open_email=get_open_email_message(self.object),
        )
        return kwargs


class ContestOpenView(Enforce2FAMixin, ContestPermissionMixin, EmailBaseView):
    template_name = 'contest_open'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_key_ceremony_complete,
        contest_is_not_opened_yet,
    ]
    form_class = ContestOpenForm

    def get_form(self):
        """ If the contest is in the past, the auto_mode checkbox will be
        disabled and unchecked by default.
        """
        form = super().get_form()
        contest = form.instance
        form.fields['auto_mode'].label = Markdown(str(_(
            'Automatically open the **%(start_date)s** at **%(start_time)s** (%(timezone)s)', 
            start_date=contest.start.strftime('%d/%m/%Y'),
            start_time=contest.start.strftime('%Hh%M'),
            timezone=contest.timezone
        )), strip_outer_p=True)
        if self.request.method == 'GET' and contest.local_start < contest.local_now:
            if not getattr(form, 'cleaned_data', None):
                form.cleaned_data = []
            form.fields['auto_mode'].disabled = True
            form.add_error('auto_mode', _('Cannot schedule past events'))
        return form
    
    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def form_valid(self, form):
        """ If auto_mode is checked, the contest will be opened automatically
        at the specified time. Otherwise, the contest will be opened
        immediately.
        """
        response = super().form_valid(form)

        if not form.cleaned_data['auto_mode']:
            self.object.open()
        return response


class ContestAutoOpenView(Enforce2FAMixin, ContestPermissionMixin, generic.UpdateView):
    """ This view is used to remove the auto_mode checkbox from the contest.
    It should only be requested by the fetch API and will return a JSON
    """
    permission_chain = [
        user_is_mediator_of_contest,
        contest_key_ceremony_complete,
        contest_is_not_opened_yet,
    ]
    form_class = ContestAutoOpenForm

    def form_valid(self, form):
        """ Return JSON response to confirm the contest has been scheduled to
        open automatically.
        """
        form.save()
        return http.JsonResponse({'status': 'ok'})

    def form_invalid(self, form):
        """ Return JSON response to confirm the contest has been scheduled to
        open automatically.
        """
        return http.JsonResponse({'status': 'error', 'errors': form.errors})


class ContestAutoCloseView(Enforce2FAMixin, ContestPermissionMixin, generic.UpdateView):
    """ This view is used to remove the auto_mode checkbox from the contest.
    It should only be requested by the fetch API and will return a JSON
    """
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_opened,
        contest_is_not_closed
    ]
    form_class = ContestAutoCloseForm

    def form_valid(self, form):
        """ Return JSON response to confirm the contest has been scheduled to
        close automatically.
        """
        form.save()
        return http.JsonResponse({'status': 'ok'})

    def form_invalid(self, form):
        """ Return JSON response to confirm the contest has been scheduled to
        close automatically.
        """
        return http.JsonResponse({'status': 'error', 'errors': form.errors})



class EmailVotersView(Enforce2FAMixin, ContestPermissionMixin, EmailBaseView):
    template_name = 'email_voters'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_opened,
    ]
    form_class = EmailVotersForm

    def form_valid(self, form):
        form.instance.send_mail(
            form.cleaned_data['open_email_title'] or get_open_email_title(self),
            form.cleaned_data['open_email'] or get_open_email_message(self),
            reverse('contest_list') if contest.link_to_contest_list else reverse('contest_vote', args=[form.instance.pk]),
            'open_email_sent'
        )
        return super().form_valid(form)



class ContestCloseView(Enforce2FAMixin, ContestPermissionMixin, generic.UpdateView):
    template_name = 'contest_close'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_opened
    ]
    form_class = ContestAutoCloseForm

    def get_form(self):
        """ If the contest is in the past, the auto_mode checkbox will be
        disabled and unchecked by default.
        """
        form = super().get_form()
        contest = form.instance
        form.fields['auto_mode'].label = Markdown(str(_(
            'Automatically close the **%(end_date)s** at **%(end_time)s** (%(timezone)s)', 
            end_date=contest.end.strftime('%d/%m/%Y'),
            end_time=contest.end.strftime('%Hh%M'),
            timezone=contest.timezone
        )), strip_outer_p=True)
        if self.request.method == 'GET' and contest.local_start < contest.local_now:
            if contest.local_end < contest.local_now:
                if not getattr(form, 'cleaned_data', None):
                    form.cleaned_data = []
                form.fields['auto_mode'].disabled = True
                form.add_error('auto_mode', _('Cannot schedule past events'))
        return form

    def form_valid(self, form):
        self.object.close()
        messages.success(
            self.request,
            _('You have closed contest %(obj)s', obj=self.object)
        )
        return super().form_valid(form)


class ContestDecryptView(Enforce2FAMixin, ContestPermissionMixin, generic.UpdateView):
    template_name = 'contest_decrypt'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_closed,
        quorum_guardian_have_uploaded_key,
        contest_is_not_decrypting,
        contest_is_not_decrypted
    ]
    form_class = CloseEmailForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = dict(
            email_title=get_close_email_title(self.object),
            email_message=get_close_email_message(self.object)
        )
        return kwargs

    def form_valid(self, form):
        email_voters = (
            'send_email' not in form.cleaned_data
            or not form.cleaned_data['send_email']
        )

        async_tally.delay(
                str(self.object.id),
                email_voters,
                form.cleaned_data['email_title'],
                form.cleaned_data['email_message']
        )

        return http.HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        messages.success(
            self.request,
            _('You have started tallying for %(obj)s', obj=self.object)
        )
        return self.object.get_absolute_url()


class ContestResultView(ContestPermissionMixin, generic.DetailView):
    template_name = 'contest_result'
    permission_chain = [
        user_can_see_contest,
        contest_is_decrypted
    ]

    def get_queryset(self):
        if Site.objects.get_current().all_results_are_visible:
            return self.default_qs()

        if self.request.user.is_authenticated:
            return super().get_queryset()

        return self.default_qs().none()


class ContestPublishView(Enforce2FAMixin, ContestPermissionMixin, generic.UpdateView):
    template_name = 'contest_publish'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_decrypted
    ]
    form_class = ContestFormBase

    def form_valid(self, form):
        if not settings.IPFS_ENABLED:
            messages.error(
                self.request,
                _('IPFS not initialized on this node')
            )
        else:
            self.object.publish_ipfs()
            if self.object.artifacts_ipfs:
                try:
                    contract = self.object.electioncontract
                except ObjectDoesNotExist:
                    # Contract not deployed on the blockchain
                    pass
                else:
                    if contract.is_internal:
                        contract.artifacts()

        return super().form_valid(form)

    def get_success_url(self):
        publish = self.request.POST.get('publish')
        if publish == 'IPFS':
            self.object.refresh_from_db()
            if self.object.artifacts_ipfs:
                messages.success(
                    self.request,
                    _(
                        'You have published artifacts for %(obj)s on IPFS',
                        obj=self.object
                    )
                )

            if not self.object.electioncontract.is_internal:
                return reverse('contest_publish', args=[self.object.id])

        return self.object.get_absolute_url()


class ContestScoresJson(ContestPermissionMixin, generic.DetailView):
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_decrypted
    ]

    def get(self, request, *args, **kwargs):
        contest = self.get_object()
        scores = {c.name: c.score for c in contest.candidate_set.all()}
        return http.JsonResponse(scores)


class ContestManifestView(ContestPermissionMixin, generic.DetailView):
    permission_chain = [
        user_can_see_contest
    ]

    def get(self, request, *args, **kwargs):
        manifest_str = to_raw(self.get_object().get_manifest())
        return http.JsonResponse(json.loads(manifest_str))


class ContestFingerprintJSONView(ContestPermissionMixin, generic.DetailView):
    permission_chain = [
        user_can_see_contest
    ]

    def get(self, request, *args, **kwargs):
        contest = self.get_object()
        return http.JsonResponse({
            'summary': contest.get_tldr_fingerprint(),
            'fingerprints': contest.get_fingerprints(),
        })


class ContestFingerprintView(ContestPermissionMixin, generic.DetailView):
    template_name = 'djelectionguard/contest_fingerprint.html'
    permission_chain = [
        user_can_see_contest
    ]


class VoterListDownloadView(Enforce2FAMixin, ContestPermissionMixin, generic.DetailView):
    permissaion_chain = [
        user_is_mediator_of_contest,
        contest_is_decrypted
    ]

    def get(self, *args, **kwargs):
        contest = self.get_object()

        voters_table = contest.voter_set.all().values_list('user__email', 'casted_at')

        data = f'email,datetime\n'
        for email, casted_at in voters_table:
            f = casted_at.strftime('%d/%m/%Y %H:%M') if casted_at else ''
            data += f"{email},{f}\n"

        return http.FileResponse(
            io.BytesIO(data.encode()),
            as_attachment=True,
            filename=f'{contest.name}-voters_list.csv',
            content_type='text/plain'
        )


class ResultDownloadView(ContestPermissionMixin, generic.DetailView):
    permission_chain = [
        user_can_see_contest,
        contest_is_decrypted
    ]

    def get(self, *args, **kwargs):
        contest = self.get_object()

        score_table = contest.candidate_set.order_by(
            '-score'
        ).values_list('name', 'score')

        data = f'name,score\n'
        for name, score in score_table:
            data += f"{name},{score}\n"

        return http.FileResponse(
            io.BytesIO(data.encode()),
            as_attachment=True,
            filename=f'{contest.name}-result_table.csv',
            content_type='text/plain'
        )


class ReferendumDownloadView(ContestPermissionMixin, generic.DetailView):
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_decrypted
    ]

    def get(self, request, *args, **kwargs):
        contest = self.get_object()

        if contest.guardian_set.filter(
            Q(user__first_name='')
            | Q(user__last_name='')
        ).count():
            messages.error(request, _('All guardians must have set their first and last name in their account'))
            return http.HttpResponseRedirect(
                reverse('contest_result', args=[contest.id])
            )

        site = Site.objects.get_current()
        if not CompanyInformations.objects.filter(site=site).first():
            messages.error(request, _('Please provide your company informations in admin panel'))
            return http.HttpResponseRedirect(
                reverse('contest_result', args=[contest.id])
            )

        from djelectionguard.contest.referendum import make_synthese
        pdf = make_synthese(contest)

        return http.FileResponse(
            io.BytesIO(pdf),
            filename=f'{contest.name}-synthese.pdf',
            content_type='application/pdf'
        )
