from django.template.defaultfilters import date as _date
from django.db.models import Sum

from djlang.utils import gettext as _

from electeez_auth.models import User
from electeez_common.components import *
from djelectionguard.components import *
from djelectionguard.candidate.components import *
from djelectionguard.guardian.components import *
from djelectionguard.scrutator.components import *
from djelectionguard.voter.components import *
from djelectionguard_tezos.views import (
    TezosSelfOpen, TezosSelfClose, TezosSelfPublish
)


class ContestFormComponent(CList):
    def __init__(self, view, form, edit=False):
        content = []
        content.append(Ul(
            *[Li(e) for e in form.non_field_errors()],
            cls='error-list'
        ))

        sms_enabled = (
            settings.TWILIO_ENABLED
            and view.request.user.can_send_sms
        )

        super().__init__(
            H4(_('Edit election') if edit else _('Create an election')),
            Form(
                form['name'],
                form['about'],
                H6(_('Contest picture')),
                Div(
                    form['picture'],
                    form['delete_picture']
                    if form.instance.picture else None,
                    cls='checkbox-field'
                ),
                H6(_('Voting settings:')),
                form['votes_allowed'],
                MDCAccordion(
                    MDCAccordionSection(
                        form['exact_scores'],
                        form['shuffle_candidates'],
                        form['link_to_contest_list'],
                        form['redirect_to_vote'],
                        label=_('Advanced options')
                    )
                ),
                H6(_('Voters information date:')) if settings.REFERENDUM else '',
                form['info'] if settings.REFERENDUM else '',
                H6(_('Election starts:')),
                form['start'],
                H6(_('Election ends:')),
                form['end'],
                form['timezone'],
                H6(_('Email collection mode:')),
                form['consent_provider'],
                H6(_('Open election email:')),
                form['open_email_title'],
                form['open_email'],
                H6(_('Close election email:')),
                form['close_email_title'],
                form['close_email'],
                sms_enabled and H6(_('Verify voters phone number:')) or None,
                sms_enabled and Div(
                    form['send_sms'],
                    cls='checkbox-field'
                ) or None,
                CSRFInput(view.request),
                MDCButton(
                    _('update election') if edit else _('create election')
                ),
                method='POST',
                cls='form contest-form'),
        )


class ContestFiltersBtn(Button):
    def __init__(self, pos, text, active=False):
        active_cls_name = 'mdc-tab--active' if active else ''
        active_indicator = 'mdc-tab-indicator--active' if active else ''

        attrs = {
            'class': f'contest-filter-btn mdc-tab {active_cls_name}',
            'role': 'tab',
            'aria-selected': 'true',
            'tabindex': pos
        }
        super().__init__(
            Span(
                Span(text, cls='mdc-tab__text-label'),
                cls='mdc-tab__content'
            ),
            Span(
                Span(
                    cls='mdc-tab-indicator__content'
                        ' mdc-tab-indicator__content--underline'
                ),
                cls=f'mdc-tab-indicator {active_indicator}'
            ),
            Span(cls='mdc-tab__ripple'),
            **attrs
        )


class ContestFilters(Div):
    def __init__(self, view):
        active_btn = view.request.GET.get('q', 'all')

        self.all_contests_btn = ContestFiltersBtn(
            1,
            _('all'),
            active_btn == 'all'
        )
        self.my_contests_btn = ContestFiltersBtn(
            2,
            _('created by me'),
            active_btn == 'created'
        )
        self.shared_contests_btn = ContestFiltersBtn(
            3,
            _('shared with me'),
            active_btn == 'shared'
        )

        super().__init__(
            Div(
                Div(
                    self.all_contests_btn,
                    self.my_contests_btn,
                    self.shared_contests_btn,
                    cls='mdc-tab-scroller__scroll-content'
                ),
                cls='mdc-tab-scroller__scroll-area'
                    ' mdc-tab-scroller__scroll-area--scroll'
            ),
            cls='mdc-tab-bar contest-filter'
        )


class ContestItem(Li):
    def __init__(self, contest, user, *args, **kwargs):
        active_cls = ''
        status = ''
        voted = kwargs.get('user_voters').get(contest.id, None)
        if contest.actual_start:
            status = _('voting ongoing')
            active_cls = 'active'
        if contest.actual_end:
            status = _('voting closed')
        if contest.plaintext_tally:
            active_cls = ''
            status = _('result available')

        status_2 = _(', voted') if voted else ''

        name = Span(contest.name, cls='contest-name', id=str(contest.id) + '_id')
        contest_url = reverse('contest_detail', args=[contest.id])

        super().__init__(
            Span(cls='mdc-deprecated-list-item__ripple'),
            Span(
                Span(cls=f'contest-indicator {"voted" if voted else ""}'),
                Span(
                    Span(status, status_2, cls='contest-status overline'),
                    name,
                    cls='list-item__text-container'
                ),
                cls='mdc-deprecated-list-item__text'
            ),
            Span(MDCIcon('task_alt'), style=dict(
                color='var(--green2)',
                display='flex',
                flex_grow=2,
                justify_content='end',
                margin='0 32px'
            )) if voted else None,
            aria_labelledby=name.id,
            role='link',
            cls='contest-list-item mdc-deprecated-list-item'
                f' mdc-ripple-upgraded {active_cls}',
            tabindex=0,
            onclick=f"location.assign('{contest_url}')"
        )


class ContestListItem(ListItem):
    def __init__(self, obj, **kwargs):
        super().__init__(ContestItem(obj, **kwargs), False)


class ContestListCreateBtn(A):
    def __init__(self):
        super().__init__(
            Span(
                Span(cls='mdc-deprecated-list-item__ripple'),
                Span(
                    Span('+', cls='new-contest-icon'),
                    cls='new-contest-icon-container'
                ),
                Span(_('Create new election')),
                cls='mdc-deprecated-list-item__text text-btn mdc-ripple-upgraded'
            ),
            cls='mdc-deprecated-list-item contest-list-item',
            href=reverse('contest_create')
        )


class ArtifactsLinks(Div):
    def __init__(self, contest):
        links = Div(
            style=dict(
                display='flex',
                flex_flow='row nowrap',
                justify_content='space-around'
            )
        )

        if contest.electioncontract.blockchain.explorer:
            links.addchild(
                Div(
                    A(
                        _('Election report'),
                        href=contest.electioncontract.explorer_link,
                        target='_blank'
                    ),
                    Br(),
                    _('On Tezos\' blockchain'),
                    style=dict(
                        text_align='center',
                        color='#888',
                        margin='12px'
                    )
                )
            )

        if contest.plaintext_tally:
            links.addchild(
                Div(
                    A(_('Election datas'), href=contest.artifacts_local_url),
                    Br(),
                    _('Local data'),
                    style=dict(
                        text_align='center',
                        color='#888',
                        margin='12px'
                    )
                )
            )

        if contest.artifacts_ipfs_url:
            links.addchild(
                Div(
                    A(
                        _('Election datas'),
                        href=contest.artifacts_ipfs_url,
                        target='_blank'
                    ),
                    Br(),
                    _('On IPFS, decentralized'),
                    style=dict(
                        text_align='center',
                        color='#888',
                        margin='12px'
                    )
                )
            )

        super().__init__(links, style=dict(margin_top='32px'))


class BasicSettingsAction(ListAction):
    def __init__(self, obj):
        btn_comp = MDCButtonOutlined(
            _('edit'),
            False,
            tag='a',
            href=reverse('contest_update', args=[obj.id]))
        super().__init__(
            _('Basic settings'),
            _('Name, votes allowed, time and date, etc.'),
            DoneIcon(), btn_comp
        )


class OnGoingElectionAction(ListAction):
    def __init__(self, **context):
        contest = context['contest']
        user = context['user']
        close_url = reverse('contest_close', args=[contest.id])
        close_btn = MDCButtonOutlined(
            _('close'),
            False,
            tag='a',
            href=close_url
        )
        start_time = '**' + _date(contest.local_start, r'd F, G\hi') + '**'
        sub_txt = None
        if contest.actual_end:
            end_time = '**' + _date(contest.local_end, r'd F, G\hi') + '**'
            title = _('Voting closed')
            txt = _(
                'The voting started on %(start)s and was open till %(end)s.'
                ' Timezone: %(timezone)s.',
                start=start_time,
                end=end_time,
                timezone=f"**{contest.timezone}**"
            )
            icon = DoneIcon()
        else:
            vote_link = (
                reverse('otp_send')
                + '?redirect='
                + reverse('contest_vote', args=[contest.id])
            )
            vote_link = f"{settings.PROTO}://{contest.site.domain}{vote_link}"
            end_time = '**' + _date(contest.end, r'd F, G\hi') + '**'
            title = _('The voting process is currently ongoing')
            txt = _(
                'The voting started on %(time_start)s and will be closed at'
                ' %(time_end)s. Timezone: %(timezone)s',
                time_start=start_time,
                time_end=end_time,
                timezone=f"**{contest.timezone}**"
            )
            icon = OnGoingIcon()

        sub_txt = _('Check the [fingerprints](%(link)s) of the election',
            link=reverse('contest_fingerprints', args=[contest.id])
        )

        inner = Span(
            Markdown(str(txt)),
            CList(Br(), Markdown(str(sub_txt))) if sub_txt else None,
            cls='body-2 red-button-container'
        )

        if contest.mediator == user and not contest.actual_end:
            inner.addchild(close_btn)

        separator = (
            contest.actual_end
            or contest.mediator == user
            or contest.guardian_set.filter(user=user).count()
        )

        super().__init__(
            title,
            inner,
            icon,
            None,
            separator=separator
        )


class UnlockBallotAction(ListAction):
    def __init__(self, **context):
        self.contest = contest = context['contest']
        self.user = context['user']
        self.has_action = False

        n_uploaded = contest.guardian_set.exclude(uploaded=None).count()

        if contest.actual_end:
            task_list = Ol()
            quorum_text = _(
                '%(quorum)s guardians',
                quorum=contest.quorum,
                n=contest.quorum,
            )
            txt = _(
                'A minimum of %(quorum_text)s upload their keys'
                ' %(uploaded)s/%(quorum)s uploaded',
                quorum_text=quorum_text,
                uploaded=n_uploaded,
                quorum=contest.quorum,
                n=n_uploaded,
            )
            cls = 'bold'
            if n_uploaded >= contest.quorum:
                cls = 'line'

            task_list.addchild(Li(txt, cls=cls))

            cls = 'bold' if cls == 'line' else ''
            txt = _(
                'Unlock the ballot box with encrypted ballots'
                ' and reveal the results'
            )
            task_list.addchild(Li(txt, cls=cls))

            content = Span(
                P(_(
                    'All guardians need to upload their private keys so that'
                    ' the ballot box can be opened to reveal the results.'
                )),
                task_list,
                cls='body-2'
            )
        else:
            content = Span(
                P(_(
                    'When the election is over the guardians use their'
                    ' keys to open the ballot box and count the results.'
                )),
                cls='body-2'
            )

        title = _('Unlocking the ballot box and revealing the results')
        if (
            contest.actual_end
            and not self.has_action
            and n_uploaded >= contest.quorum
        ):
            action_url_ = reverse('contest_decrypt', args=(contest.id,))
            action_btn_ = MDCButton(
                _('reveal results'),
                True,
                tag='a',
                href=action_url_,
                disabled=n_uploaded < contest.quorum)
            content.addchild(action_btn_)

        icon = TodoIcon()

        super().__init__(
            title,
            content,
            icon,
            None,
            separator=False,
        )


class WaitForEmailAction(ListAction):
    def __init__(self, **context):
        super().__init__(
            _('Once the ballots are counted you will be notified by email'),
            '',
            EmailIcon(), None, separator=False
        )


class ReactiveResultAction(ReactiveComponentMixin, ListActionItem):
    def __init__(self, contest, user):
        self.register = f'{contest.id}:result_action'

        subtext = Div()
        if contest.decrypting:
            icon = OnGoingIcon()
            title = _('Tallying in progress')
            if contest.mediator == user:
                subtext.addchild(
                    Div(_('An email will be sent when finished'))
                )
        else:
            icon = DoneIcon()
            title = _('Results available')
            if contest.mediator == user:
                subtext.addchild(
                    Div(_(
                        'Congratulations! You have been the mediator'
                        ' of a secure election.'
                    ))
                )

            url = reverse('contest_result', args=[contest.id])
            result_btn = MDCButton(_('view result table'), tag='a', href=url, addcls='result-btn')
            subtext.addchild(result_btn)

        super().__init__(
            title,
            subtext,
            icon,
            None,
            separator=False
        )


class ContestSettingsCard(Div):
    def __init__(self, **context):
        user = context['user']
        contest = context['contest']
        list_content = []
        if contest.mediator == user:
            list_content += [
                settings.SHOW_BASIC_SETTINGS and BasicSettingsAction(contest) or None,
                settings.SHOW_CANDIDATES and AddCandidateAction(contest) or None,
                settings.SHOW_VOTERS and AddVoterAction(**context) or None,
                settings.SHOW_BLOCKCHAIN and ChooseBlockchainAction(**context) or None,
            ]

            voter_count = context['num_voter']
            if (
                voter_count
                and context['num_candidate']
                and context['num_candidate'] >= contest.number_elected
            ):
                s = contest.PublishStates.ELECTION_NOT_DECENTRALIZED
                if contest.publish_state != s:
                    list_content.append(
                        SecureElectionAction(contest, user)
                    )
        else:
            list_content.append(SecureElectionAction(contest, user))

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        super().__init__(
            H4(contest.name, style='word-break: break-word;'),
            Div(
                Img(src=contest.picture.url, alt='contest-picture', width="100%"),
                style='padding: 32px',
            ) if contest.picture else '',
            Div(
                about,
                style='padding: 12px; word-break: break-word;',
                cls='subtitle-2'
            ),
            Ul(
                *list_content,
                cls='mdc-deprecated-list action-list'
            ),
            cls='setting-section main-setting-section'
        )


class ContestVotingCard(Div):
    def __init__(self, **context):
        list_content = []

        contest = context['contest']
        user = context['user']
        guardian = context['guardian']
        actions = []
        if 'voter' not in context:
            context['voter'] = contest.voter_set.filter(user=user).first()
            context['casted_at'] = voter.casted_at if voter else None
            context['voter'] = voter
            context['can_vote'] = (
                self.actual_start
                and not self.actual_end
                and voter
                and not voter.casted_at
            )

        if context['can_vote']:
            actions.append('vote')

        if contest.mediator_id == context['user'].id:
            actions.append('close')

        if context['guardian']:
            actions.append('upload')

        if 'vote' in actions:
            list_content.append(CastVoteAction(**context))

        list_content.append(OnGoingElectionAction(**context))

        if 'upload' in actions:
            list_content.append(UploadPrivateKeyAction(**context))
            if contest.mediator == user:
                list_content.append(UnlockBallotAction(**context))
            elif guardian.uploaded:
                if contest.mediator != user:
                    list_content.append(WaitForEmailAction(**context))
        elif contest.mediator == user:
            list_content.append(UnlockBallotAction(**context))

        if not len(actions):
            list_content.append(WaitForEmailAction(**context))

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        super().__init__(
            H4(contest.name, style='word-break: break-word;'),
            Div(
                Img(src=contest.picture.url, alt='contest-picture', width="100%"),
                style='padding: 32px',
            ) if contest.picture else '',
            Div(
                about,
                style='padding: 12px; word-break: break-word;',
                cls='subtitle-2'
            ),
            Ul(
                *list_content,
                cls='mdc-deprecated-list action-list'
            ),
            cls='setting-section main-setting-section'
        )


class ContestFinishedCard(Div):
    def __init__(self, **context):

        contest = context['contest']
        user = context['user']
        is_voter = context.get('voter', None)

        about = Markdown(escape(contest.about or '')) if contest.about else ''
        super().__init__(
            H4(contest.name, style='word-break: break-word'),
            Div(
                Img(src=contest.picture.url, alt='contest-picture', width="100%"),
                style='padding: 32px',
            ) if contest.picture else '',
            Div(
                about,
                style='padding: 12px; word-break: break-word;',
                cls='subtitle-2'
            ),
            Ul(
                CastVoteAction(**context)
                if is_voter else None,
                ReactiveResultAction(contest, user),
                cls='mdc-deprecated-list action-list'
            ),
            cls='setting-section main-setting-section'
        )


@template('djelectionguard/contest_fingerprint.html', Document, Card)
class ContestFingerprint(Div):
    def to_html(self, *content, view, **context):
        contest = context['contest']
        return super().to_html(
            H4(_('Election fingerprint')),
            Div(
                P(
                    _('This fingerprint is unique to this election'
                      ' and can be used to verify its integrity.')
                ),
                Span(
                    Markdown(_('**Election Hash - SHA512:**')),
                    Markdown(contest.get_tldr_fingerprint()),
                    cls='body-2 fingerprints',
                    style='font-size: 20px'
                ),
                Div(
                    Markdown(_('**Election Details:**')),
                    Markdown(self.build_markdown_list(contest.get_fingerprints())),
                    cls='body-2 fingerprints',
                    style='font-size: 18px'
                ),
            ),
            cls='card'
        )

    def build_markdown_list(self, fingerprint, lvl=0):
        """ Recursive function to display JSON content in markdown list format """
        content = ''
        indent = lvl * '\t'
        items = fingerprint.items() if isinstance(fingerprint, dict) else enumerate(fingerprint)
        for k, v in items:
            if isinstance(v, dict) or isinstance(v, list):
                content += f"{indent}- **{k}:**" + '\n'
                content += self.build_markdown_list(v, lvl + 1)
            else:
                content += f"{indent}- **{k}:**" + '\n'
                content += f"{indent}{v}" + '\n'
        return content.replace('\n\n', '\n')


@template('djelectionguard/contest_list.html', Document, Card)
class ContestList(Div):
    def to_html(self, *content, view, **context):
        site = Site.objects.get_current()
        can_create = (
            site.all_users_can_create
            or view.request.user.is_staff
            or view.request.user.is_superuser
        ) and not settings.CSE
        return super().to_html(
            H4(_('Elections'), style='text-align: center;'),
            # ContestFilters(view),
            Ul(
                ListItem(ContestListCreateBtn())
                if can_create else None,
                *(
                    ContestListItem(contest, **context)
                    for contest in context['contest_list']
                ) if len(context['contest_list'])
                else (
                    Li(
                        _('There are no elections yet'),
                        cls='mdc-deprecated-list-item body-1',
                        style='cursor: default'
                    ),
                ),
                cls='mdc-deprecated-list contest-list'
            ),
            cls='card'
        )


@template('djelectionguard/contest_form.html', Document, Card)
class ContestCreateCard(Div):
    style = dict(cls='card')

    def to_html(self, *content, view, form, **context):
        self.backlink = BackLink(_('back'), reverse('contest_list'))

        edit = view.object is not None
        return super().to_html(
            ContestFormComponent(view, form, edit),
        )


class ContestMainSection(ReactiveComponentMixin, Div):
    def __init__(self, *args, **context):
        contest = context['contest']
        self.context = self.get_context_data(**context)
        self.register = f'{contest.id}:main_section'

        if contest.plaintext_tally or contest.decrypting:
            main_section = ContestFinishedCard(**self.context) 
        elif contest.actual_start:
            main_section = ContestVotingCard(**self.context)
        else:
            main_section = ContestSettingsCard(**self.context)

        super().__init__(main_section)

    def get_context_data(self, **context):
        contest = context['contest']
        user = context['user']
        voter = contest.voter_set.filter(user=user).first()
        context['guardian'] = contest.guardian_set.filter(user=user).first()
        context['scrutator'] = contest.scrutator_set.filter(user=user).first()
        context['quorum_uploaded'] = contest.guardian_set.filter(uploaded=None).count() < contest.quorum
        context['num_candidate'] = contest.candidate_set.count()
        context['num_voter'] = contest.voter_set.count()
        context['num_token'] = contest.idtoken_set.count()
        context['casted_at'] = voter.casted_at if voter else None
        context['voter'] = voter
        context['can_vote'] = (
            contest.actual_start
            and not contest.actual_end
            and voter
            and not voter.casted_at
        )
        return context


@template('djelectionguard/contest_detail.html', Document)
class ContestCard(Main):
    def to_html(self, *content, view, **context):
        contest = context['contest']

        main_section = ContestMainSection(view=view, **context)
        context = main_section.context
        action_section = Div(
            main_section,
            TezosSecuredCard(contest, view.request.user),
            cls='main-container')
        sub_section = settings.SHOW_CANDIDATES_LIST and Div(
            CandidatesSettingsCard(**context),
            cls='side-container') or Div(cls='side-container')

        is_mediator = contest.mediator == view.request.user
        is_guardian = context['guardian']
        is_scrutator = context['scrutator']
        if is_mediator or is_guardian or is_scrutator:
            sub_section.addchild(GuardiansSettingsCard(**context))
            if settings.HAS_SCRUTATOR:
                sub_section.addchild(ScrutatorsSettingsCard(**context))
            sub_section.addchild(VotersSettingsCard(**context))

        return super().to_html(
            Script(src=Static('ryzom.js')),
            Script(mark_safe(view.get_token())),
            Div(
                Div(
                    BackLink(_('my elections'), reverse('contest_list')),
                    cls='main-container'),
                Div(cls='side-container'),
                action_section,
                sub_section,
                cls='flex-container'
            ),
            view=view
        )


@template('contest_open', Document, Card)
class ContestOpenCard(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))

        bank = User.objects.get_or_create(email='bank@elictis.io')[0]
        if contest.electioncontract.sender.owner == bank:
            if contest.auto_mode:
                button = MDCButton(_('schedule'), type='submit', id='form_submit_id')
            else:
                button = MDCButton(_('open'), type='submit', id='form_submit_id')
        else:
            if contest.electioncontract.contract_address:
                button = TezosSelfOpen(_('open'))
                button.attrs.update(dict(
                    data_tznet=contest.electioncontract.blockchain.name.lower(),
                    data_manifest_url=contest.manifest_url,
                    data_manifest_hash=contest.manifest_sha512,
                    data_contract_address=contest.electioncontract.contract_address
                ))
            else:
                button = Span(_('No contract address found, please retry later'))

        self.open_text = str(_('open'))
        self.schedule_text = str(_('schedule'))
        self.auto_open_url = reverse('contest_auto_open', args=[contest.id])
        self.csrf_token = view.request.META.get('CSRF_COOKIE', None)

        return super().to_html(
            Script(src='/static/js/walletbeacon.min.js'),
            Script(src='/static/js/taquito.min.js'),
            H4(_('Open the election for voting'), cls='center-text'),
            Div(
                P(_(
                    'Once you open the election for voting'
                    ' you can’t make changes to it.'
                )),
                cls='center-text'
            ),
            Form(
                context['form']['open_email_title'],
                context['form']['open_email'],
                Div(
                    context['form']['send_open_email'],
                    context['form']['auto_mode'],
                    style=dict(
                        display='flex',
                        flex_flow='column',
                        align_items='end',
                    ),
                    cls='flex-container'
                ),
                CSRFInput(view.request),
                button,
                method='POST',
                cls='form'
            ),
            cls='card'
        )

    def py2js(self):
        async def onchange_toggle(event):
            span = document.createElement('span')
            if event.target.checked:
                span.innerHTML = self.schedule_text
            else:
                fetch(self.auto_open_url, {
                    method: 'POST',
                    headers: {
                        'X-CSRFToken': self.csrf_token
                    },
                    body: JSON.stringify({
                        auto_mode: event.target.checked
                    })
                }).then(lambda response: response.json())
                span.innerHTML = self.open_text

            submit_btn = document.querySelector('#form_submit_id')
            submit_btn.innerText = span.innerText

        def assign_event():
            auto_mode_toggle = document.querySelector('#id_auto_mode')
            auto_mode_toggle.addEventListener('change', onchange_toggle)

        window.onload = assign_event


@template('contest_close', Document, Card)
class ContestCloseCard(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))

        self.csrf_token = view.request.META.get('CSRF_COOKIE', None)
        self.close_text = str(_('close the election now'))
        self.schedule_text = str(_('closing scheduled'))
        self.auto_close_url = reverse('contest_auto_close', args=[contest.id])

        bank = User.objects.get_or_create(email='bank@elictis.io')[0]
        if contest.electioncontract.sender.owner == bank:
            if contest.auto_mode:
                button = MDCButtonOutlined(self.schedule_text, False, type='submit', disabled=True)
            else:
                button = MDCButtonOutlined(self.close_text, False, type='submit')

        else:
            button = TezosSelfClose(_('close the election now'), False)
            button.attrs.update(dict(
                data_tznet=contest.electioncontract.blockchain.name.lower(),
                data_contract_address=contest.electioncontract.contract_address
            ))

        return super().to_html(
            Script(src='/static/js/walletbeacon.min.js'),
            H4(_('Manual closing of the election'), cls='center-text'),
            Div(
                P(_(
                    'This will stop the voting process'
                    ' and it can\'t be undone.'
                )),
                cls='center-text body-2'),
            Form(
                CSRFInput(view.request),
                Div(
                    context['form'],
                    button,
                    style='margin: 0 auto; width: fit-content',
                    cls='red-button-container'),
                method='POST',
                cls='form'),
            cls='card',
        )

    def py2js(self):
        async def onchange_toggle(event):
            submit_btn = document.querySelector('[type=submit]')
            span = document.createElement('span')
            if event.target.checked:
                span.innerHTML = self.schedule_text
                submit_btn.disabled = True
            else:
                span.innerHTML = self.close_text
                submit_btn.disabled = False

            response = await fetch(self.auto_close_url, {
                method: 'POST',
                headers: {
                    'X-CSRFToken': self.csrf_token
                },
                body: new.FormData(document.querySelector('form'))
            })

            content = await response.json()
            if content.status == 'error':
                location.reload()

            submit_btn.innerText = span.innerText

        def assign_event():
            auto_mode_toggle = document.querySelector('#id_auto_mode')
            auto_mode_toggle.addEventListener('change', onchange_toggle)

        window.onload = assign_event


@template('contest_decrypt', Document, Card)
class ContestDecryptCard(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))
        return super().to_html(
            H4(_('Open ballot box'), cls='center-text'),
            Div(
                P(_(
                    'This process will erase all guardian keys'
                    ' from server memory.'
                )),
                cls='center-text body-2'),
            Form(
                context['form']['email_title'],
                context['form']['email_message'],
                MDCMultipleChoicesCheckbox(
                    'send_email',
                    ((0, B(_('Do not alert voters by email')), 'true'),),
                    n=1
                ),
                CSRFInput(view.request),
                MDCButton(_('open and view results')),
                method='POST',
                cls='form'),
            cls='card',
        )


@template('contest_publish', Document, Card)
class ContestPublishCard(Div):
    def to_html(self, *content, view, form, **ctx):
        field_val = 'internal'
        if not view.object.electioncontract.is_internal:
            if view.object.artifacts_ipfs:
                field_val = 'blockchain'
                contract = view.object.electioncontract
                button = TezosSelfPublish(
                    _('Publish on blockchain'),
                    data_contest_url=view.object.get_absolute_url(),
                    data_tznet=contract.blockchain.name.lower(),
                    data_asset_address=view.object.electioncontract.nft_contract,
                    data_contract_address=contract.contract_address,
                    data_artifacts_hash=view.object.artifacts_sha512,
                    data_artifacts_url=view.object.artifacts_ipfs_url,
                )
            else:
                field_val = 'IPFS'
                button = MDCButton(_('Publish on IPFS'))
        else:
            button = MDCButton(_('publish results'))

        return super().to_html(
            Script(src='/static/js/walletbeacon.min.js'),
            Script(src='/static/js/taquito.min.js'),
            H4(_('Publish your election results'), cls='center-text'),
            Div(
                P(_('This will decentralize your election results.')),
                cls='center-text body-2'),
            Form(
                Input(type='hidden', name='publish', value=field_val),
                CSRFInput(view.request),
                Div(
                    button,
                    style='width: fit-content; margin: 0 auto;'
                ),
                method='POST',
                cls='form'),
            cls='card',
        )


@template('contest_result', Document, Card)
class ContestResultCard(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))

        votes = contest.candidate_set.aggregate(total=Sum('score'))

        table_head_row = Tr(cls='mdc-data-table__header-row')
        kwargs = dict(
            role='columnheader',
            scope='col',
            cls='mdc-data-table__header-cell overline'
        )
        table_head_row.addchild(Th('candidate', colspan=3, **kwargs))

        kwargs['style'] = 'text-align: right;'
        table_head_row.addchild(Th('votes', **kwargs))

        table_content = Tbody(cls='mdc-data-table__content')
        cls = 'mdc-data-table__cell'

        def create_cell(content, **kw):
            if 'style' not in kw:
                kw['style'] = dict()
            kw['style'].update(dict(
                word_break='break-word',
                white_space='normal'
            ))

            return Td(content, cls=cls, **kw)

        for i, candidate in enumerate(
            contest.candidate_set.order_by(settings.CANDIDATE_ORDER_BY)
        ):
            candidate_row = Tr(cls='mdc-data-table__row')
            candidate_row.addchild(
                create_cell(f'{i + 1}. ', style=dict(width='50px'))
            )

            if candidate.picture:
                candidate_row.addchild(create_cell(
                    Div(style=dict(
                        background_image=f'url({candidate.picture.url})',
                        background_position='center',
                        background_size='cover',
                        width='50px',
                        height='50px',
                        border_radius='50%',
                        margin='12px',
                        flex_shrink=1
                    )),
                    style=dict(width='74px')
                ))
            else:
                candidate_row.addchild(create_cell('', style=dict(width=0)))

            candidate_row.addchild(create_cell(candidate))

            if contest.exact_scores:
                score_percent = candidate.score
            else:
                if votes['total']:
                    score_percent = 100 * candidate.score / votes['total']
                    score_percent = f'{round(score_percent, 2)} %'
                else:
                    score_percent = '--'

            candidate_row.addchild(create_cell(
                Span(f'{score_percent}', cls='text-btn'),
                style=dict(text_align='right')
            ))

            table_content.addchild(candidate_row)

        score_table = Table(
            Thead(table_head_row),
            table_content,
            **{
                'class': 'mdc-data-table__table',
                'aria-label': 'Scores'
            }
        )

        publish_btn = ''
        if (
            contest.publish_state == contest.PublishStates.ELECTION_DECRYPTED
            and contest.mediator == view.request.user
        ):
            publish_btn = MDCButton(
                _('publish results'),
                p=True,
                icon=WorldIcon(),
                tag='a',
                href=reverse('contest_publish', args=[contest.id]),
                style='margin: 0 auto;')

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        button = DownloadBtnOutlined(
            _('Download'),
            'file_download',
            tag='a',
            href='download',
            data_filename=f'{contest.name}-scores.csv',
            data_err_msg=_('Server error')
        )

        if view.request.user == contest.mediator and settings.REFERENDUM:
            button = MDCButton(
                _('Download PDF'),
                tag='a',
                href=reverse('contest_referendum', args=[contest.id])
            )

        return super().to_html(
            H4(_('Results'), cls='center-text'),
            Div(
                H5(contest.name, style='word-break: break-word'),
                Div(
                    about,
                    style='padding: 12px; word-break: break-word;',
                    cls='subtitle-2'
                ),
                publish_btn,
                score_table,
                button,
                cls='table-container score-table center-text'
            ),
            ArtifactsLinks(contest),
            cls='card',
        )
