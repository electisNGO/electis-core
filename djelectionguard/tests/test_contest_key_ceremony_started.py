import io
import pytest

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client
from django.utils import timezone
from django.urls import reverse

from ryzom_django_channels.models import Publication
from electeez_auth.models import User
from electeez_sites.models import Site
from electeez_consent.models import Provider
from djelectionguard.models import Contest, Guardian, Scrutator, Voter
from djtezos.models import Blockchain, Account
from djelectionguard_tezos.models import ElectionContract


@pytest.fixture(autouse=True)
def setup(db):
    Publication.objects.create(name='guardians', model_module='djelectionguard.models', model_class='Guardian')
    Publication.objects.create(name='voters', model_module='djelectionguard.models', model_class='Voter')
    Provider.objects.create(id=1, name='test', public=True, provider_class='electeez_consent.manual.Provider')
    Site.objects.create(id=1, domain='testserver', name='test.com')


@pytest.fixture
def admin(db):
    return User.objects.create_user(
        email='admin@test.com',
        password='admin',
        is_staff=True,
        is_superuser=True,
        is_active=True
    )


@pytest.fixture
def external(db):
    return User.objects.create_user(
        email='external@test.com',
        password='external',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )


def contest_data():
    now = timezone.now() + timezone.timedelta(days=1)
    tomorrow = now + timezone.timedelta(days=1)

    return {
        'name': 'Test Contest',
        'about': 'about contest',
        'votes_allowed': 1,
        'start_0': now.date(),
        'start_1': now.time(),
        'end_0': tomorrow.date(),
        'end_1': tomorrow.time(),
        'consent_provider': 1,
        'timezone': 'Europe/Paris',
        'open_email_title': 'title',
        'open_email': 'body',
        'close_email_title': 'title',
        'close_email': 'body'
    }


@pytest.fixture
def blockchain(db):
    blockchain = Blockchain.objects.create(
        name='fake',
        provider_class='djtezos.fake.Provider',
        confirmation_blocks=0,
        is_active=True,
        endpoint='http://localhost:1337',
        explorer='http://localhost:1337/',
    )
    blockchain.account_set.create(
        name='test',
        owner=(User.objects.get_or_create(email='bank@elictis.io')[0]),
        address='tz1Z9g1WxX1xXZC9ZLz9LH9z9z9z9z9z9z9z',
        balance=1000,
    )
    return blockchain


@pytest.fixture
def contract(blockchain, contest):
    account = blockchain.account_set.first()
    contract = ElectionContract.objects.create(
        sender=account,
        election=contest,
        is_valid=True,
        state='deploy'
    )
    return contract


@pytest.fixture
@pytest.mark.django_db
def contest(admin):
    data = contest_data()
    del data['start_0']
    del data['start_1']
    del data['end_0']
    del data['end_1']
    del data['consent_provider']
    contest = Contest.objects.create(
        mediator=admin,
        start=timezone.now() + timezone.timedelta(days=1),
        end=timezone.now() + timezone.timedelta(days=2),
        consent_provider=Provider.objects.get(id=1),
        **data
    )
    return contest


def scrutator_data():
    return {
        'email': 'scrutator2@test.com'
    }


@pytest.fixture
@pytest.mark.django_db
def scrutator(contest):
    scrutator_user = User.objects.create_user(
        email=scrutator_data()['email'],
        password='scrutator',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )
    return Scrutator.objects.create(
        contest=contest,
        user=scrutator_user,
    )


def guardian_data():
    return {
        'email': 'guardian2@test.com'
    }


@pytest.fixture
@pytest.mark.django_db
def guardian(contest):
    guardian_user = User.objects.create_user(
        email='guardian@test.com',
        password='guardian',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )
    guardian = Guardian.objects.create(
        contest=contest,
        user=guardian_user,
        sequence=1,
    )

    contest.key_ceremony()

    return guardian


def get(user, url):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.get(url)
    return response


def post(user, url, **data):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.post(url, data)
    return response


def test_admin_can_access_contract(contest, admin):
    response = get(admin, reverse('electioncontract_create', args=[contest.id]))
    assert response.status_code == 200


def test_admin_can_create_contract(contest, admin, blockchain):
    get(admin, reverse('electioncontract_create', args=[contest.id]))
    response = post(admin, reverse('electioncontract_create', args=[contest.id]), blockchain=blockchain.id)
    assert response.status_code == 302
    assert contest.electioncontract


def test_guardian_cannot_access_contract(contest, guardian):
    response = get(guardian.user, reverse('electioncontract_create', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_create_contract(contest, guardian, blockchain):
    response = post(guardian.user, reverse('electioncontract_create', args=[contest.id]), blockchain=blockchain.id)
    assert response.status_code == 404
    assert not hasattr(contest, 'electioncontract')


def test_guardian_can_see_contest(contest, guardian):
    response = get(guardian.user, reverse('contest_detail', args=[contest.id]))
    assert response.status_code == 200


def test_guardian_cannot_access_contest_update(contest, guardian):
    response = get(guardian.user, reverse('contest_update', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_can_see_guardian_list(contest, guardian):
    response = get(guardian.user, reverse('contest_guardian_list', args=[contest.id]))
    assert response.status_code == 200


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_can_see_scrutator_list(contest, guardian):
    response = get(guardian.user, reverse('contest_scrutator_list', args=[contest.id]))
    assert response.status_code == 200


def test_guardian_can_see_voter_list(contest, guardian):
    response = get(guardian.user, reverse('contest_voters_detail', args=[contest.id]))
    assert response.status_code == 200


def test_guardian_cannot_access_voter_update(contest, guardian):
    response = get(guardian.user, reverse('contest_voters_update', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_create_guardian(contest, guardian):
    response = post(guardian.user, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


def test_guardian_cannot_delete_guardian(contest, guardian):
    response = post(guardian.user, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_cannot_create_scrutator(contest, guardian):
    response = post(guardian.user, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 0


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_cannot_delete_scrutator(contest, guardian, scrutator):
    response = post(guardian.user, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 1


def test_mediator_cannot_create_guardian(admin, contest, guardian):
    response = post(admin, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


def test_mediator_cannot_delete_guardian(admin, contest, guardian):
    response = post(admin, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_create_scrutator(admin, contest, guardian):
    response = post(admin, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 302


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_delete_scrutator(admin, contest, guardian, scrutator):
    response = post(admin, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 302
    assert Scrutator.objects.filter(contest=contest).count() == 0


def test_guardian_can_download_key(contest, guardian):
    response = get(guardian.user, reverse('contest_guardian_download', args=[contest.id, guardian.id]))
    assert response.status_code == 200


def test_guardian_can_download_then_upload_key(contest, admin, guardian, contract):
    response = get(guardian.user, reverse('contest_guardian_download', args=[contest.id, guardian.id]))
    assert response.status_code == 200

    # create bytesIO object from response.streaming_content
    data = io.BytesIO()
    for chunk in response.streaming_content:
        data.write(chunk)

    # create SimpleUploadedFile object from bytesIO object
    key = SimpleUploadedFile('key.pkl', data.getvalue(), content_type='application/octet-stream')
    response = post(guardian.user, reverse('contest_guardian_verify', args=[contest.id, guardian.id]), pkl_file=key)
    assert response.status_code == 302

    response = get(admin, reverse('contest_open', args=[contest.id]))
    assert response.status_code == 200

    contest.candidate_set.create(name='Alice', sequence=1)
    contest.candidate_set.create(name='Bob', sequence=2)
    response = post(admin, reverse('contest_open', args=[contest.id]), open_email_title='title', open_email='body', send_open_email=False, auto_mode=False)
    assert response.status_code == 302
    contest.refresh_from_db()
    assert contest.actual_start
