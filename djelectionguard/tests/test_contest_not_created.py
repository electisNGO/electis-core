import pytest

from django.test import Client
from django.utils import timezone
from django.urls import reverse

from electeez_auth.models import User
from electeez_sites.models import Site
from electeez_consent.models import Provider
from djelectionguard.models import Contest, Guardian, Scrutator, Voter


@pytest.fixture(autouse=True)
def setup(db):
    Provider.objects.create(id=1, name='test', public=True, provider_class='electeez_consent.manual.Provider')
    Site.objects.create(id=1, domain='testserver', name='test.com')


@pytest.fixture
def admin(db):
    return User.objects.create_user(
        email='admin@test.com', 
        password='admin',
        is_staff=True,
        is_superuser=True,
        is_active=True
    )


@pytest.fixture
def external(db):
    return User.objects.create_user(
        email='external@test.com',
        password='external',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )


def contest_data():
    now = timezone.now() + timezone.timedelta(days=1)
    tomorrow = now + timezone.timedelta(days=1)

    return {
        'name': 'Test Contest',
        'about': 'about contest',
        'number_elected': 2,
        'votes_allowed': 2,
        'start_0': now.date(),
        'start_1': now.time(),
        'end_0': tomorrow.date(),
        'end_1': tomorrow.time(),
        'timezone': 'Europe/Paris',
        'consent_provider': 1,
        'open_email_title': 'title',
        'open_email': 'body',
        'close_email_title': 'title',
        'close_email': 'body',
    }


def get(user, url):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.get(url)
    return response


def post(user, url, **data):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.post(url, data)
    return response


def test_contest_not_created(admin):
    assert Contest.objects.count() == 0


def test_admin_can_access_contest_list(admin):
    response = get(admin, 'contest_list')
    assert response.status_code == 200, response.status_code


def test_external_can_access_contest_list(external):
    response = get(external, 'contest_list')
    assert response.status_code == 200, response.status_code


def test_admin_can_access_contest_create(admin):
    response = get(admin, 'contest_create')
    assert response.status_code == 200, response.status_code


def test_external_cannot_access_contest_create(external):
    response = get(external, 'contest_create')
    assert response.status_code == 302, response.status_code


def test_admin_can_create_contest(admin):
    response = post(admin, 'contest_create', **contest_data())
    assert response.status_code == 302, response.status_code
    assert Contest.objects.count() == 1


def test_external_cannot_create_contest(external):
    response = post(external, 'contest_create', **contest_data())
    assert response.status_code == 302, response.status_code
