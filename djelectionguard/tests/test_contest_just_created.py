import pytest

from django.conf import settings
from django.test import Client
from django.utils import timezone
from django.urls import reverse

from ryzom_django_channels.models import Publication
from electeez_auth.models import User
from electeez_sites.models import Site
from electeez_consent.models import Provider
from djelectionguard.models import Contest, Guardian, Scrutator, Voter


@pytest.fixture(autouse=True)
def setup(db):
    Publication.objects.create(name='guardians', model_module='djelectionguard.models', model_class='Guardian')
    Publication.objects.create(name='voters', model_module='djelectionguard.models', model_class='Voter')
    Provider.objects.create(id=1, name='test', public=True, provider_class='electeez_consent.manual.Provider')
    Site.objects.create(id=1, domain='testserver', name='test.com')


@pytest.fixture
def admin(db):
    return User.objects.create_user(
        email='admin@test.com',
        password='admin',
        is_staff=True,
        is_superuser=True,
        is_active=True
    )


@pytest.fixture
def external(db):
    return User.objects.create_user(
        email='external@test.com',
        password='external',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )


def contest_data():
    now = timezone.now() + timezone.timedelta(days=1)
    tomorrow = now + timezone.timedelta(days=1)

    return {
        'name': 'Test Contest',
        'about': 'about contest',
        'votes_allowed': 2,
        'start_0': now.date(),
        'start_1': now.time(),
        'end_0': tomorrow.date(),
        'end_1': tomorrow.time(),
        'consent_provider': 1,
        'timezone': 'Europe/Paris',
        'open_email_title': 'title',
        'open_email': 'body',
        'close_email_title': 'title',
        'close_email': 'body'
    }


@pytest.fixture
@pytest.mark.django_db
def contest(admin):
    data = contest_data()
    del data['start_0']
    del data['start_1']
    del data['end_0']
    del data['end_1']
    del data['consent_provider']
    return Contest.objects.create(
        mediator=admin,
        start=timezone.now() + timezone.timedelta(days=1),
        end=timezone.now() + timezone.timedelta(days=2),
        consent_provider=Provider.objects.get(id=1),
        **data
    )


def get(user, url):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.get(url)
    return response


def post(user, url, **data):
    client = Client()
    client.force_login(user)
    url = url if '/' in url else reverse(url)
    response = client.post(url, data)
    return response


def test_mediator_can_see_contest(contest, admin):
    response = get(admin, reverse('contest_detail', args=[contest.id]))
    assert response.status_code == 200


def test_external_cannot_see_contest(contest, external):
    response = get(external, reverse('contest_detail', args=[contest.id]))
    assert response.status_code == 404


def test_mediator_can_access_contest_update(contest, admin):
    response = get(admin, reverse('contest_update', args=[contest.id]))
    assert response.status_code == 200


def test_external_cannot_access_contest_update(contest, external):
    response = get(external, reverse('contest_update', args=[contest.id]))
    assert response.status_code == 404


def test_mediator_can_update_contest(contest, admin):
    data = contest_data()
    data['name'] = 'New Contest Name'
    response = post(admin, reverse('contest_update', args=[contest.id]), **data)
    assert response.status_code == 302
    assert Contest.objects.get(id=contest.id).name == 'New Contest Name'


def test_external_cannot_update_contest(contest, external):
    data = contest_data()
    data['name'] = 'New Contest Name'
    response = post(external, reverse('contest_update', args=[contest.id]), **data)
    assert response.status_code == 404
    assert Contest.objects.get(id=contest.id).name == 'Test Contest'


def test_mediator_can_see_guardian_list(contest, admin):
    response = get(admin, reverse('contest_guardian_list', args=[contest.id]))
    assert response.status_code == 404


def test_external_cannot_see_guardian_list(contest, external):
    response = get(external, reverse('contest_guardian_list', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_see_scrutator_list(contest, admin):
    response = get(admin, reverse('contest_scrutator_list', args=[contest.id]))
    assert response.status_code == 200


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_external_cannot_see_scrutator_list(contest, external):
    response = get(external, reverse('contest_scrutator_list', args=[contest.id]))
    assert response.status_code == 404


def test_mediator_can_see_voter_list(contest, admin):
    response = get(admin, reverse('contest_voters_detail', args=[contest.id]))
    assert response.status_code == 200


def test_external_cannot_see_voter_list(contest, external):
    response = get(external, reverse('contest_voters_detail', args=[contest.id]))
    assert response.status_code == 404


def test_mediator_can_see_voter_update(contest, admin):
    response = get(admin, reverse('contest_voters_update', args=[contest.id]))
    assert response.status_code == 200


def test_external_cannot_see_voter_update(contest, external):
    response = get(external, reverse('contest_voters_update', args=[contest.id]))
    assert response.status_code == 404


def voter_data():
    voters_emails = [
        'voter1@test.com',
        'voter2@test.com'
    ]
    return '\n'.join(voters_emails)


def test_mediator_can_update_voter(contest, admin):
    response = post(admin, reverse('contest_voters_update', args=[contest.id]), voters_emails=voter_data())
    assert response.status_code == 302
    assert Voter.objects.filter(contest=contest).count() == 2


def test_external_cannot_update_voter(contest, external):
    response = post(external, reverse('contest_voters_update', args=[contest.id]), voters_emails=voter_data())
    assert response.status_code == 404
    assert Voter.objects.filter(contest=contest).count() == 0


def test_mediator_can_see_guardian_create(contest, admin):
    response = get(admin, reverse('contest_guardian_create', args=[contest.id]))
    assert response.status_code == 200


def test_external_cannot_see_guardian_create(contest, external):
    response = get(external, reverse('contest_guardian_create', args=[contest.id]))
    assert response.status_code == 404


def guardian_data():
    return {'email': 'guardian@test.com'}


@pytest.fixture
@pytest.mark.django_db
def guardian(contest):
    guardian_user = User.objects.create_user(
        email=guardian_data()['email'],
        password='guardian',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )
    return Guardian.objects.create(
        contest=contest,
        user=guardian_user,
        sequence=1,
    )


def test_mediator_can_create_guardian(contest, admin):
    response = post(admin, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 302
    assert Guardian.objects.filter(contest=contest).count() == 1


def test_external_cannot_create_guardian(contest, external):
    response = post(external, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 0


def test_mediator_can_delete_guardian(contest, admin, guardian):
    response = post(admin, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 302
    assert Guardian.objects.filter(contest=contest).count() == 0


def test_external_cannot_delete_guardian(contest, external, guardian):
    response = post(external, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_see_scrutator_create(contest, admin):
    response = get(admin, reverse('contest_scrutator_create', args=[contest.id]))
    assert response.status_code == 200


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_external_cannot_see_scrutator_create(contest, external):
    response = get(external, reverse('contest_scrutator_create', args=[contest.id]))
    assert response.status_code == 404


def scrutator_data():
    return {'email': 'scrutator@test.com'}


@pytest.fixture
@pytest.mark.django_db
def scrutator(contest):
    scrutator_user = User.objects.create_user(
        email=scrutator_data()['email'],
        password='scrutator',
        is_staff=False,
        is_superuser=False,
        is_active=True
    )
    return Scrutator.objects.create(
        contest=contest,
        user=scrutator_user,
    )


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_create_scrutator(contest, admin):
    response = post(admin, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 302
    assert Scrutator.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_external_cannot_create_scrutator(contest, external):
    response = post(external, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 0


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_mediator_can_delete_scrutator(contest, admin, scrutator):
    response = post(admin, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 302
    assert Scrutator.objects.filter(contest=contest).count() == 0


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_external_cannot_delete_scrutator(contest, external, scrutator):
    response = post(external, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 1


def test_guardian_cannot_see_contest(contest, guardian):
    response = get(guardian.user, reverse('contest_detail', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_access_contest_update(contest, guardian):
    response = get(guardian.user, reverse('contest_update', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_see_guardian_list(contest, guardian):
    response = get(guardian.user, reverse('contest_guardian_list', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_cannot_see_scrutator_list(contest, guardian):
    response = get(guardian.user, reverse('contest_scrutator_list', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_see_voter_list(contest, guardian):
    response = get(guardian.user, reverse('contest_voters_detail', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_access_voter_update(contest, guardian):
    response = get(guardian.user, reverse('contest_voters_update', args=[contest.id]))
    assert response.status_code == 404


def test_guardian_cannot_create_guardian(contest, guardian):
    response = post(guardian.user, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


def test_guardian_cannot_delete_guardian(contest, guardian):
    response = post(guardian.user, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_cannot_create_scrutator(contest, guardian):
    response = post(guardian.user, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 0


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_guardian_cannot_delete_scrutator(contest, guardian, scrutator):
    response = post(guardian.user, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_see_contest(contest, scrutator):
    response = get(scrutator.user, reverse('contest_detail', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_access_contest_update(contest, scrutator):
    response = get(scrutator.user, reverse('contest_update', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_see_guardian_list(contest, scrutator):
    response = get(scrutator.user, reverse('contest_guardian_list', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_see_scrutator_list(contest, scrutator):
    response = get(scrutator.user, reverse('contest_scrutator_list', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_see_voter_list(contest, scrutator):
    response = get(scrutator.user, reverse('contest_voters_detail', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_access_voter_update(contest, scrutator):
    response = get(scrutator.user, reverse('contest_voters_update', args=[contest.id]))
    assert response.status_code == 404


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_create_guardian(contest, scrutator):
    response = post(scrutator.user, reverse('contest_guardian_create', args=[contest.id]), quorum=1, **guardian_data())
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 0


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_delete_guardian(contest, scrutator, guardian):
    response = post(scrutator.user, reverse('contest_guardian_delete', args=[contest.id, guardian.id]))
    assert response.status_code == 404
    assert Guardian.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_create_scrutator(contest, scrutator):
    response = post(scrutator.user, reverse('contest_scrutator_create', args=[contest.id]), **scrutator_data())
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 1


@pytest.mark.skipif(settings.HAS_SCRUTATOR is False, reason='HAS_SCRUTATOR is False')
def test_scrutator_cannot_delete_scrutator(contest, scrutator):
    response = post(scrutator.user, reverse('contest_scrutator_delete', args=[contest.id, scrutator.id]))
    assert response.status_code == 404
    assert Scrutator.objects.filter(contest=contest).count() == 1
