# Generated by Django 3.2.7 on 2021-12-06 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djelectionguard', '0031_auto_20211206_1154'),
    ]

    operations = [
        migrations.AddField(
            model_name='contest',
            name='close_email_title',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='contest',
            name='open_email_title',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
