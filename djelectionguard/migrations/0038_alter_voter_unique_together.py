# Generated by Django 3.2.12 on 2022-03-22 15:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('djelectionguard', '0037_auto_20220322_0648'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='voter',
            unique_together={('phone_hash', 'contest_id')},
        ),
    ]
