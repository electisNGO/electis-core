""" Data migration to ensure contest start date is before end date. """
from django.db import migrations
from django.db.models import F

from djelectionguard.models import Contest


def start_before_end(apps, schema_editor):
    """ Ensure contest start date is before end date. """
    Contest.objects.filter(start__gt=F('end')).update(start=F('end'), end=F('start'))


class Migration(migrations.Migration):
    """ Data migration to ensure contest start date is before end date. """

    dependencies = [
        ('djelectionguard', '0056_auto_20240307_1138'),
    ]

    operations = [
        migrations.RunPython(start_before_end),
    ]
