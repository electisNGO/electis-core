from django.conf import settings
from django.urls import include, path

from djelectionguard.voter.views import verify_voter_nir

urlpatterns = [
    path('', include('djelectionguard.contest.urls')),
    path('nir/', verify_voter_nir, name='voter_nir'),
    path('<uuid:pk>/guardian/', include('djelectionguard.guardian.urls')),
    path('<uuid:pk>/candidate/', include('djelectionguard.candidate.urls')),
    path('<uuid:pk>/voter/', include('djelectionguard.voter.urls'))
]

if settings.HAS_SCRUTATOR:
    urlpatterns.append(path('<uuid:pk>/scrutator/', include('djelectionguard.scrutator.urls')))
