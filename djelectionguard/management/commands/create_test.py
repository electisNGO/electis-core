from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from djtezos.models import Account
from djelectionguard.models import Contest
from djelectionguard_tezos.models import ElectionContract
from electeez_auth.models import User


class Command(BaseCommand):
    help = 'Tally & decrypt an election'

    def add_arguments(self, parser):
        parser.add_argument('--name', type=str)
        parser.add_argument('--mediator', type=str)
        parser.add_argument('--ncandidate', type=int)
        parser.add_argument('--guardians', type=str)
        parser.add_argument('--quorum', type=int)
        parser.add_argument('--voters', type=str)

    def handle(self, *args, **options):
        name = options.get('name') or 'Test election'

        default_mediator = User.objects.filter(
            is_superuser=True,
            is_active=True
        ).first()
        mediator = User.objects.get(
            email=options.get('mediator', default_mediator)
        )
        print(f'mediator is {mediator.email}')
        ncandidate = options.get('ncandidate') or 2
        guardians = (
            options.get('guardians') or mediator.email
        ).split(',')

        voters = (options.get('voters') or mediator.email).split(',')

        contest = Contest.objects.create(
            name=name,
            mediator=mediator,
            start=datetime.now(),
            end=datetime.now() + timedelta(days=1)
        )
        for voter in voters:
            user, created = User.objects.get_or_create(
                email=voter, is_active=True
            )
            contest.voter_set.create(user=user)

        for guardian in guardians:
            user, created = User.objects.get_or_create(
                email=guardian
            )
            contest.guardian_set.create(user=user)

        for i in range(ncandidate):
            contest.candidate_set.create(name=chr(65 + i))

        ElectionContract.objects.create(
            sender=Account.objects.first(),
            election=contest,
            state='deploy'
        )
        contest.quorum = options.get('quorum') or len(guardians) - 1
        contest.key_ceremony()

        print(f'ID: {contest.id}')
