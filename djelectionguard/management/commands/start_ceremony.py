import json

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from redis import StrictRedis as Client

from djelectionguard.permissions import (
    ContestPermissionMixin,
    contest_key_ceremony_not_started,
)
from djelectionguard.models import Contest


class Command(BaseCommand):
    help = 'Start scheduled Key Ceremonies'

    def handle(self, *args, **options):
        print("Starting Key Ceremony")
        class ContestsToOpen(ContestPermissionMixin):
            permission_chain = [
                contest_key_ceremony_not_started,
            ]

        contests_to_open = ContestsToOpen()

        print(contests_to_open)

        for contest in contests_to_open.get_queryset().exclude(kc_start=None):
            print(contest)
            if contest.local_kc_start <= contest.local_now:
                print(f'Starting KC for {contest}')
                contest.key_ceremony()
