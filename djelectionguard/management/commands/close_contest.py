import json

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from redis import StrictRedis as Client

from djelectionguard.permissions import (
    ContestPermissionMixin,
    contest_is_opened,
    contest_is_not_closed,
)
from djelectionguard.models import Contest


class Command(BaseCommand):
    help = 'Close scheduled elections'

    def handle(self, *args, **options):
        print("CLOSING ELECTIONS")
        class ContestsToClose(ContestPermissionMixin):
            permission_chain = [
                contest_is_not_closed,
                contest_is_opened,
            ]
            
        contests_to_close = ContestsToClose()

        for contest in contests_to_close.get_queryset():
            print(contest)
            if contest.auto_mode:
                if contest.local_end <= contest.local_now:
                    print(f'CLOSING CONTEST {contest}')
                    contest.close()
