from django.db.models import Case, When, Value
from django.utils import timezone

from ryzom_django_channels.components import (
    ReactiveComponentMixin,
    SubscribeComponentMixin,
    model_template
)

from djlang.utils import gettext as _

from electeez_auth.models import Token

from djelectionguard.components import *


class GuardianActionButton(CList):
    def __init__(self, guardian, action):
        url = reverse(
            f'contest_guardian_{action}',
            args=[guardian.contest.id, guardian.id]
        )
        if action == 'download':
            btn = DownloadBtn(
                _('Download'),
                'file_download',
                tag='a',
                href=url,
                data_filename=f'guardian-{guardian.id}.pkl',
                data_err_msg=str(_('Server error'))
            )

        elif action in ['verify', 'upload']:
            btn = MDCTextButton(_('Upload'), 'file_upload', tag='a', href=url)

        super().__init__(btn)


class GuardianDeleteBtn(A):
    def __init__(self, guardian):
        self.guardian = guardian

        super().__init__(
            MDCIcon(
                'delete',
                cls='material-icons'),
            tag='a',
            href=reverse(
                'contest_guardian_delete',
                args=[guardian.contest.id, guardian.id]
            )
        )


@model_template('guardian-row')
class ReactiveGuardianRow(Tr):
    def __init__(self, guardian):
        cls = 'mdc-data-table__cell'

        if guardian.is_current_user:
            can_download = (
                not guardian.contest.actual_start
                and not guardian.verified
            )
            can_verify = can_download and guardian.downloaded
            can_upload = (
                guardian.verified
                and guardian.contest.actual_end
                and not guardian.uploaded
                and not guardian.uploaded_erased
                and not guardian.contest.decrypting
            )

            if guardian.verified:
                dl_elem = CheckedIcon()
                v_elem = CheckedIcon()
            else:
                if can_download:
                    dl_elem = GuardianActionButton(guardian, 'download')
                else:
                    dl_elem = _('No')
                if can_verify:
                    v_elem = GuardianActionButton(guardian, 'verify')
                else:
                    v_elem = _('No')

            if guardian.uploaded:
                ul_elem = CheckedIcon()
            else:
                if can_upload:
                    ul_elem = GuardianActionButton(guardian, 'upload')
                else:
                    ul_elem = _('No')

            if not guardian.downloaded:
                v_elem = '--'
                ul_elem = '--'

                if guardian.contest.actual_start:
                    dl_elem = '--'

            super().__init__(
                Td(guardian.user, cls=cls),
                Td(
                    dl_elem,
                    cls=cls + ' center'),
                Td(
                    v_elem,
                    cls=cls + ' center'),
                Td(
                    ul_elem,
                    cls=cls + ' center'),
                cls='mdc-data-table__row',
                id=str(guardian.id) + '-row',
                style=dict(
                    background_color='lemonchiffon'
                )
            )
        else:
            super().__init__(
                Td(guardian.user, cls=cls),
                Td(
                    CheckedIcon() if guardian.downloaded else _('No'),
                    cls=cls + ' center'),
                Td(
                    CheckedIcon() if guardian.verified else _('No'),
                    cls=cls + ' center'),
                Td(
                    CheckedIcon() if guardian.uploaded else _('No'),
                    cls=cls + ' center'),
                cls='mdc-data-table__row',
                id=str(guardian.id) + '-row'
            )


class GuardianTable(SubscribeComponentMixin, Div):
    publication = 'guardians'
    model_template = 'guardian-row'

    def __init__(self, view, **context):
        contest = context['contest']

        headers = (
            _('email'),
            _('key downloaded'),
            _('key verified'),
            _('key uploaded')
        )

        table_head_row = Tr(cls='mdc-data-table__header-row')
        for th in headers:
            table_head_row.addchild(
                Th(
                    th,
                    role='columnheader',
                    scope='col',
                    cls='mdc-data-table__header-cell overline',
                    style='width: 50%'
                    if th == 'email' else 'text-align: center;'
                )
            )

        self.container = Tbody(cls='mdc-data-table__content')
        table = Table(
            Thead(table_head_row),
            self.container,
            **{
                'class': 'mdc-data-table__table',
                'aria-label': 'Guardians'
            }
        )

        super().__init__(
            table,
            cls='table-container guardian-table'
        )

    def to_html(self, *content, **context):
        view = self.get_view(**context)
        self.subscribe_options = dict(
            contest=str(view.object.pk),
        )
        self.reactive_setup(**context)

        return super(Div, self).to_html(*content, **context)

    @classmethod
    def get_queryset(self, user, qs, opts):
        qs = qs.select_related('user').filter(contest__id=opts['contest'])
        qs = qs.annotate(
            is_current_user=Case(
                When(user=user, then=Value(True)),
                default=Value(False)
            ),
            for_contest_mediator=Case(
                When(contest__mediator=user, then=Value(True)),
                default=Value(False)
            )
        )
        return qs


class SecureElectionInner(ReactiveComponentMixin, Span):
    def __init__(self, contest_id, **kwargs):
        obj = Contest.objects.get(id=contest_id)

        self.register = f'{obj.id}:secure_inner'

        user = kwargs.get('user')
        text = _(
            'All guardians must possess a private key so that the'
            ' ballot box is secure and the election can be opened for voting.'
        )
        todo_list = Ol()

        if user == obj.mediator:
            todo_list.addchild(Li('Edit guardians', cls='line'))

        guardian = obj.guardian_set.filter(user=user).first()
        n_confirmed = obj.guardian_set.exclude(verified=None).count()
        quorum_reached = n_confirmed >= obj.quorum

        if guardian:
            cls = 'line' if guardian.downloaded or quorum_reached else 'bold'
            todo_list.addchild(Li(_('Download my private key'), cls=cls))

            cls = ''
            if (
                guardian.downloaded
                and not guardian.verified
                and not quorum_reached
            ):
                cls = 'bold'
            elif guardian.verified or quorum_reached:
                cls = 'line'
            todo_list.addchild(
                Li(
                    _('Confirm possession of an uncompromised private key'),
                    cls=cls
                )
            )

        if user == obj.mediator or obj.scrutator_set.filter(user=user).exists():
            cls = ''
            if guardian and guardian.verified:
                cls = 'bold'
            if quorum_reached:
                cls = 'line'
            todo_list.addchild(
                Li(
                    _(
                        'A minimum of %(quorum)s guardians confirm'
                        ' possession of uncompromised private keys',
                        n=obj.quorum,
                        quorum=obj.quorum
                    ),
                    Br(),
                    _(
                        '%(confirmed)d/%(gardiens)d confirmed',
                        n=n_confirmed,
                        confirmed=n_confirmed,
                        gardiens=obj.quorum
                    ),
                    cls=cls))

            cls = ''
            if quorum_reached:
                cls = 'bold'
            todo_list.addchild(Li(_('Open the election for voting'), cls=cls))

        subtext = _(
            'Guardians must NOT loose their PRIVATE keys and they must keep'
            ' them SECRET.'
        )

        action_btn = None
        if guardian and not guardian.downloaded and not quorum_reached:
            action_btn = MDCButtonOutlined(
                _('download private key'),
                p=False,
                icon='file_download',
                tag='a',
                href=reverse('contest_guardian_list', args=[obj.id]))
        elif guardian and not guardian.verified and not quorum_reached:
            action_btn = MDCButtonOutlined(
                _('confirm key integrity'),
                p=False,
                tag='a',
                href=reverse('contest_guardian_list', args=[obj.id]))
        elif user == obj.mediator:
            if quorum_reached:
                action_btn = MDCButton(
                    _('Open for voting'),
                    tag='a',
                    href=reverse('contest_open', args=[obj.id]))
        if obj.auto_mode:
            additional = Markdown(str(
                _('The election will open automatically the **%(start_date)s** at **%(start_time)s**',
                    start_date=obj.start.strftime('%d %b %Y'),
                    start_time=obj.start.strftime('%H:%M')
                )), remove_outer_p=True)
        else:
            additional = None

        super().__init__(
            text,
            P(todo_list),
            subtext,
            action_btn,
            additional,
            cls='body-2'
        )


class SecureElectionAction(ListAction):
    def __init__(self, contest, user):
        title = _('Secure the election')

        inner = SecureElectionInner(contest.id, user=user)
        button = None
        is_scrutator = contest.scrutator_set.filter(user=user).exists()
        if contest.mediator == user or is_scrutator:
            if not contest.joint_public_key and not is_scrutator:
                title = _('Edit guardians and start the key ceremony')
                inner = None
                button = MDCButtonOutlined(
                    _('Add'),
                    icon='add',
                    p=False,
                    tag='a',
                    href=reverse('contest_guardian_create', args=[contest.id])
                )
                icon = TodoIcon()
            elif contest.guardian_set.filter(erased=None).count() == 0:
                title = _(
                    'Ballot box securely locked.'
                    ' Election can be open for voting.'
                )
                icon = DoneIcon()
            else:
                icon = TodoIcon()

        elif guardian := contest.guardian_set.filter(user=user).first():
            if guardian.verified:
                icon = DoneIcon()
            else:
                icon = TodoIcon()

        super().__init__(
            title,
            inner,
            icon,
            button,
            separator=False
        )


class UploadPrivateKeyAction(ListAction):
    def __init__(self, **context):

        guardian = context.get('guardian')
        contest = context.get('contest')
        user = context.get('user')
        title = _('Key ceremony')
        content = Div(
            _(
                'All guardians need to upload their private keys so that'
                ' the ballot box can be opened to reveal the results.'
            )
        )
        if contest.actual_end and not guardian.uploaded:
            action_url_ = reverse('contest_guardian_list', args=[contest.id])
            action_btn_ = MDCButtonOutlined(
                _('upload my private key'),
                False,
                tag='a',
                href=action_url_)
            content.addchild(action_btn_)

        if context['quorum_uploaded']:
            icon = DoneIcon()
        else:
            icon = TodoIcon()

        super().__init__(
            title,
            content,
            icon,
            None,
            separator=user == contest.mediator
        )


class GuardianListComp(MDCList):
    tag = 'guardian-list'

    def __init__(self, contest, editable=False):
        guardians = contest.guardian_set.select_related('user').all()
        super().__init__(
            *(
                MDCListItem(guardian.user)
                for guardian in guardians
            ) if guardians.count()
            else [_('No guardian yet.')],
            role='list'
        )


class GuardiansSettingsCard(Div):
    def __init__(self, view, **context):
        contest = view.object
        editable = not contest.joint_public_key
        kwargs = dict(p=False, tag='a')
        if editable:
            kwargs['href'] = reverse(
                'contest_guardian_create',
                args=[contest.id]
            )
            btn = MDCButtonOutlined(_('view all/edit'), **kwargs)
        else:
            kwargs['href'] = reverse(
                'contest_guardian_list',
                args=[contest.id]
            )
            btn = MDCButtonOutlined(_('view all'), **kwargs)

        super().__init__(
            H5(_('Guardians')),
            GuardianListComp(contest, editable),
            btn,
            cls='setting-section'
        )


@template('guardian_list', Document, Card)
class GaurdianListCard(Div):
    def to_html(self, *content, view, **context):
        contest = context['contest']
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )

        return super().to_html(
            Script(src=Static('ryzom.js')),
            Script(mark_safe(view.get_token())),
            H4(
                _('Guardians'),
                cls='center-text',
                style=dict(
                    margin_bottom='12px'
                )
            ),
            I(
                _(
                    'A minimum of %(quorum)s guardian is needed to'
                    ' lock or open the ballot box',
                    n=contest.quorum,
                    quorum=contest.quorum
                ),
                cls='center-text',
                style=dict(
                    margin_top=0
                )
            ),
            GuardianTable(view, **context),
            cls='card',
            view=view
        )


@template('guardian_create', Document, Card)
class GuardianCreateCard(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )
        table_head_row = Tr(cls='mdc-data-table__header-row')
        for th in (_('Guardians'), ''):
            table_head_row.addchild(
                Th(
                    th,
                    role='columnheader',
                    scope='col',
                    cls='mdc-data-table__header-cell overline',
                )
            )

        table_content = Tbody(cls='mdc-data-table__content')
        cls = 'mdc-data-table__cell'
        for guardian in contest.guardian_set.all():
            activated = guardian.user and guardian.user.is_active
            table_content.addchild(Tr(
                Td(guardian.user, cls=cls),
                Td(
                    GuardianDeleteBtn(guardian),
                    cls=cls,
                    style='text-align:right'),
                cls='mdc-data-table__row',
                style='opacity: 0.5;' if not activated else '',
            ))

        table = Table(
            Thead(table_head_row),
            table_content,
            **{
                'class': 'mdc-data-table__table',
                'aria-label': _('Voters')
            }
        )

        return super().to_html(
            H4(_('Add guardians'), cls='center-text'),
            Div(
                _(
                    'Guardians are responsible for locking and unlocking'
                    ' of the ballot box with their private keys.'
                ),
                cls='center-text body-1'
            ),
            Div(
                B(_('GUARDIAN_HELP_TEXT')),
                cls='red-section'),
            Form(
                Div(
                    form['email'],
                    cls='checkbox-field'
                ),
                CSRFInput(view.request),
                MDCButtonOutlined(
                    _('add guardian'),
                    p=False,
                    icon='person_add'
                ),
                table,
                Div(
                    form['quorum'],
                    Span(
                        MDCButton(
                            tag='input',
                            type='submit',
                            name='start',
                            value=_('Start the key ceremony'),
                            style=dict(
                                margin_left='auto',
                                min_height='36px',
                                height='auto'
                            )
                        ),
                        style='margin: 32px 12px; flex-grow: 1;'),
                    style='display:flex;'
                          'flex-flow: row wrap;'
                          'justify-content: space-between;'
                          'align-items: baseline;'),
                method='POST',
                cls='form'
            ),
            cls='card'
        )


@template('djelectionguard/guardian_form.html', Document, Card)
class GuardianVerifyCard(Div):
    def to_html(self, *content, view, form, **context):
        guardian = view.get_object()
        contest = guardian.contest
        self.backlink = BackLink(
            _('back'),
            reverse('contest_guardian_list', args=[contest.id]))

        self.submit_btn = MDCButton(_('confirm'), True, disabled=True)
        self.submit_btn_id = self.submit_btn.id

        return super().to_html(
            H4(
                _('Confirm possession of an uncompromised private key'),
                cls='center-text'
            ),
            Div(
                _(
                    'You need to upload your private key to confirm that'
                    ' you posses a valid key that hasn’t been temepered with.'
                ),
                cls='center-text'
            ),
            Form(
                form['pkl_file'],
                self.submit_btn,
                CSRFInput(view.request),
                enctype='multipart/form-data',
                method='POST',
                cls='form',
            ),
            cls='card'
        )

    def enable_post(event):
        file_input = document.querySelector('#id_pkl_file')
        file_name = file_input.value
        btn = getElementByUuid(file_input.submit_btn_id)
        btn.disabled = file_name == ''

    def py2js(self):
        file_input = document.querySelector('#id_pkl_file')
        file_input.submit_btn_id = self.submit_btn_id
        file_input.addEventListener('change', self.enable_post)


@template('guardian_upload', Document, Card)
class GuardianUploadKeyCard(Div):
    def to_html(self, *content, view, form, **context):
        guardian = view.get_object()
        contest = guardian.contest
        self.backlink = BackLink(
            _('back'),
            reverse('contest_guardian_list', args=[contest.id]))

        self.submit_btn = MDCButton(_('confirm'), True, disabled=True)
        self.submit_btn_id = self.submit_btn.id

        return super().to_html(
            H4(_('Verify your private key'), cls='center-text'),
            Div(
                _(
                    'All guardians’ must upload their valid private keys'
                    ' to unlock the ballot box.'
                ),
                cls='center-text'
            ),
            Form(
                form['pkl_file'],
                MDCErrorList(form.non_field_errors()),
                self.submit_btn,
                CSRFInput(view.request),
                enctype='multipart/form-data',
                method='POST',
                cls='form',
            ),
            cls='card'
        )

    def py2js(self):
        file_input = document.querySelector('#id_pkl_file')
        file_input.submit_btn_id = self.submit_btn_id
        file_input.addEventListener('change', self.enable_post)

    def enable_post(event):
        file_input = document.querySelector('#id_pkl_file')
        file_name = file_input.value
        btn = getElementByUuid(file_input.submit_btn_id)
        btn.disabled = file_name == ''
