import codecs
import io

from cryptography.fernet import Fernet

from django import http
from django.conf import settings
from django.contrib import messages
from django.views import generic
from django.views.generic.edit import FormMixin
from django.utils import timezone
from django.urls import reverse

from ryzom_django_channels.views import ReactiveMixin, register

from djelectionguard.views import Enforce2FAMixin
from djlang.utils import gettext as _

from ..permissions import *
from .forms import (
    GuardianCreateForm,
    GuardianVerifyForm,
    GuardianUploadForm,
)


class GuardianListView(
    Enforce2FAMixin,
    ReactiveMixin,
    ContestPermissionMixin,
    generic.DetailView
):
    template_name = 'guardian_list'
    permission_chain = [
        user_is_guardian_or_mediator,
        contest_key_ceremony_started
    ]


class GuardianCreateView(
    Enforce2FAMixin,
    ContestPermissionMixin,
    FormMixin,
    generic.DetailView
):
    template_name = 'guardian_create'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_key_ceremony_not_started
    ]
    form_class = GuardianCreateForm

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        return super().get_context_data(**kwargs)

    def get_form(self):
        contest = self.get_object()
        self.contest = contest
        form = super().get_form()
        form.initial['quorum'] = contest.quorum
        form.instance = Guardian(contest=contest)
        return form

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        if form.cleaned_data['email']:
            messages.success(
                self.request,
                _('You have invited %(obj)s as guardian', obj=form.instance),
            )
        if 'quorum' in form.changed_data:
            messages.success(
                self.request,
                _(
                    'Quorum set to %(quorum)s',
                    quorum=form.instance.contest.quorum
                )
            )
        self.form = form
        return super().form_valid(form)

    def get_success_url(self):
        if self.form.cleaned_data['start']:
            return reverse('contest_detail', args=[self.contest.id])

        return reverse('contest_guardian_create', args=[self.get_object().id])


class GuardianDeleteView(Enforce2FAMixin, generic.DeleteView):
    pk_url_kwarg = 'gpk'

    def get_queryset(self):
        return Guardian.objects.filter(
            contest__mediator=self.request.user,
            contest__joint_public_key=None,
            contest__actual_start=None,
        )

    def dispatch(self, request, *args, **kwargs):
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)
        return self.delete(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        contest = self.get_object().contest

        response = super().delete(request, *args, **kwargs)

        if contest.quorum > contest.guardian_set.count():
            contest.quorum = contest.guardian_set.count()
            contest.save()
            messages.success(
                self.request,
                _(
                    'Quorum set to %(quorum)s to match the number of'
                    ' guardians',
                    quorum=contest.quorum
                )
            )
        messages.success(
            self.request,
            _('You have removed guardian %(guardian)s', guardian=self.object)
        )
        return response

    def get_success_url(self):
        contest = self.get_object().contest
        return reverse('contest_guardian_create', args=(contest.id,))


class GuardianDownloadView(Enforce2FAMixin, GuardianPermissionMixin, generic.DetailView):
    pk_url_kwarg = 'gpk'
    permission_chain = [
        user_is_guardian,
        guardian_key_ceremony_started,
        guardian_key_not_erased,
        guardian_contest_not_opened_yet
    ]

    def get(self, request, *args, **kwargs):
        if 'wsgi.file_wrapper' in request.META:
            del request.META['wsgi.file_wrapper']
        obj = self.get_object()
        obj.downloaded = timezone.now()
        obj.save()

        encrypter = Fernet(settings.CRYPTO_KEY)
        guardian_b = obj.get_keypair()
        guardian_b64 = codecs.encode(guardian_b, 'base64')
        guardian_encrypted = encrypter.encrypt(guardian_b64)

        response = http.FileResponse(
            io.BytesIO(guardian_encrypted),
            as_attachment=True,
            filename=f"{obj.contest} - guardian-{obj.pk}.pkl",
            content_type='application/octet-stream',
        )
        register(f'{obj.contest.id}:secure_inner').refresh(obj.contest.id)
        return response


class GuardianVerifyView(Enforce2FAMixin, GuardianPermissionMixin, generic.UpdateView):
    pk_url_kwarg = 'gpk'
    permission_chain = [
        user_is_guardian,
        guardian_key_downloaded,
        guardian_key_not_verified,
        guardian_contest_not_opened_yet
    ]
    form_class = GuardianVerifyForm

    def get_success_url(self):
        obj = self.object
        register(f'{obj.contest.id}:secure_inner').refresh(obj.contest.id)
        messages.success(
            self.request,
            _(
                'You have verified your guardian key for %(obj)s',
                obj=self.object.contest
            )
        )
        return self.object.contest.get_absolute_url()


class GuardianUploadView(Enforce2FAMixin, GuardianPermissionMixin, generic.UpdateView):
    pk_url_kwarg = 'gpk'
    template_name = 'guardian_upload'
    permission_chain = [
        user_is_guardian,
        guardian_contest_is_closed,
        guardian_key_not_uploaded,
        guardian_contest_is_not_decrypting,
        guardian_contest_is_not_decrypted
    ]
    form_class = GuardianUploadForm

    def get_success_url(self):
        messages.success(
            self.request,
            _(
                'You have uploaded your guardian key for %(obj)s',
                obj=self.object.contest
            )
        )
        return self.object.contest.get_absolute_url()
