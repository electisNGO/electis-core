import codecs
import hashlib

from cryptography.fernet import Fernet

from django import forms
from django.conf import settings
from django.utils import timezone
from django.db.models import Q

from djlang.utils import gettext as _

from electeez_sites.models import Site
from electeez_auth.models import User

from ..models import Guardian


class GuardianCreateForm(forms.Form):
    email = forms.CharField(required=False)
    quorum = forms.IntegerField(
        min_value=1,
        required=True,
        help_text=_('Minimum guardians needed to unlock the ballot box'))
    start = forms.Field(required=False)

    class Meta:
        fields = ['email']

    def clean(self):
        if self.cleaned_data['email']:
            site = Site.objects.get_current()
            email_or_wallet = self.cleaned_data['email']
            user = User.objects.filter(
                Q(email=email_or_wallet)
                | Q(wallet=email_or_wallet),
                siteaccess__site=site
            ).first()
            if not user:
                if '@' in email_or_wallet:
                    user = User.objects.create_user(email=email_or_wallet)

            if self.instance.contest.guardian_set.filter(user=user):
                raise forms.ValidationError(
                    dict(email=f'{user}' + _('already added!'))
                )

        n_guardians = self.instance.contest.guardian_set.count()
        if self.cleaned_data['email']:
            n_guardians += 1

        quorum = self.cleaned_data['quorum']
        if quorum > n_guardians:
            raise forms.ValidationError(
                dict(quorum=_('Cannot be higher than the number of guardians'))
            )

        return self.cleaned_data

    def save(self):
        contest = self.instance.contest
        contest.quorum = self.cleaned_data['quorum']
        contest.save()

        if self.cleaned_data['email']:
            email_or_wallet = self.cleaned_data['email']
            user = User.objects.filter(
                Q(email=email_or_wallet)
                | Q(wallet=email_or_wallet)
            ).first()
            self.instance.user = user
            self.instance.sequence = contest.guardian_sequence
            self.instance.save()

        if self.cleaned_data['start']:
            contest.key_ceremony()


class GuardianUploadFormBase(forms.ModelForm):
    pkl_file = forms.FileField(
        label=_("Your privacy key is a file with '.pkl' extension."),
        widget=forms.FileInput(
            attrs=dict(
                label=_('Select file'),
                empty_value=_('No file selected')
            ),
        )
    )

    class Meta:
        model = Guardian
        fields = []

    def decode_file(self, file_content):
        encrypter = Fernet(settings.CRYPTO_KEY)
        guardian_encrypted = file_content
        guardian_b64 = encrypter.decrypt(guardian_encrypted)
        guardian_b = codecs.decode(guardian_b64, 'base64')

        return guardian_b


class GuardianVerifyForm(GuardianUploadFormBase):
    submit_label = _('Verify integrity')

    def clean_pkl_file(self):
        try:
            file_content = self.cleaned_data['pkl_file'].read()
            assert (
                self.decode_file(file_content) == self.instance.get_keypair()
            )
        except Exception:
            raise forms.ValidationError(
                _('File corrupted, please try downloading again')
            )

        return b''

    def save(self, *args, **kwargs):
        self.instance.verified = timezone.now()
        return super().save(self, *args, **kwargs)


class GuardianUploadForm(GuardianUploadFormBase):
    submit_label = 'Upload'

    def clean(self):
        try:
            file_content = self.cleaned_data['pkl_file'].read()
            decoded = self.decode_file(file_content)
            sha512 = hashlib.sha512(decoded).hexdigest()
            assert sha512 == self.instance.key_sha512
        except Exception:
            raise forms.ValidationError(
                _("The provided guardian key is invalid")
            )

        self.guardian_key = decoded

        return self.cleaned_data

    def save(self, *args, **kwargs):
        self.instance.upload_keypair(self.guardian_key)
        return self.instance
