from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import (
    GuardianListView,
    GuardianCreateView,
    GuardianDownloadView,
    GuardianVerifyView,
    GuardianUploadView,
    GuardianDeleteView,
)


urlpatterns = [
    path(
        '',
        login_required(GuardianListView.as_view()),
        name='contest_guardian_list'
    ),
    path(
        'create/',
        login_required(GuardianCreateView.as_view()),
        name='contest_guardian_create'
    ),
    path(
        '<uuid:gpk>/download/',
        login_required(GuardianDownloadView.as_view()),
        name='contest_guardian_download'
    ),
    path(
        '<uuid:gpk>/verify/',
        login_required(GuardianVerifyView.as_view()),
        name='contest_guardian_verify'
    ),
    path(
        '<uuid:gpk>/upload/',
        login_required(GuardianUploadView.as_view()),
        name='contest_guardian_upload'
    ),
    path(
        '<uuid:gpk>/delete/',
        login_required(GuardianDeleteView.as_view()),
        name='contest_guardian_delete'
    ),
]
