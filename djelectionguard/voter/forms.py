import io
import csv
import hashlib

from datetime import datetime

from django import forms
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator, RegexValidator
from django.utils import timezone

from phonenumber_field.formfields import PhoneNumberField

from djlang.utils import gettext as _

from electeez_auth.models import User, IDToken
from electeez_sites.models import Site
from ..models import Contest, Voter


class VotersEmailsField(forms.CharField):
    widget = forms.Textarea

    def clean(self, value):
        value = super().clean(value)
        value = value.replace(
            '\r\n', '\n'       # windows
        ).replace('\r', '\n')  # mac

        email_validator = EmailValidator()
        wallet_validator = RegexValidator('^tz[123][A-Za-z0-9]{33}$')
        invalid = []
        addrs = []
        for line in value.split('\n'):
            line = line.strip()
            if not line:
                continue
            try:
                if '@' in line:
                    line = line.lower()
                    email_validator(line)
                else:
                    wallet_validator(line)
            except ValidationError:
                invalid.append(line)
            else:
                addrs.append(line)

        if invalid:
            raise ValidationError(
                _('Please remove lines containing invalid addresses: ')
                + ', '.join(invalid))

        site = Site.objects.get_current()
        if site.max_voters:
            if len(addrs) > site.max_voters:
                raise ValidationError(
                    _(
                        'There are more voters than allowed:'
                        ' %(voters)s/%(max_allowed)s',
                        voters=len(addrs),
                        max_allowed=site.max_voters
                    )
                )

        return addrs


class VotersEmailsForm(forms.ModelForm):
    submit_label = _('Update voters')
    voters_emails = VotersEmailsField(
        label=_('Voters emails'),
        help_text=_('The list of allowed voters with one email per line'),
        required=False,
        widget=forms.Textarea(attrs=dict(cols=50, rows=20))
    )

    class Meta:
        model = Contest
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        wallets = self.instance.voter_set.filter(
            user__email=None
        ).values_list(
            'user__wallet',
            flat=True
        )
        emails = self.instance.voter_set.filter(
            user__wallet=None
        ).values_list(
            'user__email',
            flat=True
        )
        self.fields['voters_emails'].initial = '\n'.join(
            [*wallets, *emails]
        )

    def save(self):
        voters_emails_list = self.cleaned_data.get('voters_emails')
        self.instance.update_voters(voters_emails_list)
        return self.instance


class VotersFileUploadForm(forms.ModelForm):
    submit_label = _('Set voters')
    voters_file = forms.FileField(
        label=_('Voters file (.xsl, .xlsx, .csv)'),
        widget=forms.FileInput(
            attrs=dict(
                label=_('Select file'),
                empty_value=_('No file selected')
            ),
        )
    )

    class Meta:
        model = Contest
        fields = ['voters_file']


class VoterPhoneForm(forms.ModelForm):
    submit_label = _('Send a SMS')

    phone = PhoneNumberField(
        label=_('Your phone number'),
        help_text=_(
            'Prefix your number with your country code'
            ' (e.g +33 for France).'
        )
    )

    class Meta:
        model = Voter
        fields = []

    def clean(self):
        if 'phone' not in self.cleaned_data:
            return self.cleaned_data

        phone_number = self.cleaned_data['phone']
        full_num = (
            f'+{phone_number.country_code}'
            f'{phone_number.national_number}'
        )
        num_hash = hashlib.sha512(full_num.encode()).hexdigest()
        return dict(
            phone_hash=num_hash,
            full_num=full_num
        )

    def save(self):
        self.instance.phone_hash = self.cleaned_data['phone_hash']
        self.instance.generate_token()
        self.instance.send_verification_token(
            self.cleaned_data['full_num']
        )


class VoterVerifyForm(forms.ModelForm):
    submit_label = _('Verify my code')

    code = forms.IntegerField(
        label=_('Enter the code received by SMS'),
    )

    def clean(self):
        code = []
        for i in range(6):
            code += self.data[f'code_{i}']
        self._errors = {}

        full_code = int(''.join(code))

        # if code is good, user has access to phone so we can tell
        # he's not trying to steal someone else's informations about voting
        if self.instance.token == full_code:
            if Voter.objects.filter(
                contest_id=self.instance.contest_id,
                phone_hash=self.instance.phone_hash
            ).exclude(
                phone_verified=None
            ).count():
                # but he may be trying to use it twice
                self.instance.token_expiry = timezone.now()
                self.instance.save()
                self.phone_used = True
                messages.error(
                    self.view.request,
                    _('This phone has already been used')
                )
                raise forms.ValidationError(
                    _('This phone has already been used')
                )

        return dict(code=full_code)

    class Meta:
        model = Voter
        fields = []
