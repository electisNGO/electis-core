import io
import textwrap
import json
import pandas
import pickle

from django import forms
from django import http
from django.contrib import messages
from django.conf import settings
from django.db import transaction
from django.views import generic
from django.views.generic.edit import FormMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from datetime import datetime

from redis import StrictRedis

from ryzom_django_channels.views import ReactiveMixin, register

from djlang.utils import gettext as _

from djelectionguard.views import Enforce2FAMixin
from electeez_auth.models import User

from ..models import Voter, tally_ballot
from ..permissions import *
from ..serialize import CiphertextBallotDeserializer

from .forms import (
    VotersEmailsForm,
    VotersFileUploadForm,
    VoterVerifyForm,
    VoterPhoneForm
)


class VotersDetailView(
    Enforce2FAMixin,
    ReactiveMixin,
    ContestPermissionMixin,
    generic.DetailView
):
    template_name = 'djelectionguard/contest_voters_detail.html'
    permission_chain = [user_is_guardian_or_mediator]


class VotersUpdateView(
    Enforce2FAMixin,
    ReactiveMixin,
    ContestPermissionMixin,
    generic.UpdateView
):
    template_name = 'djelectionguard/voters_update.html'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_not_closed
    ]

    def get_form_class(self):
        contest = self.get_object()
        self.provider = contest.consent_provider.provider
        if self.provider.from_file:
            self.form_label = _('Please upload the list of birthdate/token')
            self.form_label = self.provider.get_form_label()
            return VotersFileUploadForm

        self.form_label = _(
            'the list of allowed voters with one email per line'
            ' (sparated by enter/return ⏎)'
        )
        return VotersEmailsForm

    def form_valid(self, form):
        if self.provider.from_file:
            self.object = self.provider.save_form(form)
        else:
            self.object = form.save()
        messages.success(
            self.request,
            _('You have updated voters for contest %(obj)s', obj=self.object)
        )
        register(f'{self.object.id}:voters_status').refresh(
            self.object.id,
            str(_('Finalizing...')),
        )
        if self.object.consent_provider.provider.manual:
            return super().form_valid(form)
        else:
            return http.JsonResponse(dict(status=200))

    def form_invalid(self, form):
        if self.object.consent_provider.provider.manual:
            return super().form_invalid(form)
        else:
            return http.JsonResponse(dict(status=500))

    def get_success_url(self):
        return self.object.get_absolute_url()


class VoterPhoneView(VoterPermissionMixin, generic.UpdateView):
    pk_url_kwarg = 'vpk'
    template_name = 'voter_phone'
    permission_chain = [
        voter_is_user
    ]
    form_class = VoterPhoneForm

    class Meta:
        model = Voter

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.contest.send_sms or self.object.phone_verified:
            messages.success(request, _('Your phone is already verified'))
            return redirect('contest_vote', pk=self.object.contest_id)
        elif self.object.token and not self.object.token_expired:
            return redirect(
                'voter_verify',
                pk=self.object.contest_id,
                vpk=self.object.id
            )
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        self.object = self.get_object()
        return reverse(
            'voter_verify',
            args=[self.object.contest_id, self.object.id]
        )


class VoterVerifyView(VoterPermissionMixin, generic.UpdateView):
    pk_url_kwarg = 'vpk'
    template_name = 'voter_verify'
    permission_chain = [
        voter_is_user
    ]
    form_class = VoterVerifyForm

    class Meta:
        model = Voter

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.contest.send_sms or self.object.phone_verified:
            return redirect('contest_vote', pk=self.object.contest_id)
        elif self.object.token and self.object.token_expired:
            messages.error(request, _('Your code expired, please try again'))
            return redirect(
                'voter_phone',
                pk=self.object.contest_id,
                vpk=self.object.id
            )
        return super().dispatch(request, *args, **kwargs)

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.view = self
        return form

    def form_valid(self, form):
        if form.is_valid():
            code = form.cleaned_data['code']

            if self.object.token == code:
                self.object.phone_verified = timezone.now()
                self.object.save()
                return super().form_valid(form)
        return super().form_invalid(form)

    def form_invalid(self, form):
        if hasattr(form, 'phone_used'):
            form.instance.token_expiry = timezone.now()
            form.instance.save()
            return redirect(
                'voter_phone',
                pk=self.object.contest_id,
                vpk=self.object.id
            )
        elif form.instance.token_expiry <= timezone.now():
            messages.error(
                self.request,
                _('Your code expired, please try again')
            )
            return redirect(
                'voter_phone',
                pk=self.object.contest_id,
                vpk=self.object.id
            )

        return super().form_invalid(form)

    def get_success_url(self):
        return reverse('contest_vote', args=[self.object.contest_id])


class VoteView(ContestPermissionMixin, FormMixin, generic.DetailView):
    template_name_field = 'vote_template'
    permission_chain = [
        user_is_voter_for_contest,
        contest_is_opened,
        contest_is_not_closed,
    ]

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.info(
                request,
                _('Please get a magic link to proceed to secure vote')
            )
            return http.HttpResponseRedirect(
                reverse('otp_send')
                + '?next='
                + self.request.path_info
            )

        self.object = self.get_object()
        voter = self.object.voter_set.filter(user=request.user).first()
        if settings.TWILIO_ENABLED and self.object.send_sms:
            if voter and not voter.phone_verified:
                messages.error(
                    request,
                    _('Verify you phone before you can vote')
                )
                return http.HttpResponseRedirect(
                    reverse('voter_phone', args=[self.object.id, voter.id])
                )

        if voter and voter.casted_at:
            messages.error(
                request,
                _(
                    'you have already casted your vote for %(obj)s',
                    obj=self.object
                )
            )
            return http.HttpResponseRedirect(reverse('contest_list'))

        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        class FormClass(forms.Form):
            submit_label = _('Create ballot')
            ballot = forms.JSONField(decoder=CiphertextBallotDeserializer)

        form = FormClass(**self.get_form_kwargs())
        return form

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        voter = Voter.objects.filter(
            contest=self.object,
            user=request.user
        ).first()
        voter.save()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        client = StrictRedis.from_url(settings.REDIS_SERVER)
        user_cache_key = f"user-{request.user.id}:verify_voter_nir"
        voter = Voter.objects.filter(
            contest=self.object,
            user=request.user
        ).first()

        if voter.token and client.get(f"{user_cache_key}:verified") is None:
            messages.error(
                request,
                _('You have to verify your identity before you can vote')
            )
            return http.HttpResponseRedirect(
                reverse('contest_vote', args=[self.object.id])
            )
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        encrypted_ballot = form.cleaned_data['ballot']
        contest = self.object
        casted = False

        voter_query = Voter.objects.select_for_update().filter(
            user=self.request.user,
            contest=contest
        )
        with transaction.atomic():
            voter = voter_query.first()
            if voter.casted_at:
                messages.error(
                    self.request,
                    _(
                        'you have already casted your vote for %(obj)s',
                        obj=self.object
                    )
                )
                return http.HttpResponseRedirect(reverse('contest_list'))

            if 'spoil' in self.request.POST:
                contest.ballot_box.spoil(encrypted_ballot)
                messages.info(
                    self.request,
                    _(
                        'You spoiled your ballot for %(obj)s,'
                        ' you can make another ballot',
                        obj=self.object
                    )
                )
                return http.HttpResponseRedirect(
                    reverse('contest_vote', args=[self.object.pk])
                )

            else:
                submitted_ballot = contest.ballot_box.cast(encrypted_ballot)

                if not submitted_ballot:
                    messages.info(
                        self.request,
                        _(
                            'Your ballot was malformed.'
                            ' You have to make another ballot'
                        )
                    )
                    return http.HttpResponseRedirect(
                        reverse('contest_vote', args=[self.object.pk])
                    )

                voter.casted_at = datetime.now(tz=timezone.utc)
                casted = True
                voter.save()

        tally_ballot.delay(str(contest.id), str(submitted_ballot.object_id))
        register(str(contest.id) + ':voters_voted').refresh(contest.id)

        messages.success(
            self.request,
            _('You casted your ballot for %(obj)s', obj=self.object)
        )
        return http.HttpResponseRedirect(
            reverse('vote_success', args=[self.object.id, voter.id])
        )


def verify_voter_nir(request):
    from djelectionguard.models import Contest

    if (
        request.method != 'POST'
        or not request.user.is_authenticated
    ):
        return http.JsonResponse(
            dict(message='Method not allowed'),
            status=400
        )

    data = json.loads(request.body.decode('utf-8'))

    contest_id = data.get('contest_id', None)
    contest = Contest.objects.filter(
        id=contest_id,
        voter__user=request.user
    ).first()

    if not contest:
        return http.JsonResponse(
            dict(message=_('Contest not found')),
            status=404
        )

    voter = contest.voter_set.filter(user=request.user).first()
    if not voter:
        return http.JsonResponse(
            dict(message=_('Voter not found')),
            status=404
        )

    client = StrictRedis.from_url(settings.REDIS_SERVER)
    user_cache_key = f"user-{request.user.id}:verify_voter_nir"
    last_try_datetime_pkl = client.lindex(user_cache_key, 0)
    last_try_datetime = pickle.loads(last_try_datetime_pkl) if last_try_datetime_pkl else None
    num_last_try = client.llen(user_cache_key)

    if num_last_try >= 3:
        now = datetime.now()
        elapsed = now - last_try_datetime
        if elapsed.seconds < 60:
            return http.JsonResponse(
                dict(
                    message=_(
                        'You have failed too many times, please wait %(time)ss',
                        time=60 - elapsed.seconds
                    ),
                ),
                status=400
            )

    if nir := data.get('nir', None):
        voter_nir = "{:04}".format(voter.token) if voter.token else '9999'
        if voter_nir == nir:
            client.set(f"{user_cache_key}:verified", 'true')
            return http.JsonResponse(dict(message='OK'))

    client.lpush(user_cache_key, pickle.dumps(datetime.now()))

    return http.JsonResponse(
        dict(message=_('Credential mismatch')),
        status=400
    )


class VoteSuccessView(VoterPermissionMixin, generic.DetailView):
    pk_url_kwarg = 'vpk'
    template_name = 'djelectionguard/vote_success'
    permission_chain = [
        voter_is_user,
        voter_has_casted
    ]


class VotersDownloadView(Enforce2FAMixin, ContestPermissionMixin, generic.DetailView):
    permission_chain = [contest_is_decrypted, user_is_mediator_of_contest]

    def get(self, request, *args, **kwargs):
        contest = self.get_object()

        data = 'email,casted_at\n'
        for voter in contest.voter_set.order_by("id"):
            data += f"{voter.user.email},{voter.casted_at}\n"

        return http.FileResponse(
            io.BytesIO(data.encode()),
            as_attachment=True,
            filename=f'{contest.name}-voters.csv',
            content_type='text/plain'
        )

class VotersTokenDownloadView(Enforce2FAMixin, ContestPermissionMixin, generic.DetailView):
    permission_chain = [user_is_mediator_of_contest]

    def get(self, request, *args, **kwargs):
        contest = self.get_object()

        df = pandas.DataFrame({
            'CLASSE': [],
            'NOM': [],
            'PRENOM': [],
            'IDENTIFIANT': []
        })

        data = pandas.read_excel(contest.voters_file)
        for i, row in data.iterrows():
            email = row.get('MAIL')
            if not email or email != email:
                user = User.objects.filter(
                    email=None,
                    contact_email=None,
                    first_name=row.get('PRENOM', '').upper(),
                    last_name=row.get('NOM', '').upper(),
                ).first()
                if not user:
                    print(f'Error with row: {row}')
                    continue
                token = ' '.join(textwrap.fill(user.profile, 4).split('\n'))
                df = df.append({
                    'CLASSE': row.get('CLASSE'),
                    'NOM': row.get('NOM'),
                    'PRENOM': row.get('PRENOM'),
                    'IDENTIFIANT': token
                }, ignore_index=True)

        df = df.sort_values(by=['CLASSE', 'NOM', 'PRENOM'])

        stream = io.BytesIO()
        writer = pandas.ExcelWriter(stream)
        df.to_excel(writer, index=False)
        writer.save()
        stream.seek(0)

        return http.FileResponse(
            stream,
            as_attachment=True,
            filename=f'{contest.name}-voters.xlsx',
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
