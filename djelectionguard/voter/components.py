import random
import math

from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils import timezone
from electionguard.serialize import to_raw

from djlang.utils import gettext as _

from ryzom_django_channels.components import (
    ReactiveComponentMixin,
    SubscribeComponentMixin,
    model_template
)

from djelectionguard.components import *
from djelectionguard.candidate.components import CandidateDetail


class VoterList(Ul):
    def __init__(self, contest):
        emails = contest.voters_emails.split('\n')
        num_emails = len(emails)
        if emails[0] == '':
            num_emails = 0
        super().__init__(
            *(
                MDCListItem(voter)
                for voter
                in emails
            ) if num_emails
            else _('No voter yet.'),
            cls='mdc-list voters-list'
        )


class AddVoterAction(ListAction):
    def __init__(self, **context):
        contest = context['contest']
        num_voters = context['num_voter']
        num_token = context['num_token']

        num_total = num_voters + num_token

        kwargs = dict(
            tag='a',
            href=reverse('contest_voters_update', args=[contest.id])
        )
        if num_total:
            btn_comp = MDCButtonOutlined(_('edit'), False, **kwargs)
            icon = DoneIcon()
            txt = _(
                '%(num_voters)d voters',
                n=num_total,
                num_voters=num_total
            )
        else:
            btn_comp = MDCButtonOutlined(_('add'), False, 'add', **kwargs)
            icon = TodoIcon()
            txt = ''

        super().__init__(
            _('Add voters'),
            txt, icon, btn_comp,
            separator=True
        )


class CastVoteAction(ListAction):
    def __init__(self, **context):
        contest = context['contest']
        voter = context['voter']
        if voter.casted_at:
            head = _('Voted')
            txt = Span(
                _(
                    'You casted your vote!'
                    ' The results will be published after'
                    ' the election is closed.'
                ),
            )
            icon = DoneIcon()
            btn_comp = None
        elif not contest.actual_end:
            head = _('Cast my vote')
            txt = ''
            icon = TodoIcon()
            url = reverse('contest_vote', args=(contest.id,))
            btn_comp = MDCButtonOutlined(_('vote'), False, tag='a', href=url)
        else:
            head = _('You did not vote')
            txt = _('The vote is closed, sorry you missed it.')
            icon = TodoIcon(style=dict(filter='brightness(0.5)'))
            btn_comp = None

        super().__init__(head, txt, icon, btn_comp, separator=True)


class VotersSettingsCard(Div):
    def __init__(self, view, **context):
        contest = context['contest']
        num_emails = context['num_voter']

        kwargs = dict(
            p=False,
            tag='a',
            href=reverse('contest_voters_detail', args=[contest.id]))
        if contest.actual_start:
            btn = MDCButtonOutlined(_('view all'), **kwargs)
        elif num_emails:
            btn = MDCButtonOutlined(_('view all/edit'), **kwargs)
        elif view.request.user == contest.mediator:
            kwargs['href'] = reverse(
                'contest_voters_update',
                args=[contest.id]
            )
            btn = MDCButtonOutlined(_('add'), icon='add', **kwargs)
        else:
            btn = ''

        super().__init__(
            H5(_('Voters')),
            Span(
                _('%(voters)s voters added', n=num_emails, voters=num_emails),
                cls='voters_count'
            ),
            btn,
            cls='setting-section'
        )


class TrackerDetail(Div):
    def __init__(self, contest):
        rows = (
            ('tracker-contest-name', _('Election name'), contest.name),
            ('tracker-contest-id', _('Election ID'), contest.id),
            ('tracker-ballot-id', _('Ballot ID'), ''),
            ('tracker-ballot-hash', _('Ballot Hash'), ''),
        )

        wiki = 'https://fr.wikipedia.org/wiki/Fonction_de_hachage'

        return super().__init__(
            Div(
                _('TRACKING_MSG'),
                style=dict(
                    text_align='center',
                    margin_top='32px',
                    margin_bottom='32px',
                    opacity='0.6'
                )
            ),
            Div(
                MDCButton(_('Send my ballot')),
                style=dict(
                    display='flex',
                    justify_content='space-around',
                    margin='32px 0'
                )
            ),
            Div(
                B(
                    _('Learn more'),
                    style=dict(
                        text_align='center',
                        display='block'
                    )
                ),
                P(
                    mark_safe(
                        _(
                            'TRACKING_MORE_INFO',
                            link=f'<a href={wiki}>Fonction de hashage</a>'
                        )
                    )
                ),
                style=dict(
                    background='aliceblue',
                    margin_top='32px',
                    padding='12px',
                    opacity='0.6'
                )
            ),
            Table(
                *(Tr(
                    Th(
                        label, ': ',
                        scope='row',
                        cls='overline',
                        style='text-align: right;'
                              'padding: 12px;'
                              'white-space: nowrap;'
                    ),
                    Td(
                        Pre(
                            value,
                            id=_id,
                            style='word-break: break-word;'
                                  'white-space: break-spaces;'
                        ),
                    )
                ) for _id, label, value in rows),
                style='margin: 0 auto;',
                role='presentation'
            ),
            style=dict(display='none')
        )

    class HTMLElement:
        def connectedCallback(self):
            this.style.display = 'none'

        def show(self, ballot_id, ballot_sha):
            window.form = form
            document.querySelector('#contest-desc').style.display = 'none'
            document.querySelector('.mdc-list').style.display = 'none'
            location.href='#'
            this.querySelector('#tracker-ballot-id').innerHTML = ballot_id
            this.querySelector('#tracker-ballot-hash').innerHTML = ballot_sha
            this.querySelector('button').addEventListener(
                'click',
                this.onclick.bind(form)
            )
            this.style.display = 'block'
            this.style.opacity = 1

        def onclick(self, event):
            this.submit()


class DialogConfirmVoteForm(Form):
    def __init__(self, *content, selections=[], max_selections=1, **attrs):
        def hidden_selections():
            for s in selections:
                candidate = CandidateDetail(s)
                candidate.style.display = 'none'
                candidate.attrs['data-candidate-id'] = s.id
                yield candidate

        self.max_selections = max_selections

        actions = MDCDialogActions(
            MDCDialogCloseButtonOutlined(_('modify')),
            MDCDialogAcceptButton(
                _('Next'),
                addcls='mdc-button--raised black-button',
            ),
            style={
                'display': 'flex',
                'justify-content': 'space-around'
            }
        )

        self.remaining_text_start = str(_('If you want it, you have'))
        self.remaining_text_end = str(_('choice left'))
        self.remaining_text_end_plural = str(_('choices left'))

        super().__init__(
            *content,
            MDCDialog(
                _('Confirm your selection'),
                Div(
                    _(
                        'Be careful, once confirmed,'
                        ' your choice is definitive and cannot be changed'
                    ),
                    *hidden_selections(),
                ),
                actions=Div(
                    actions,
                    Div(
                        Span(id='remaining'),
                        style=dict(
                            background='aliceblue',
                            text_align='center',
                            padding='12px',
                            margin='24px',
                            margin_top='0'
                        ),
                    ),
                )
            ),
            **attrs
        )

    def ondialogclosed(event):
        candidates = event.currentTarget.querySelectorAll(
            '[data-candidate-id]'
        )
        for candidate in candidates:
            candidate.style.display = 'none'

    async def ondialogclosing(event):
        if event.detail.action == 'accept':
            encrypted_ballot = create_encrypted_ballot(
                form.selections_form_data
            )
            serialized_ballot = serializeBallot(encrypted_ballot)
            ballot_sha = await sha512sum(encrypted_ballot)
            form.style.opacity = 0
            form.style.position = 'absolute'
            form.reset()
            form.ballot.value = serialized_ballot
            self.showTrackingInfo(
                encrypted_ballot.object_id,
                ballot_sha
            )

    def showTrackingInfo(ballot_id, ballot_sha):
        tracker.show(ballot_id, ballot_sha)

    def handle_submit(event):
        event.preventDefault()
        this.dialog = this.querySelector('mdc-dialog')
        this.selections_form_data = new.FormData(this).getAll('selections')
        for selection in this.selections_form_data:
            candidate = this.dialog.querySelector(
                '[data-candidate-id="' + selection + '"]'
            )
            candidate.style.display = 'flex'

        remaining = this.max_selections - len(this.selections_form_data)
        self.update_remaining(this, remaining)
        this.dialog.onclosing = self.ondialogclosing
        this.dialog.onclosed = self.ondialogclosed
        this.dialog.open()

    def update_remaining(form, remaining):
        elem = document.querySelector('#remaining')
        remaining_text = (
            form.remaining_text_start + ' ' + remaining + ' '
        )
        if remaining > 1:
            remaining_text += form.remaining_text_end_plural
        else:
            remaining_text += form.remaining_text_end

        if remaining == 0:
            elem.parentElement.style.display = 'none'
        else:
            elem.innerHTML = remaining_text
            elem.parentElement.style.display = 'block'

    def py2js(self):
        form = getElementByUuid(self.id)
        form.max_selections = self.max_selections
        form.remaining_text_start = self.remaining_text_start
        form.remaining_text_end = self.remaining_text_end
        form.remaining_text_end_plural = self.remaining_text_end_plural
        form.addEventListener('submit', self.handle_submit.bind(form))


class ReactiveStatus(ReactiveComponentMixin, Div):
    def __init__(self, contest_id, msg, **kwargs):
        self.register = f'{contest_id}:voters_status'

        super().__init__(
            msg,
            style='text-align: center; font-style: italic',
            cls='status'
        )


class ReactiveVoterVoted(ReactiveComponentMixin, H5):
    def __init__(self, contest_id, **kwargs):
        self.register = f'{contest_id}:voters_voted'

        contest = Contest.objects.get(id=contest_id)
        voters_count = contest.voter_set.all().count()
        voters_voted = contest.voter_set.exclude(casted_at=None).count()

        super().__init__(
            _(
                '%(voters)s / %(count)s have voted',
                n=voters_voted,
                voters=voters_voted,
                count=voters_count,
            ),
            cls='center-text'
        )


class ReactiveVoterTotal(ReactiveComponentMixin, H4):
    def __init__(self, contest_id, **kwargs):
        self.register = f'{contest_id}:voters_total'

        contest = Contest.objects.get(id=contest_id)
        voters_count = contest.voter_set.all().count()

        super().__init__(
            _('%(count)s Voters', n=voters_count, count=voters_count),
            style='text-align: center;',
            cls='center-text'
        )


class ReactiveVoterAdded(ReactiveComponentMixin, H5):
    def __init__(self, contest_id, added=0, count=0, **kwargs):
        self.register = f'{contest_id}:voters_added'

        super().__init__(
            _(
                '%(voters)s / %(count)s have been added',
                n=added,
                voters=added,
                count=count
            ),
            cls='center-text'
        )


@model_template('voter-row')
class ReactiveVoterRow(Tr):
    def __init__(self, voter):
        activated = voter.user and voter.user.is_active
        open_email_sent = (
            voter.open_email_sent.astimezone(voter.contest.timezone).strftime("%d/%m/%Y %H:%M") ###
            if voter.open_email_sent else ''
        ) if voter.user.email else '--'
        close_email_sent = (
            voter.close_email_sent.astimezone(voter.contest.timezone).strftime("%d/%m/%Y %H:%M")
            if voter.close_email_sent else ''
        ) if voter.user.email else '--'

        cls = 'mdc-data-table__cell'

        super().__init__(
            Td(voter.user.display_name, cls=cls),
            Td(
                open_email_sent,
                cls=cls + ' center',
            ),
            Td(
                CheckedIcon() if voter.casted_at else 'No',
                cls=cls + ' center',
            ),
            Td(
                close_email_sent,
                cls=cls + ' center',
            ),
            cls='mdc-data-table__row',
            style='opacity: 0.5;' if not activated else '',
            id=str(voter.id) + '-row'
        )


class VotersTableContent(SubscribeComponentMixin, Div):
    publication = 'voters'
    model_template = 'voter-row'

    def __init__(self, *content, **context):
        self.pagination = Pagination()
        self.container = Tbody(cls='mdc-data-table__content')
        table_head_row = Tr(cls='mdc-data-table__header-row')
        for th in ('email', 'vote email sent', 'voted', 'tally email sent'):
            table_head_row.addchild(
                Th(
                    th,
                    role='columnheader',
                    scope='col',
                    cls='mdc-data-table__header-cell overline',
                    style='' if th == 'email' else 'text-align: center;'
                )
            )

        table = Table(
            Thead(table_head_row),
            **{
                'class': 'mdc-data-table__table',
                'aria-label': 'Voters'
            }
        )
        table.addchild(self.container)

        super().__init__(
            Div(
                Div(
                    table,
                    cls='mdc-data-table__table-container',
                    style='width: 100%'
                ),
                cls='mdc-data-table',
                style='width: 100%; overflow: auto'
            ),
            self.pagination,
        )

    def to_html(self, *content, **context):
        view = self.get_view(**context)
        self.subscribe_options = dict(
            contest=str(view.get_object().pk),
            p=view.request.GET.get('p', 0),
            psize=view.request.GET.get('psize', 100),
            casted=view.request.GET.get('casted', None)
        )
        self.reactive_setup(**context)
        self.pagination.set_parameters(self.subscription.options)

        return super(Div, self).to_html(*content, **context)

    @classmethod
    def get_queryset(cls, user, qs, opts):
        qs = qs.select_related('user').filter(contest__id=opts['contest'])
        if opts['casted'] is not None:
            if opts['casted'] in ['1', 'true']:
                qs = qs.excludes(casted_at=None)
            else:
                qs = qs.filter(casted_at=None)
        rows = qs.order_by('user__email')

        max_row = rows.count()
        try:
            opts['psize'] = max(100, min(500, int(opts['psize'])))
            opts['last_page'] = math.ceil(max_row / opts['psize']) - 1
            opts['p'] = max(0, min(int(opts['p']), opts['last_page']))
        except Exception:
            opts['psize'] = 100
            opts['last_page'] = math.ceil(max_row / opts['psize']) - 1
            opts['p'] = 0

        opts['start'] = opts['p'] * opts['psize']
        opts['total'] = max_row
        return rows[opts['start']:opts['start'] + opts['psize']]


@template('djelectionguard/contest_voters_detail.html', Document)
class VotersDetailCard(Div):
    style = dict(cls='card')

    def __init__(self, *content, **context):
        self.id = 'voters-detail-card'
        super().__init__(*content, id=self.id, **context)

    def to_html(self, *content, view, **context):
        contest = view.object
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )

        table = VotersTableContent()

        edit_btn = MDCButtonOutlined(
            _('edit voters'),
            False,
            'edit',
            tag='a',
            href=reverse('contest_voters_update', args=[contest.id]))

        email_btn = MDCButtonOutlined(
            _('invite new voters'),
            False,
            'email',
            tag='a',
            href=reverse('email_voters', args=[contest.id]))


        if contest.actual_end or view.request.user != contest.mediator:
            edit_btn = ''
            email_btn = ''

        voters = contest.voter_set
        voters_count = voters.count()

        if (
            not contest.actual_start
            or not voters.filter(open_email_sent=None).count()
        ):
            email_btn = ''

        export_btn = ''
        if view.object.plaintext_tally is not None:
            export_btn = DownloadBtnOutlined(
                _('Download'),
                'file_download',
                tag='a',
                href='download',
                data_filename=f'{contest.name}-voters.csv',
                data_err_msg=_('Server error')
            )

        download_btn = ''
        params = view.object.consent_provider.parameters
        if 'parser_class' in params and params['parser_class'] == 'Edu':
            download_btn = MDCButtonOutlined(
                _('download voters tokens'),
                False,
                'download',
                tag='a',
                href=reverse('download_voter_token', args=[contest.id])
            )

        return super().to_html(
            Script(src=Static('ryzom.js')),
            Script(mark_safe(view.get_token())),
            H4(
                _('%(count)s Voters', n=voters_count, count=voters_count),
                cls='center-text'
            ),
            Div(
                ReactiveVoterVoted(view.object.id),
            ),
            Div(edit_btn, email_btn, cls='center-button'),
            Div(
                table,
            ),
            export_btn,
            download_btn,
            view=view
        )


@template('djelectionguard/voters_update.html', Document, Card)
class ContestVotersUpdateCard(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))
        voters = contest.voter_set.all()
        count = voters.count()

        self.form_id = None
        if contest.consent_provider.provider.manual:
            title = H4(
                _('%(count)s Voters', n=count, count=count),
                style='text-align: center;'
            )
            inner = [
                Div(
                    view.form_label,
                    cls='body-2',
                    style='margin-bottom: 24px;text-align: center;'
                ),
                Form(
                    CSRFInput(view.request),
                    form,
                    MDCButton(_('Save')),
                    method='POST',
                    cls='form'
                ),
            ]

        else:
            ajax_form = Form(
                CSRFInput(view.request),
                MDCButtonOutlined(
                    _('Sync'),
                    p=False,
                    icon='sync',
                    style='margin: auto'
                ),
                method='POST',
                cls='form'
            )
            self.form_id = ajax_form.id

            title = Div(
                ReactiveVoterTotal(view.object.id)
            )

            inner = [
                Div(
                    _(
                        'the list of allowed voters, fetched by %(provider)s',
                        provider=contest.consent_provider
                    ),
                    cls='body-2',
                    style='margin-bottom: 24px;text-align: center;'
                ),
                ajax_form,
                Div(
                    ReactiveStatus(view.object.id, None)
                ),
                Div(
                    ReactiveVoterAdded(view.object.id, added=0, count=0),
                ),
            ]

        return super().to_html(
            Script(src=Static('ryzom.js')),
            Script(mark_safe(view.get_token())),
            title,
            *inner,
            cls='card',
            view=view
        )

    async def on_form_submit(event):
        event.preventDefault()

        form = event.target
        button = form.querySelector('button')

        def toggle_button():
            icon = button.querySelector('i')
            status = form.parentElement.querySelector('.status')

            if icon.classList.contains('rotate'):
                icon.classList.remove('rotate')
            else:
                icon.classList.add('rotate')

            button.disabled = not button.disabled
            status.innerHTML = ''

        toggle_button()

        await fetch(form.action, {
            'method': form.method,
            'body': new.FormData(form)
        }).then(
            lambda response: toggle_button()
        )

    def py2js(self):
        if self.form_id:
            form = getElementByUuid(self.form_id)
            form.addEventListener('submit', self.on_form_submit)


@template('email_voters', Document, Card)
class ContestEmailVoters(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_voters_detail', args=[contest.id]))

        return super().to_html(
            H4(
                _('Send an invite to the newly added voters'),
                cls='center-text'
            ),
            Form(
                context['form']['open_email_title'],
                context['form']['open_email'],
                CSRFInput(view.request),
                MDCButton(context['form'].submit_label),
                method='POST',
                cls='form'
            ),
            cls='card'
        )


@template('contest_vote', Document, Card)
class ContestVoteCard(Div):
    def to_html(self, *content, view, form, **context):
        print(form.errors)
        contest = view.get_object()
        if not contest.link_to_contest_list:
            self.backlink = BackLink(
                _('back'),
                reverse('contest_detail', args=[contest.id]))
        else:
            self.backlink = BackLink(
                _('back'),
                reverse('contest_list')
            )


        max_selections = contest.votes_allowed

        candidates = list(contest.candidate_set.all())

        if contest.shuffle_candidates:
            random.shuffle(candidates)
        else:
            candidates.sort(key=lambda c: c.sequence)

        choices = (
            (i, CandidateDetail(candidate), candidate.id)
            for i, candidate
            in enumerate(candidates)
        )

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        manifest_str = to_raw(contest.description)
        internal_manifest_str = to_raw(contest.metadata)
        context_str = to_raw(contest.context)

        voter = contest.voter_set.filter(
            user=view.request.user
        ).first()

        if voter:
            self.voter_id = str(voter.id)
            self.seed = voter.get_seed()

        tracker = TrackerDetail(contest)
        self.tracker_id = tracker.id

        return super().to_html(
            Script('global = window;'),
            Script(mark_safe(
                f'window.currentManifest = {manifest_str};'
                f'window.currentInternalManifest = {internal_manifest_str};'
                f'window.currentContext = {context_str};'
            )),
            Script(src=staticfiles_storage.url('js/electis.js')),
            H4(
                contest.name,
                Div(
                    Img(
                        src=contest.picture.url,
                        alt='contest-picture',
                        width="100%",
                        id='contest-picture',
                    ),
                    style='padding: 32px',
                ) if contest.picture else None,
                Div(
                    about,
                    cls='center-text body-2',
                    style='word-break: break-word'
                ),
                cls='center-text',
                style='word-break: break-word',
                id='contest-desc'
            ),
            DialogConfirmVoteForm(
                Div(
                    _(
                        'You have up to a total of %(vote_allowed)s choice',
                        n=max_selections,
                        vote_allowed=max_selections
                    ),
                    style='opacity: 0.6'
                ),
                Ul(
                    *[Li(e) for e in form.non_field_errors()],
                    cls='error-list'
                ),
                CSRFInput(view.request),
                Input(
                    type='hidden',
                    name='ballot'
                ),
                MDCMultipleChoicesCheckbox(
                    'selections',
                    choices,
                    n=max_selections),
                MDCButton(_('create ballot'), type='submit', style=dict(margin='auto')),
                selections=candidates,
                max_selections=max_selections,
                method='POST',
                cls='form vote-form',
            ),
            tracker,
            cls='card'
        )

    def create_encrypted_ballot(choices):
        chosen = available_selections.filter(
            lambda elem: elem.candidate_id in choices
        )

        selections = [
            new.PlaintextBallotSelection(
                c.object_id,
                c.sequence_order,
                1, 0, None
            ) for c in chosen
        ]
        contests = [
            new.PlaintextBallotContest(
                current_manifest.contests[0].object_id,
                current_manifest.contests[0].sequence_order,
                selections
            )
        ]
        ballot = new.PlaintextBallot(
            crypto.randomUUID(),
            current_manifest.ballot_styles[0].object_id,
            contests
        )

        encrypted_ballot = encrypt_ballot(
            ballot, internal_manifest, context, encryption_seed
        )

        return encrypted_ballot

    def py2js(self):
        window.current_manifest = buildManifest(currentManifest)
        window.internal_manifest = new.InternalManifest(current_manifest)
        window.context = new.CiphertextElectionContext(
            currentContext.number_of_guardians,
            currentContext.quorum,
            new.ElementModP(
                BigInt("0x" + currentContext.elgamal_public_key.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.commitment_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.manifest_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_base_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_extended_base_hash.data)),
            null
        )
        window.available_selections = \
            current_manifest.contests[0].ballot_selections
        window.encryption_seed = new.ElementModQ(BigInt("0x" + self.seed))

        window.voter_id = self.voter_id
        window.create_encrypted_ballot = self.create_encrypted_ballot
        window.tracker = getElementByUuid(self.tracker_id)


@template('djelectionguard/vote_success', Document, Card)
class ContestVoteSuccessCard(Div):
    def to_html(self, *content, view, **context):
        voter = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[voter.contest.id])
        )

        return super().to_html(
            H4(
                DoneIcon(),
                _('Your vote has been validated!'),
                style='text-align:center;'
            ),
            Div(
                _('Thank you for your participation.'
                  ' Your secret vote has been taken in account.'
                  ' You can, if you want, close this page.'),
                MDCButtonOutlined(
                    _('Back to my elections'),
                    tag='a', 
                    href=reverse('contest_list'),
                    p=False,
                    style=dict(margin='24px auto')
                ),
                style=dict(
                    margin_top='50px',
                    text_align='center'
                )
            ),
        )


class CodeInput(MDCTextFieldOutlined):
    def __init__(self, i):

        super().__init__(
            Input(
                type='text',
                name=f'code_{i}',
                maxlength=1
            ),
            label=_('number %(num)s', num=i),
            data_sequence=i
        )

    class HTMLElement:
        def connectedCallback(self):
            this.addEventListener('keyup', this.onkey.bind(this))
            this.addEventListener('touchend', this.onkey.bind(this))

        def get_focus(self, event):
            if this.dataset.sequence == 0:
                this.focus_now()
            return True

        def focus(self):
            setTimeout(lambda: this.focus_now(), 10)

        def focus_now(self):
            this.input().focus()
            this.field().focus()

        def prev_elem(self):
            prev_sequence = parseInt(this.dataset.sequence) - 1
            if prev_sequence < 0:
                prev_sequence = 0

            return this.parentElement.querySelector(
                "[data-sequence='" + prev_sequence + "']"
            )

        def next_elem(self):
            next_sequence = parseInt(this.dataset.sequence) + 1
            if next_sequence > 5:
                next_sequence = 5

            return this.parentElement.querySelector(
                "[data-sequence='" + next_sequence + "']"
            )

        def input(self):
            return this.querySelector('input')

        def field(self):
            return this.querySelector('.mdc-text-field').MDCTextField

        def onkey(self, event):
            if parseInt(event.key) in range(10):
                this.input().value = event.key
                if this.next_elem():
                    this.next_elem().focus()
            elif event.key in ['Backspace', 'Delete']:
                if this.input().value == '':
                    prev = this.prev_elem()
                    if prev:
                        prev.input().value = ''
                        prev.field().focus()
                else:
                    this.input().value = ''
            elif event.key == 'Enter' and this.dataset.sequence == '5':
                this.closest('form').submit()


class ExpiryCounter(MDCButton):
    tag = 'expiry-counter'
    sass = '''
    expiry-counter[disabled]
        pointer-events: none
        background-color: lightgray !important
    '''

    def __init__(self, voter):
        time_remaining = max(0, (voter.token_expiry - timezone.now()).seconds)
        super().__init__(
            time_remaining
            and _('%(sec)ss remaining', sec=60 * 3)
            or _('retry'),
            p=False,
            disabled=True,
            data_wording=_('remaining'),
            data_retry=_('retry'),
            data_time_remaining=time_remaining,
            data_still_valid=time_remaining,
            data_redirect=reverse(
                'voter_phone',
                args=[voter.contest_id, voter.id]
            )
        )

    class HTMLElement:
        def connectedCallback(self):
            this.disabled = True
            this.remaining = parseInt(this.dataset.timeRemaining)
            this.addEventListener('click', this.onclick.bind(this))
            this.start()

        def onclick(self, event):
            if this.dataset.stillValid != '0':
                this.disable()
                event.preventDefault()
                event.stopPropagation()
                return False
            else:
                location.href = this.dataset.redirect
            return True

        def disable(self):
            this.dataset.stillValid = 1
            this.setAttribute('disabled', True)

        def enable(self):
            this.dataset.stillValid = 0
            this.removeAttribute('disabled')
            label = this.querySelector('.mdc-button__label')
            if label:
                label.innerHTML = this.dataset.retry

        def set_value(self, remaining):
            value = remaining + 's ' + this.dataset.wording
            label = this.querySelector('.mdc-button__label')
            if label:
                label.innerHTML = value

        def start(self):
            this.remaining = this.remaining - 1
            if this.remaining > 0:
                this.set_value(this.remaining)
                setTimeout(this.start.bind(this), 1000)
            else:
                this.enable()


@template('voter_verify', Document, Card)
class VoterVerifyCard(Div):
    sass = '''
        form.form.no-notch
            .MDCField
                display: inline-flex
                width: 64px
                height: 96px
                label
                    height: 100%
                    input
                        font-size: 32px
                        height: 100%
                        text-align: center
            .mdc-notched-outline__notch
                display: none
    '''

    def to_html(self, *content, view, form, **context):
        voter = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[voter.contest.id])
        )

        return super().to_html(
            H4(
                _('Provide a phone number to receive your voting code'),
                style=dict(
                    text_align='center'
                )
            ),
            Div(
                Form(
                    CSRFInput(view.request),
                    Div(
                        *[
                            CodeInput(i) for i in range(6)
                        ],
                        style=dict(
                            display='flex',
                            flex_flow='row nowrap',
                            justify_content='space-around',
                            max_width='600px',
                            margin='0 auto'
                        )
                    ),
                    Div(
                        ExpiryCounter(voter),
                        MDCButton(
                            _('Verify my code'),
                        ),
                        style=dict(display='flex')
                    ),
                    method='POST',
                    cls='form no-notch'
                )
            )
        )


@template('voter_phone', Document, Card)
class VoterPhoneCard(Div):
    def to_html(self, *content, view, form, **context):
        voter = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[voter.contest_id])
        )

        return super().to_html(
            H4(
                _('Provide a phone number to receive your voting code'),
                style=dict(
                    text_align='center'
                )
            ),
            Div(
                Form(
                    CSRFInput(view.request),
                    form,
                    MDCButton(
                        _('Send a SMS'),
                    ),
                    method='POST',
                    cls='form'
                )
            )
        )
