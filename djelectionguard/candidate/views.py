from django.contrib import messages
from django.views import generic
from django.views.generic.edit import FormMixin
from django.urls import reverse

from djlang.utils import gettext as _
from djelectionguard.views import Enforce2FAMixin

from ..permissions import *
from ..models import Candidate

from .components import *
from .forms import CandidateForm


class CandidateListView(ContestPermissionMixin, generic.DetailView):
    template_name = 'djelectionguard/candidate_list.html'
    permission_chain = [
        user_can_see_contest
    ]


class CandidateCreateView(
    Enforce2FAMixin,
    ContestPermissionMixin,
    FormMixin,
    generic.DetailView
):
    template_name = 'djelectionguard/candidate_form.html'
    permission_chain = [
        user_is_mediator_of_contest,
        contest_is_not_opened_yet,
    ]
    form_class = CandidateForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.instance.contest = self.get_object()
        return form

    def form_valid(self, form):
        form.save()
        messages.success(
            self.request,
            _('You have added candidate') + f' {form.instance}',
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('contest_candidate_create', args=[self.get_object().id])


class CandidateUpdateView(Enforce2FAMixin, CandidatePermissionMixin, generic.UpdateView):
    pk_url_kwarg = 'cpk'
    template_name = 'djelectionguard/candidate_update.html'
    permission_chain = [
        candidate_user_is_mediator,
        candidate_contest_not_opened
    ]
    model = Candidate
    form_class = CandidateForm

    def get_form(self, *args, **kwargs):
        form = super().get_form()
        form.contest = self.get_object().contest
        return form

    def get_success_url(self):
        contest = self.get_object().contest
        messages.success(
            self.request,
            _('You have updated candidate') + f'{self.object}',
        )
        return reverse('contest_candidate_create', args=(contest.id,))


class CandidateDeleteView(Enforce2FAMixin, CandidatePermissionMixin, generic.DeleteView):
    pk_url_kwarg = 'cpk'
    permission_chain = [
        candidate_user_is_mediator,
        candidate_contest_not_opened
    ]

    def dispatch(self, request, *args, **kwargs):
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)
        return self.delete(request, *args, **kwargs)

    def form_valid(self, form):
        if form.instance.profile == 'blank':
            form.add_error(None, _('This candidate cannot be deleted'))
            return self.form_invalid(form)
        return super().form_valid(form)

    def get_success_url(self):
        contest = self.get_object().contest
        messages.success(
            self.request,
            _('You have removed candidate') + f'{self.object}',
        )
        return reverse('contest_candidate_create', args=(contest.id,))
