from djlang.utils import gettext as _

from djelectionguard.components import *


class AddCandidateAction(ListAction):
    def __init__(self, obj):
        num_candidates = obj.candidate_set.count()
        kwargs = dict(
            tag='a',
            href=reverse('contest_candidate_create', args=[obj.id]))
        if num_candidates and num_candidates >= obj.number_elected:
            btn_comp = MDCButtonOutlined(_('edit'), False, **kwargs)
            icon = DoneIcon()
        else:
            btn_comp = MDCButtonOutlined(_('add'), False, 'add', **kwargs)
            icon = TodoIcon()

        number = obj.number_elected
        txt = _(
            '%(candidates)d candidates, minimum: %(elected)d',
            n=num_candidates,
            candidates=num_candidates,
            elected=number
        )

        super().__init__(
            _('Add candidates'), txt, icon, btn_comp,
        )


class CandidateDetail(Div):
    def __init__(self, candidate, editable=False, **kwargs):
        if editable:
            kwargs['tag'] = 'a'
            kwargs['href'] = reverse(
                'contest_candidate_update',
                args=[candidate.contest.id, candidate.id]
            )
            kwargs['style'] = 'margin-left: auto; margin-top: 12px;'

        extra_style = 'align-items: baseline;'
        content = []

        if candidate.picture:
            extra_style = ''
            content.append(
                Div(
                    Image(
                        loading='eager',
                        src=candidate.picture.url,
                        style='width: 100%;'
                              'display: block;'
                    ),
                    style='width: 100px; padding: 12px;'
                )
            )

        subcontent = Div(
            H5(
                candidate,
                style=dict(
                    margin_top='6px',
                    margin_bottom='6px',
                    word_break='break-word'
                )
            ),
            I(
                candidate.subtext,
                style=dict(
                    font_size='small',
                    font_weight='initial',
                    word_break='break-word',
                )
            ),
            Br(),
            A(
                _('CANDIDATE_DOCUMENT_LINK'),
                href=candidate.document.url,
                target='_blank',
                style=dict(
                    font_size='small',
                    font_weight='initial',
                    word_break='break-word',
                )
            ) if candidate.document else '',
            style='flex: 1 1 65%; padding: 12px;'
        )

        if candidate.description:
            description = Markdown(candidate.description)
            subcontent.addchild(
                Div(
                    description,
                    style='margin-top: 24px; word-break: break-word;'
                )
            )

        content.append(subcontent)

        if editable and not candidate.description:
            content.append(
                MDCButtonOutlined(_('edit'), False, 'edit', **kwargs)
            )
        elif editable:
            subcontent.addchild(
                MDCButtonOutlined(_('edit'), False, 'edit', **kwargs)
            )

        if 'style' not in kwargs:
            kwargs['style'] = ''

        super().__init__(
            *content,
            style='padding: 12px;'
                  'display: flex;'
                  'flex-flow: row wrap;'
                  'justify-content: center;'
                  + kwargs.pop('style')
                  + extra_style,
            cls='candidate-detail',
        )


class CandidateAccordionItem(MDCAccordionSection):
    def __init__(self, candidate, editable=False):
        super().__init__(
            CandidateDetail(candidate, editable),
            label=candidate,
        )


class CandidateAccordion(MDCAccordion):
    def __init__(self, contest, editable=False):
        super().__init__(
            *(
                CandidateAccordionItem(candidate, editable)
                for candidate
                in contest.candidate_set.all()
            ) if contest.candidate_set.count()
            else [_('No candidate yet.')]
        )


class CandidateListComp(MDCList):
    def __init__(self, contest, editable=False):
        qs = contest.candidate_set.all()

        super().__init__(
            *(
                MDCListItem(candidate)
                for candidate in qs
            ) if qs.count()
            else [_('No candidate yet.')]
        )


class CandidatesSettingsCard(Div):
    def __init__(self, view, **context):
        contest = view.get_object()
        editable = (view.request.user == contest.mediator
                    and not contest.actual_start)
        kwargs = dict(p=False, tag='a')
        if contest.candidate_set.count():
            if editable:
                kwargs['href'] = reverse(
                    'contest_candidate_create',
                    args=[contest.id]
                )
                btn = MDCButtonOutlined(_('view all/edit'), **kwargs)
            else:
                kwargs['href'] = reverse(
                    'contest_candidate_list',
                    args=[contest.id]
                )
                btn = MDCButtonOutlined(_('view all'), **kwargs)
        else:
            if editable:
                kwargs['href'] = reverse(
                    'contest_candidate_create',
                    args=[contest.id]
                )
                btn = MDCButtonOutlined(_('add'), icon='add', **kwargs)
            else:
                btn = None

        super().__init__(
            H5(_('Candidates')),
            CandidateListComp(contest, editable),
            btn,
            cls='setting-section'
        )


class ContestCandidateForm(Div):
    def __init__(self, form):
        self.form = form
        self.count = 0
        if form.instance and form.instance.description:
            self.count = len(form.instance.description)
        super().__init__(form)

    def init_counter(form_id, count):
        form = getElementByUuid(form_id)
        counter = form.querySelector('.mdc-text-field-character-counter')
        counter.innerHTML = count + '/300'

    def update_counter(event):
        field = event.currentTarget
        current_count = field.value.length
        if current_count > 300:
            field.value = field.value.substr(0, 300)
            current_count = 300
        parent = field.parentElement.parentElement.parentElement
        counter = parent.querySelector('.mdc-text-field-character-counter')
        counter.innerHTML = current_count + '/300'

    def py2js(self):
        self.init_counter(self.id, self.count)
        field = document.getElementById('id_description')
        field.addEventListener('keyup', self.update_counter)


@template('djelectionguard/candidate_form.html', Document, Card)
class ContestCandidateCreateCard(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        editable = (view.request.user == contest.mediator
                    and not contest.actual_start)
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )
        form_component = ''
        if editable:
            form_component = Form(
                ContestCandidateForm(form),
                CSRFInput(view.request),
                MDCButton(_('Add candidate'), icon='person_add_alt_1'),
                method='POST',
                cls='form')
        count = contest.candidate_set.count()
        return super().to_html(
            H4(
                _('%(count)s Candidates', n=count, count=count),
                cls='center-text'
            ),
            CandidateAccordion(contest, editable),
            H5(_('Add a candidate'), cls='center-text'),
            form_component,
            cls='card'
        )


@template('djelectionguard/candidate_update.html', Document, Card)
class ContestCandidateUpdateCard(Div):
    def to_html(self, *content, view, form, **context):
        candidate = view.get_object()
        contest = candidate.contest
        self.backlink = BackLink(
            _('back'),
            reverse('contest_candidate_create', args=[contest.id]))
        delete_btn = MDCTextButton(
            _('delete'),
            'delete',
            tag='a',
            href=reverse(
                'contest_candidate_delete',
                args=[contest.id, candidate.id]
            )
        )

        return super().to_html(
            H4(
                _('Edit candidate'),
                style='text-align: center;'
            ),
            Form(
                CSRFInput(view.request),
                ContestCandidateForm(form),
                Div(
                    Div(delete_btn, cls='red-button-container'),
                    MDCButton(_('Save'), True),
                    style='display: flex; justify-content: space-between'),
                method='POST',
                cls='form'),
            cls='card'
        )


@template('djelectionguard/candidate_list.html', Document, Card)
class CandidateList(Div):
    def to_html(self, *content, view, **context):
        contest = view.get_object()
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id])
        )

        return super().to_html(
            H4(_('Candidates'), cls='center-text'),
            CandidateAccordion(
                contest,
                view.request.user == contest.mediator
                and not contest.actual_start
            )
        )
