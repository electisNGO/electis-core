import re
from django import forms

from ryzom_django_mdc.html import *

from djlang.utils import gettext as _

from djelectionguard_tezos.interface import contract_file
from ..models import Candidate


class CandidateForm(forms.ModelForm):
    description = forms.CharField(
        label=_('Description'),
        widget=forms.Textarea(
            attrs=dict(
                rows=3,
                maxlength=300
            )
        ),
        required=False,
        help_text=Div(
            Div('0/300', cls='mdc-text-field-character-counter'),
            cls='mdc-text-field-helper-line'
        )
    )
    subtext = forms.CharField(
        required=False,
        label=_('Sous-texte'),
    )
    picture = forms.ImageField(
        widget=forms.FileInput(
            attrs=dict(
                label=_('Select image'),
                empty_value=_('No file selected')
            ),
        ),
        label=_('CANDIDATE_PICTURE'),
        required=False
    )
    document = forms.FileField(
        widget=forms.FileInput(
            attrs=dict(
                label=_('Select file'),
                empty_value=_('No file selected')
            ),
        ),
        label=_('CANDIDATE_DOCUMENT'),
        required=False
    )
    def clean_name(self):
        name = self.cleaned_data['name']
        if self.instance.contest.candidate_set.filter(
            name=name
        ).exclude(pk=self.instance.pk):
            raise forms.ValidationError(
                f'{name} already added!'
            )

        if contract_file != 'election_compiled.json':
            if not re.match('^tz[123][a-zA-Z0-9]{33}$', name):
                raise forms.ValidationError(
                    '[name] should be a wallet address'
                )

        return name

    def save(self):
        self.instance.sequence = self.instance.contest.candidate_sequence
        self.instance.save()

    class Meta:
        model = Candidate
        fields = [
            'name',
            'picture',
            'subtext',
            'description',
            'document',
        ]

        labels = {
            'name': _('CANDIDATE_NAME'),
            'description': _('CANDIDATE_DESCRIPTION'),
            'picture': _('CANDIDATE_PICTURE')
        }
