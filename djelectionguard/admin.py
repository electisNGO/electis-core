from django.contrib import admin

from .models import Candidate, Contest, Guardian, Voter


class CandidateInline(admin.TabularInline):
    model = Candidate
    fields = ('name',)


class GuardianInline(admin.TabularInline):
    model = Guardian


class ContestAdmin(admin.ModelAdmin):
    inlines = [CandidateInline, GuardianInline]
    list_display = (
        'name',
        'mediator',
        'start',
        'end',
    )


admin.site.register(Contest, ContestAdmin)


class VoterAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'contest',
    )
    search_fields = (
        'user__email',
        'user__first_name',
        'user__last_name',
        'contest__name',
    )
    list_filter = (
        'casted_at',
    )
    readonly_fields = (
        'casted_at',
        'open_email_sent',
        'close_email_sent',
    )


admin.site.register(Voter, VoterAdmin)
