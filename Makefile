.PHONY: bundle run daphne tally consent redis

export MEDIA_ROOT := /app/media
# export LOG_DIR := /app/log
# export IPFS_URL := ipfs:export 5001 
# export DB_HOST := postgres 
# export DB_PORT := 5432 
# export DB_NAME:= $(whoami)
# export DB_USER := $(whoami)
# export DB_PASS := $(whoami)
# export DB_ENGINE := django.db.backends.postgresql
# export DEBUG := true
export PYTHONPATH := $(shell pwd):$(shell pwd)/..:$(shell pwd)/../electis
export DJANGO_APP := electis
export DJANGO_SETTINGS_MODULE :=electis.settings 
# export WS_PORT := 8081


bundle: 
	DEBUG=1 poetry run python manage.py ryzom_bundle
	DEBUG=1 poetry run python manage.py compilemessages
	DEBUG=1 poetry run python manage.py compilescss
	DEBUG=1 poetry run python manage.py makemigrations --noinput
	DEBUG=1 poetry run python manage.py migrate --noinput
	# FileNotFoundError: [Errno 2] No such file or directory: '/Users/pierre/repos/electis/managed-cse/electis-core/tests/static'
	DEBUG=1 poetry run python manage.py collectstatic --noinput
	# AttributeError: 'Settings' object has no attribute 'SITE_DATA'
	DEBUG=1 poetry run python manage.py load_default_site 
	DEBUG=1 poetry run python manage.py djtezos_load

run:
 	# @echo $(PYTHONPATH)
	poetry run python manage.py runserver


daphne:
	PYTHONPATH=`pwd` poetry run daphne -p 8081 electeez_common.asgi:application

tally:
	while true; do poetry run python manage.py tally; sleep 60; done

consent:
	while true; do poetry run python manage.py update_consent; sleep 3600; done

redis:
	brew services start redis