import * as taquito from '@taquito/taquito'
import * as taquito_utils from '@taquito/utils'
import * as wallet from '@taquito/beacon-wallet'

window.taquito = taquito
window.tu = taquito_utils
window.wallet = wallet
