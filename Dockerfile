FROM python:3.9-bullseye

RUN useradd --home-dir /app --uid 1000 app && mkdir -p /app/log /app/media && chown -R app /app
WORKDIR /app

# Set up poetry so it install system wide
ENV POETRY_INSTALLER_MAX_WORKERS=10

# RUN curl -sSL https://install.python-poetry.org | python3 -
RUN pip install -U pip setuptools && pip install poetry==1.6
RUN apt-get update && apt-get install -y xfonts-75dpi xfonts-base libsodium23 libsecp256k1-0 gettext bash flite libgmp-dev libmpfr-dev libmpc-dev gcc locales 
RUN sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.bullseye_amd64.deb && dpkg -i wkhtmltox_0.12.6.1-2.bullseye_amd64.deb
# use arm on apple silicon
# RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.bullseye_arm64.deb && dpkg -i wkhtmltox_0.12.6.1-2.bullseye_arm64.deb

ENV PATH="/app/node_modules/.bin:/app/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
ENV PYTHONIOENCODING=UTF-8 PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

RUN mkdir -p /spooler/blockchain /spooler/email /spooler/tally && chown -R app /spooler

# Install Depedencies
USER app
COPY electis-core/pyproject.toml /app/
COPY electis-core/poetry.lock /app/
RUN poetry install --no-directory
ENV PYTHONPATH=/app:/app/electis-core

# To enable rysom to write in it
# RUN chown app:app -R /usr/local/lib/python3.9/site-packages/*
COPY --chown=app:app . /app/
RUN DEBUG= poetry run python ./manage.py ryzom_bundle
RUN DEBUG= poetry run python ./manage.py compilemessages

EXPOSE 8000

CMD /bin/bash -euxc "until poetry run djcli dbcheck; do sleep 1; done \
  && poetry run python ./manage.py run_multiple \
  compilescss 'collectstatic --noinput' 'migrate --noinput' load_default_site djtezos_load \
  && find public -type f | xargs gzip -f -k -9 \
  && poetry run uwsgi \
  --http-socket=0.0.0.0:8000 \
  --chdir=/app \
  --plugin=python \
  --spooler=/spooler/blockchain \
  --spooler=/spooler/email \
  --spooler-processes=4 \
  --spooler-frequency=1 \
  --spooler-chdir=/app \
  --module=electeez_common.wsgi:application \
  --http-keepalive \
  --harakiri=1024 \
  --max-requests=1000 \
  --master \
  --workers=32 \
  --chmod=666 \
  --log-5xx \
  --vacuum \
  --enable-threads \
  --post-buffering=8192 \
  --ignore-sigpipe \
  --ignore-write-errors \
  --disable-write-exception \
  --mime-file /etc/mime.types \
  --thunder-lock \
  --offload-threads '%k' \
  --route '^/static/.* addheader:Cache-Control: public, max-age=7776000' \
  --route '^/js|css|fonts|images|icons|favicon.png/.* addheader:Cache-Control: public, max-age=7776000' \
  --static-map /static=/app/public \
  --static-map /media=/app/media \
  --static-gzip-all"
