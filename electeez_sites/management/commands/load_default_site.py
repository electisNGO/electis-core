from django.db import (
    DEFAULT_DB_ALIAS, connections, transaction
)
from django.apps import apps
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Load default site config'

    def handle(self, *args, **options):

        self.using = DEFAULT_DB_ALIAS

        with transaction.atomic(using=self.using):
            Site = apps.get_model('electeez_sites', 'Site')
            if not Site.objects.first():
                call_command('loaddata', settings.SITE_DATA)

            site = Site.objects.filter(name=settings.HOST).first()
            if not site:
                site = Site.objects.first()
                site.domain = site.name = settings.HOST
                site.save()

            User = apps.get_model('electeez_auth', 'User')
            for email in settings.ADMINS_EMAIL:
                if not User.objects.filter(email=email).first():
                    User.objects.create_user(
                        email=email,
                        is_staff=True,
                        is_active=True,
                        is_superuser=True
                    )

        if transaction.get_autocommit(self.using):
            connections[self.using].close()
