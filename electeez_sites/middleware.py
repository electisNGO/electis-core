from django import http

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.contrib import messages
from django.utils.translation import activate

from djlang.utils import gettext as _


class HealthCheckMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path == '/health/':
            return http.HttpResponse('ok')
        return self.get_response(request)


class DynamicSiteDomainMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        cached = cache.get(request.get_host())
        if cached:
            current_site = cached
        else:
            try:
                current_site = Site.objects.get(domain=request.get_host())
                cache.set(request.get_host(), current_site, 60 * 60)

            except Site.DoesNotExist:
                return http.HttpResponseNotFound()

        request.current_site = current_site
        if request.path_info != '/':
            path_info = request.path_info
            if request.path_info.startswith('/'):
                path_info = path_info[1:]
            iso, path = path_info.split('/', 1) if '/' in path_info else (None, path_info)
            if iso in [k for k, v in settings.LANGUAGES]:
                response = None
                default_language = current_site.language_set.filter(default=True).first()
                default_iso = default_language and default_language.iso or settings.LANGUAGE_CODE
                cookie_iso = request.COOKIES.get('prefered_language', default_iso)
                if cookie_iso != iso:
                    response = http.HttpResponseRedirect(f"/{cookie_iso}/{path}")

                user = request.user.is_authenticated and request.user or None
                if user:
                    if pl := user.preferedlanguage_set.first():
                        if pl.language.iso != iso:
                            activate(pl.language.iso)
                            messages.info(request, str(_("Redirected to %(lang)s", lang=pl.language.name)))
                            response = http.HttpResponseRedirect(f"/{pl.language.iso}/{path}")
                if response:
                    return response

        settings.SITE_ID = current_site.id

        response = self.get_response(request)
        return response
