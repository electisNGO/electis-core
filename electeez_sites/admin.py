from django.contrib import admin

from django.contrib.sites.models import Site as LegacySite

from .models import Site, CompanyInformations


admin.site.unregister(LegacySite)


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = ('domain', 'name')
    search_fields = ('domain', 'name')


@admin.register(CompanyInformations)
class CompanyInformationsAdmin(admin.ModelAdmin):
    pass
