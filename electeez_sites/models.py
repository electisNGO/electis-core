from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site, SiteManager
from django.core.cache import cache

from colorfield.fields import ColorField

from electeez_consent.models import Provider


class SiteManager(SiteManager):
    def get_current(self):
        cached = cache.get(f'electeez_site:{settings.SITE_ID}')
        if cached:
            return cached
        site = Site.objects.get(id=settings.SITE_ID)
        cache.set(f'electeez_site:{settings.SITE_ID}', site)
        return site


class Site(Site):
    contact_email = models.EmailField(default='contact@electis.app')
    sender_email = models.EmailField(default='contact@electis.app')

    enforce_2fa = models.BooleanField(default=False)
    all_users_can_create = models.BooleanField(default=False)
    all_results_are_visible = models.BooleanField(default=False)
    registration_allowed = models.BooleanField(default=False)
    max_voters = models.PositiveIntegerField(default=0)

    footer_url = models.CharField(
        max_length=255,
        default='https://electis.com'
    )

    banner_url = models.ImageField(
        upload_to=f'{settings.STATIC_ROOT_DIR}/banner',
        null=True,
        blank=True
    )

    banner_color = ColorField(default='#000000')
    banner_text_color = ColorField(default='#2e2e2e')
    banner_link_color = ColorField(default='#0075eb')

    background_color = ColorField(default='#ffffff')
    footer_color = ColorField(default='#ffffff')
    footer_link_color = ColorField(default='#0075eb')
    primary_color = ColorField(default='#0075eb')
    secondary_color = ColorField(default='#2e2e2e')

    email_banner_fg = models.CharField(
        max_length=7,
        default='#000000'
    )

    email_banner_bg = models.CharField(
        max_length=7,
        default='#ffffff'
    )

    email_banner = models.ImageField(
        upload_to=f'{settings.STATIC_ROOT_DIR}/images',
        null=True,
        blank=True
    )

    email_banner_url = models.URLField(
        null=True, blank=True
    )

    email_footer_fg = models.CharField(
        max_length=7,
        default='#000000'
    )

    email_footer_bg = models.CharField(
        max_length=7,
        default='#ffffff'
    )

    email_footer = models.ImageField(
        upload_to=f'{settings.STATIC_ROOT_DIR}/images',
        null=True,
        blank=True
    )

    email_footer_url = models.URLField(
        null=True, blank=True
    )

    consent_provider = models.ForeignKey(
        Provider,
        on_delete=models.SET_NULL,
        blank=True, null=True
    )

    objects = SiteManager()

    @property
    def short_code(self):
        return f"access_{self.domain}"

    @property
    def access_code(self):
        return f"electeez_sites.{self.short_code}"

    @property
    def permission(self):
        return Permission.objects.filter(codename=self.short_code).first()

    def has_user(self, user):
        return user.has_perm(self.access_code)


@receiver(post_save, sender=Site)
def update_cache(sender, instance, **kwargs):
    cache.set(f'electeez_site:{instance.id}', instance)


class CompanyInformations(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    postal_code = models.CharField(max_length=5, null=True, blank=True)
    city = models.CharField(max_length=25, null=True, blank=True)
    siren = models.CharField(max_length=9, null=True, blank=True)

    site = models.OneToOneField(
        Site,
        on_delete=models.CASCADE,
        related_name='company_informations',
        null=True, blank=True
    )

    def __str__(self):
        return self.name
