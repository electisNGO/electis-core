from django.apps import AppConfig


class ElecteezConsentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'electeez_consent'
