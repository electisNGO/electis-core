# Generated by Django 3.2.17 on 2023-04-22 06:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('electeez_consent', '0009_alter_provider_provider_class'),
    ]

    operations = [
        migrations.AddField(
            model_name='provider',
            name='public',
            field=models.BooleanField(default=False),
        ),
    ]
