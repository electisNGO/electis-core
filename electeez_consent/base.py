class ProviderBase:
    manual = False
    from_file = False
    required_keys = []

    def __init__(self, provider):
        self.provider = provider
        self.parameters = self.get_parameters()

    def get_parameters(self):
        params = self.parameters
        for k in self.required_keys:
            if k in params:
                setattr(self, k, params[k])
            else:
                setattr(self, k, '')
            params[k] = getattr(self, k)
        return params

    @property
    def parameters(self):
        return self.provider.parameters

    @parameters.setter
    def parameters(self, value):
        self.provider.parameters = value

    def create_consent(self, user):
        return self.update_consent(user, 'CONFIRMED')

    def remove_consent(self, user):
        return self.update_consent(user, 'WITHDRAWN')
