import requests

from datetime import datetime, timedelta

from djlang.utils import gettext as _

from .base import ProviderBase


class Provider(ProviderBase):
    required_keys = [
        'api_url',
        'oauth_url',
        'purpose_id',
        'client_id',
        'client_secret',
        'api_key',
        'last_fetch'
    ]

    @property
    def access_token_data(self):
        if self.last_fetch:
            print('Trying last token transaction')
            token_data = self.last_fetch
            if 'expiration_datetime' in token_data:
                dt = datetime.fromisoformat(token_data['expiration_datetime'])
                if datetime.now() < dt:
                    print('Last token still valid, using')
                    self.access_token_data = token_data
                else:
                    print('Token expired')
                    self.access_token_data = {}
            else:
                print('Token malformed')

        token_data = getattr(self, '_access_token_data', {})
        if not token_data:
            self.request_access_token()

        return self._access_token_data

    @access_token_data.setter
    def access_token_data(self, value):
        self.last_fetch = value
        self._access_token_data = value
        new_params = self.parameters
        new_params['last_fetch'] = self.last_fetch
        self.parameters = new_params

    def request_access_token(self):
        print('Consent: request access_token')
        url = self.oauth_url + '/api/access/v1/oauth/token'
        data = dict(
            grant_type='client_credentials',
            client_id=self.client_id,
            client_secret=self.client_secret
        )
        response = requests.post(url, data=data, files=data)

        if response.status_code == 200:
            access_token_data = response.json()
            exp_in = access_token_data['expires_in']
            exp_date = datetime.now() + timedelta(seconds=exp_in)
            access_token_data['expiration_datetime'] = exp_date.isoformat()
            self.access_token_data = access_token_data
            return True

        return response.content

    def update_consent(self, user, value):
        url = self.api_url + '/request/v1/consentreceipts'

        data = dict(
            identifier=user.email,
            requestInformation=self.api_key,
            purposes=[
                dict(Id=self.purpose_id, TransactionType=value)
            ]
        )

        response = requests.post(url, json=data)

        if response.status_code == 200:
            return True

        return False

    def get_updates(self, date):
        base_url = (
            self.oauth_url
            + '/api/consentmanager/v1/purposes/'
            f'{self.purpose_id}/datasubjects/'
        )
        base_query = '?size=100&transactionStatus=ACTIVE'
        page = 0
        total_pages = 1
        user_emails = []
        while page < total_pages:
            if 'access_token' in self.access_token_data:
                token = self.access_token_data['access_token']
                headers = dict(Authorization=f'Bearer {token}')

                url = base_url + base_query + f'&page={page}'
                print(f'Requesting url {url}')
                res = requests.get(url, headers=headers)
                if res.status_code != 200:
                    return (
                        False,
                        _(
                            'OneTrust server responded with %(msg)s',
                            msg=str(res.content)
                        )
                    )

                data = res.json()
                for subject in data['content']:
                    user_emails.append(subject['DataSubjectId'])

                total_pages = data['totalPages']
                page += 1
            else:
                return (False, _('Could not get valid access_token'))

        return (True, user_emails)
