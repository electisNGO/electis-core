import csv
import io
import hashlib
import random
import pandas

from datetime import datetime
from phonenumber_field.modelfields import PhoneNumber

from electeez_consent.base import ProviderBase
from electeez_auth.models import User, IDToken


class BaseParser:
    def __init__(self, **params):
        pass

    def save(self, form):
        pass

    def parse_file(self, form):
        voters_file = form.cleaned_data['voters_file']
        if voters_file.content_type in ['text/csv']:
            return self.parse_csv_file(voters_file)
        else:
            data = pandas.read_excel(voters_file)
            return [d[1] for d in data.iterrows()]

    def parse_csv_file(self, file):
        data = file.read()
        buffer = io.StringIO(data.decode())
        sniffer = csv.Sniffer()
        buffer.seek(0)
        dialect = sniffer.sniff(buffer.read(), ',;\t|')
        buffer.seek(0)
        return csv.DictReader(buffer, dialect=dialect)


class EduParser(BaseParser):
    def save(self, form):
        content = self.parse_file(form)
        users = list()
        tokens = list()

        # Create a filled row for each entry
        for i, row in content:
            email = row.get('MAIL', '')
            if isinstance(email, str):
                email = email.lower()
            else:
                email = ''
            first_name = row.get('PRENOM', '').upper()
            last_name = row.get('NOM', '').upper()
            phone_raw = row.get('TEL', '')
            try:
                phone = PhoneNumber.from_string(phone_raw, 'FR')
            except Exception:
                phone = ''
            bdate = row.get('DATE DE NAISSANCE', '')

            user_row = dict(
                email=email, phone=phone, bdate=bdate,
                first_name=first_name, last_name=last_name
            )
            if not phone or not str(phone.national_number)[0] in ['6', '7']:
                user_row['verif_token'] = bdate.day * 100 + bdate.month
            else:
                user_row['verif_token'] = random.randint(0, 10000)

            id_token = random.randint(100_000_000_000, 1_000_000_000_000)
            if id_token in tokens:
                id_token = random.randint(100_000_000_000, 1_000_000_000_000)
            user_row['id_token'] = id_token
            tokens.append(id_token)
            users.append(user_row)

        on_error = []
        final_users = []
        final_voters = []
        print(f"num rows: {len(users)}")

        # For each created row, create a user
        for i, user_row in enumerate(users):
            def create_voter(user):
                if v := user.voter_set.filter(contest=form.instance).first():
                    return v
                return user.voter_set.create(
                    contest=form.instance,
                    token=user_row['verif_token']
                )

            user_data = dict(
                phone=user_row['phone'],
                profile=user_row['id_token'],
                first_name=user_row['first_name'],
                last_name=user_row['last_name'],
                is_active=True,
            )

            existing = None
            if user_row['email']:
                # check if a user with the same email exists
                existing = User.objects.filter(
                    email=user_row['email']
                ).first()

            # if a user is found, let's check its name
            if (existing and existing.first_name == user_data['first_name']
                and existing.last_name == user_data['last_name']):
                # seems like its exactly the same, let's not compare the phone for now
                if existing not in final_users:
                    create_voter(existing)
                    final_users.append(existing)

                continue

            # if a user is found with another name
            # we will consider that it's the same family
            elif existing:
                if known_user := User.objects.filter(
                    first_name=user_data['first_name'],
                    last_name=user_data['last_name']
                ).exclude(pk=existing.pk).first():
                    # We found one with same name different ID
                    # Hope it's the one we're registering.
                    existing = known_user
                else:
                    # User not existing, create it with no email
                    # to avoid conflict with the first one in the same family
                    # maybe we should register it somewhere to send the email
                    # containing the id token
                    existing = User.objects.create_user(
                        email='',
                        wallet=user_row['id_token'],
                        contact_email=user_row['email'],
                        **user_data
                    )

                if existing not in final_users:
                    create_voter(existing)
                    final_users.append(existing)

                continue

            # If we get here we did not find a user with that mail
            # Either it's the first time we load this file, either the
            # User has no mail

            # Check if a user with the same name exsists
            existing = User.objects.filter(
                first_name=user_data['first_name'],
                last_name=user_data['last_name']
            ).first()

            if existing and existing.phone == user_data['phone']:
                if existing not in final_users:
                    create_voter(existing)
                    final_users.append(existing)

                continue

            # Not the same phone ? it's another one, create it
            # If not existing, create it
            if email := user_row['email']:
                existing = User.objects.create_user(
                    email=email,
                    contact_email=email,
                    **user_data
                )
            else:
                existing = User.objects.create_user(
                    email='',
                    wallet=user_row['id_token'],
                    **user_data
                )
            if existing not in final_users:
                create_voter(existing)
                final_users.append(existing)

            continue

        final_users = list(set(final_users))
        print(f"final users: {len(final_users)}")

        # voter = existing.voter_set.filter(contest=form.instance).first()
        # if not voter:
            # voter = form.instance.voter_set.create(user=existing, token=user_row['verif_token'])

        # final_voters.append(voter)

        print(f"final voter: {len(final_voters)}")
        print(f"on error: {on_error}")

        form.instance.voter_set.exclude(user__id__in=[u.id for u in final_users]).delete()

        print('parsing finished')

        return form.instance


class NIRParser(BaseParser):
    def save(self, form):
        content = self.parse_file(form)
        users = dict()
        for row in content:
            email = row.get("Email").lower()
            nir_raw = row.get("NIR")
            nir = nir_raw and nir_raw.replace(" ", "") or ""
            if email in users.keys():
                print(f"Email {email} already in the list")
            users[email] = nir

        emails = users.keys()
        # Create missing users
        for email in emails:
            User.objects.create_user(email=email)

        users_not_having_voter = User.objects.filter(email__in=emails).exclude(voter__contest=form.instance)

        # Create missing voters
        for user in users_not_having_voter:
            form.instance.voter_set.create(user=user, token=users[user.email])

        # Remove the rest
        for voter in form.instance.voter_set.exclude(user__email__in=emails):
            if not voter.casted_at:
                voter.delete()

        return form.instance


class EmailParser(BaseParser):
    def save(self, form):
        content = self.parse_file(form)
        emails = list()
        for row in content:
            email = row.get("Email").lower()
            if email not in emails:
                emails.append(email)

        # Create missing users
        for email in emails:
            User.objects.create_user(email=email)

        users_not_having_voter = User.objects.filter(email__in=emails).exclude(voter__contest=form.instance)

        # Create missing voters
        for user in users_not_having_voter:
            form.instance.voter_set.create(user=user)

        # Remove the rest
        for voter in form.instance.voter_set.exclude(user__email__in=emails):
            if not voter.casted_at:
                voter.delete()

        return form.instance


class IDTokenParser(BaseParser):
    def save(self, form):
        content = self.parse_file(form)
        for row in content:
            bdatefield = 'birthdate'
            tokenfield = 'token'
            for label in ['Birthdate', 'Birthday', 'birthdate', 'birthday']:
                if label in row:
                    bdatefield = label
            for label in ['Token', 'token']:
                if label in row:
                    tokenfield = label

            if bdatefield in row and tokenfield in row:
                if row[bdatefield]:
                    bdatetime = datetime.strptime(row[bdatefield], '%d/%m/%Y')
                    bdate = bdatetime.date().isoformat()
                else:
                    bdate = ''
                encoded = (row[tokenfield] + bdate).encode()
                h = hashlib.sha256(encoded).hexdigest()
                try:
                    if existing_idtoken := IDToken.objects.filter(
                        token=h
                    ).exclude(
                        email=None, used=None
                    ).first():
                        if not form.instance.voter_set.filter(
                            user__email=existing_idtoken.email
                        ).first():
                            if existing_user := User.objects.filter(
                                email=existing_idtoken.email,
                                is_active=True
                            ).first():
                                form.instance.voter_set.create(
                                    user=existing_user
                                )
                                continue
                            else:
                                # token used but user non existant??
                                # let's not consider this case for now
                                continue
                        else:
                            continue
                    elif existing_idtoken := IDToken.objects.filter(
                            token=h
                    ).first():
                        existing_idtoken.contest = form.instance
                        existing_idtoken.save()
                        continue

                    form.instance.idtoken_set.create(token=h)

                except Exception as e:
                    print(e)

        return form.instance


class IDTokenNIRParser(BaseParser):
    def save(self, form):
        content = self.parse_file(form)
        voters_emails = []
        for i, row in content:
            nirfield = 'NIR'
            tokenfield = 'Token'
            for label in ['NIR', 'Nir', 'nir']:
                if row.get(label):
                    nirfield = label
                    break
            for label in ['Token', 'token']:
                if row.get(label):
                    tokenfield = label
                    break

            for label in ['Nom', 'Name', 'nom', 'NOM', 'name', 'NAME']:
                if row.get(label):
                    namefield = label
                    break

            if nirfield in row and tokenfield in row:
                nir = row[nirfield]
                token = row[tokenfield]

                idtoken = IDToken.objects.filter(token=token).first()
                if not idtoken:
                    new_user = User(email='anonymous@electis.io')
                    new_user.anonymise()
                    user = User.objects.create_user(email=new_user.email)
                    if name := row.get(namefield, None):
                        user.profile = name
                    user.is_active = True
                    user.save()
                    idtoken = IDToken.objects.create(
                        email=user.email,
                        token=token,
                        used=datetime.now(),
                        from_qr=False
                    )
                else:
                    user = User.objects.get(email=idtoken.email)

                voter = form.instance.voter_set.filter(user=user).first()
                if not voter:
                    voter = form.instance.voter_set.create(user=user, token=nir)

                voters_emails.append(user.email)

                form.instance.voter_set.exclude(user__email__in=voters_emails).delete()

        return form.instance


class Provider(ProviderBase):
    from_file = True
    manual = True
    default_form_label = 'Please upload your voter list'
    default_parser_class = 'Email'
    parser_classes = dict(
        NIR=NIRParser,
        Email=EmailParser,
        IDToken=IDTokenParser,
        Edu=EduParser,
        IDTokenNIR=IDTokenNIRParser,
    )

    def get_form_label(self):
        return self.parameters.get('form_label') or self.default_form_label

    def get_parser_class(self):
        parser_class = self.parameters.get('parser_class')
        if parser_class and parser_class in self.parser_classes:
            return self.parser_classes[parser_class]
        return self.parser_classes[self.default_parser_class]

    def get_parser(self):
        parser_class = self.get_parser_class()
        return parser_class(**self.parameters)

    def save_form(self, form):
        parser = self.get_parser()
        return parser.save(form)
