from djelectionguard.models import Candidate
from djelectionguard.voter.components import *
from djlang.utils import gettext as _


def name(c):
    return c.name


Candidate.__str__ = name


class ScrollPage(Div):
    def __init__(self, scroller, content, **ctx):
        super().__init__(
            content,
            aria_hidden=True,
            **ctx,
            style=dict(
                width='100%',
                position='absolute'
            )
        )

    class HTMLElement:
        def connectedCallback(self):
            this.scroller = this.closest('scrolling-choice')
            this.scroller.addPage(this)
            this.classList.add('page-num-' + len(this.scroller.pages))

        def onclick(self, event):
            pass

        def show(self):
            this.ariaHidden = False
            buttons = this.querySelectorAll('button')
            for button in buttons:
                button.tabIndex = 0
                button.disabled = False

        def hide(self):
            this.ariaHidden = True
            buttons = this.querySelectorAll('button')
            for button in buttons:
                button.tabIndex = -1
                button.disabled = True


class ScrollingChoice(Div):
    def __init__(self, *pages, **ctx):
        super().__init__(
            Div(
                *[ScrollPage(self, page) for page in pages],
                style=dict(
                    width='100%',
                    overflow='clip',
                    margin_bottom='32px',
                    position='relative'
                ),
                id='scroller-page-container'
            ),
            Div(
                MDCButton(
                    _('prev'),
                    disabled=True,
                    type='button',
                    id='scroller-prev-btn',
                    style='margin-left:unset',
                    data_decr_page=1
                ),
                MDCButton(
                    _('next'),
                    disabled=True,
                    type='button',
                    id='scroller-next-btn',
                    style='margin-left:unset',
                    data_incr_page=1
                ),
                style=dict(
                    display='flex',
                    flex_flow='row nowrap',
                    justify_content='space-between'
                )
            ),
            style=dict(
                display='flex',
                flex_flow='column nowrap'
            )
        )

    class HTMLElement:
        def connectedCallback(self):
            this.pages = []
            this.currentPage = 0
            this.addEventListener('click', this.onclick.bind(this))
            this.addEventListener('change', this.onchange.bind(this))
            window.addEventListener('load', this.setHeight.bind(this))
            window.addEventListener('resize', this.setHeight.bind(this))

        def setHeight(self):
            p = this.pages[this.currentPage]
            container = this.querySelector('#scroller-page-container')
            container.style.height = p.getBoundingClientRect().height + 'px'

            width = this.getBoundingClientRect().width
            for page in this.pages:
                new_x = (page.index - this.currentPage) * width
                page.style.left = new_x + 'px'
                if page.index != this.currentPage:
                    page.hide()
                else:
                    page.show()

        def onclick(self, event):
            this.prev = this.querySelector('#scroller-prev-btn')
            this.next = this.querySelector('#scroller-next-btn')
            if event.target.closest('button') == this.prev:
                this.go_prev()
            elif event.target.closest('button') == this.next:
                this.go_next()

        def onchange(self, event):
            this.prev = this.querySelector('#scroller-prev-btn')
            this.next = this.querySelector('#scroller-next-btn')
            p1 = this.pages[0]
            p2 = this.pages[1]
            p1_checked = 0
            p2_checked = 0
            list_checkboxes = p1.querySelectorAll('input[type=checkbox]')
            for cb in list_checkboxes:
                sub = getElementByUuid(cb.value)
                if not sub and cb.checked:
                    this.next.dataset.incrPage = 2
                    this.prev.dataset.decrPage = 2
                    p1_checked = 1
                elif sub and cb.checked:
                    this.next.dataset.incrPage = 1
                    this.prev.dataset.decrPage = 1
                    sub.style.display = 'block'
                    p1_checked += 1
                    if this.currentPage == 0 and window.tourNumOrder == 1:
                        for c in sub.querySelectorAll('input'):
                            c.checked = True
                        p2_checked = 1
                elif sub:
                    sub.style.display = 'none'
                    for c in sub.querySelectorAll('input'):
                        c.checked = False

            list_checkboxes = p2.querySelectorAll('input[type=checkbox]')
            for cb in list_checkboxes:
                list_item = cb.closest('li')
                list_item_label = list_item.querySelector('label')
                if cb.checked:
                    list_item_label.classList.remove('linethrough')
                    p2_checked += 1
                else:
                    list_item_label.classList.add('linethrough')

            if this.currentPage == 0:
                if p1_checked:
                    this.next.disabled = False
                else:
                    this.next.disabled = True
            if this.currentPage == 1:
                if p2_checked:
                    this.next.disabled = False
                else:
                    this.next.disabled = True

        def addPage(self, page):
            page.index = this.pages.length
            width = this.getBoundingClientRect().width
            page.style.left = width * this.pages.length + 'px'
            this.pages.push(page)

        def go_next(self):
            if not this.currentPage < this.pages.length - 1:
                return

            this.prev.disabled = False
            if this.currentPage == 0 and window.tourNumOrder == 1:
                this.next.disabled = False
            else:
                this.next.disabled = True
            this.currentPage += parseInt(this.next.dataset.incrPage)
            this.setHeight()

        def go_prev(self):
            this.next.disabled = False

            this.currentPage -= parseInt(this.prev.dataset.decrPage)
            if this.currentPage < 0:
                this.currentPage = 0

            if this.currentPage == 0:
                this.prev.disabled = True

            this.setHeight()


class CandidateDetail(Div):
    def __init__(self, candidate, **kwargs):
        extra_style = 'align-items: baseline;'
        content = []

        if candidate.picture:
            extra_style = ''
            content.append(
                Div(
                    Image(
                        loading='eager',
                        src=candidate.picture.url,
                        style='width: 100%;'
                              'display: block;'
                    ),
                    style='width: 100px; padding: 12px;'
                )
            )

        subcontent = Div(
            H5(
                candidate.name,
                style=dict(
                    margin_top='6px',
                    margin_bottom='6px',
                    word_break='break-word'
                )
            ),
            I(
                candidate.subtext,
                style=dict(
                    font_size='small',
                    font_weight='initial',
                    word_break='break-word',
                )
            ),
            A(
                _('CANDIDATE_DOCUMENT_LINK'),
                href=candidate.document.url,
                target='_blank',
                style=dict(
                    font_size='small',
                    font_weight='initial',
                    word_break='break-word',
                )
            ) if candidate.document else '',
            style='flex: 1 1 65%; padding: 12px;'
        )

        if candidate.description:
            description = Markdown(candidate.description)
            subcontent.addchild(
                Div(
                    description,
                    style='margin-top: 24px; word-break: break-word;'
                )
            )

        content.append(subcontent)

        if 'style' not in kwargs:
            kwargs['style'] = ''

        super().__init__(
            *content,
            style='padding: 12px;'
                  'display: flex;'
                  'flex-flow: row wrap;'
                  'justify-content: center;'
                  + kwargs.pop('style')
                  + extra_style,
            cls='candidate-detail',
            **kwargs,
        )


class TrackerDetailCSE(Div):
    def __init__(self, contest, view):
        rows = (
            ('tracker-contest-name', _('Election name'), contest.name),
            ('tracker-contest-id', _('Election ID'), contest.id),
            ('tracker-ballot-id', _('Ballot ID'), ''),
            ('tracker-ballot-hash', _('Ballot Hash'), ''),
        )

        voter = contest.voter_set.filter(user=view.request.user).first()

        wiki = 'https://fr.wikipedia.org/wiki/Fonction_de_hachage'

        return super().__init__(
            Div(
                _('TRACKING_MSG'),
                style=dict(
                    text_align='center',
                    margin_top='32px',
                    margin_bottom='32px',
                    opacity='0.6'
                )
            ),
            Div(
                Div(
                    *[
                        CodeInput(i) for i in range(4)
                    ],
                    style=dict(
                        display='flex',
                        flex_flow='row nowrap',
                        justify_content='space-around',
                        max_width='600px',
                        margin='0 auto',
                    ),
                    cls='code-input-container form no-notch'
                ),
                Div(cls='error-container red', style='display:none'),
                MDCButton(_('Send my ballot')),
                style=dict(
                    display='flex',
                    flex_flow='column nowrap',
                    justify_content='space-around',
                    margin='32px 0',
                    align_items='center',
                )
            ),
            Div(
                B(
                    _('Learn more'),
                    style=dict(
                        text_align='center',
                        display='block'
                    )
                ),
                P(
                    mark_safe(
                        _(
                            'TRACKING_MORE_INFO',
                            link=f'<a href={wiki}>Fonction de hashage</a>'
                        )
                    ),
                    style='text-align: center',
                ),
                style=dict(
                    background='aliceblue',
                    margin_top='32px',
                    padding='12px',
                    opacity='0.6'
                )
            ),
            Table(
                *(Tr(
                    Th(
                        label, ': ',
                        scope='row',
                        cls='overline',
                        style='text-align: right;'
                              'padding: 12px;'
                              'white-space: nowrap;'
                    ),
                    Td(
                        Pre(
                            value,
                            id=_id,
                            style='word-break: break-word;'
                                  'white-space: break-spaces;'
                        ),
                    )
                ) for _id, label, value in rows),
                style='margin: 0 auto;',
            ),
            style=dict(display='none'),
            data_voter_nir_url=reverse('voter_nir'),
            data_contest_id=str(contest.id)
        )

    class HTMLElement:
        def connectedCallback(self):
            this.style.display = 'none'

        def show(self, ballot_id, ballot_sha):
            window.form = form
            form.tracker = this
            document.querySelector('#contest-desc').style.display = 'none'
            document.querySelector('.mdc-list').style.display = 'none'
            location.href = '#'
            this.querySelector('#tracker-ballot-id').innerHTML = ballot_id
            this.querySelector('#tracker-ballot-hash').innerHTML = ballot_sha
            this.querySelector('button').addEventListener(
                'click',
                this.onclick.bind(form)
            )
            this.style.display = 'block'
            this.style.opacity = 1

        async def onclick(self, event):
            if this.sending_ballot:
                return

            this.tracker.hideError()
            voter_nir = ""
            for i in this.tracker.querySelectorAll('code-input input'):
                voter_nir += i.value

            if not len(voter_nir) == 4:
                this.tracker.showError('Please enter your NIR')

            this.csrf = this.csrfmiddlewaretoken.value

            response = await fetch(
                this.tracker.dataset.voterNirUrl,
                {
                    'method': 'POST',
                    'headers': {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRFToken': this.csrf,
                    },
                    'body': JSON.stringify({
                        'nir': voter_nir,
                        'contest_id': this.tracker.dataset.contestId
                    })
                }
            )
            if response.status == 200:
                this.sending_ballot = True
                this.submit()
            else:
                content = await response.json()
                this.tracker.showError(content.message)

        def showError(self, error):
            this.error_container = this.querySelector('.error-container')
            this.error_container.innerHTML = error
            this.error_container.style.display = 'block'

        def hideError(self):
            this.error_container = this.querySelector('.error-container')
            this.error_container.innerHTML = ''
            this.error_container.style.display = 'none'


@template('contest_vote_nir', Document, Card)
class ContestVoteCard(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        if contest.link_to_contest_list:
            self.backlink = BackLink(
                _('back'),
                reverse('contest_list'))
        else:
            self.backlink = BackLink(
                _('back'),
                reverse('contest_detail', args=[contest.id]))

        max_selections = contest.votes_allowed

        candidates = list(contest.candidate_set.all())

        if contest.shuffle_candidates:
            random.shuffle(candidates)
        else:
            candidates.sort(key=lambda c: c.sequence)

        choices = (
            (i, CandidateDetail(candidate), candidate.id)
            for i, candidate
            in enumerate(candidates)
        )

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        manifest_str = to_raw(contest.description)
        internal_manifest_str = to_raw(contest.metadata)
        context_str = to_raw(contest.context)

        voter = contest.voter_set.filter(
            user=view.request.user
        ).first()

        if voter:
            self.seed = voter.get_seed()
        else:
            self.seed = '1'

        tracker = TrackerDetailCSE(contest, view)
        self.tracker_id = tracker.id

        return super().to_html(
            Script('global = window;'),
            Script(mark_safe(
                f'window.currentManifest = {manifest_str};'
                f'window.currentInternalManifest = {internal_manifest_str};'
                f'window.currentContext = {context_str};'
            )),
            Script(src='/static/js/electis.js'),
            H4(
                contest.name,
                Div(
                    Img(
                        src=contest.picture.url,
                        alt='contest-picture',
                        width="100%",
                        id='contest-picture',
                    ),
                    style='padding: 32px',
                ) if contest.picture else None,
                Div(
                    about,
                    cls='center-text body-2',
                    style='word-break: break-word'
                ),
                cls='center-text',
                style='word-break: break-word',
                id='contest-desc'
            ),
            DialogConfirmVoteForm(
                Ul(
                    *[Li(e) for e in form.non_field_errors()],
                    cls='error-list'
                ),
                CSRFInput(view.request),
                Input(
                    type='hidden',
                    name='ballot'
                ),
                MDCMultipleChoicesCheckbox(
                    'selections',
                    choices,
                    n=max_selections),
                MDCButton(_('create ballot'), style=dict(margin='auto')),
                selections=candidates,
                max_selections=max_selections,
                method='POST',
                cls='form vote-form ' + contest.vote_template,
            ),
            tracker,
            cls='card'
        )

    def create_encrypted_ballot(choices):
        chosen = available_selections.filter(
            lambda elem: elem.candidate_id in choices
        )

        selections = [
            new.PlaintextBallotSelection(
                c.object_id,
                c.sequence_order,
                1, 0, None
            ) for c in chosen
        ]
        contests = [
            new.PlaintextBallotContest(
                current_manifest.contests[0].object_id,
                current_manifest.contests[0].sequence_order,
                selections
            )
        ]
        ballot = new.PlaintextBallot(
            crypto.randomUUID(),
            current_manifest.ballot_styles[0].object_id,
            contests
        )

        encrypted_ballot = encrypt_ballot(
            ballot, internal_manifest, context, encryption_seed
        )

        return encrypted_ballot

    def py2js(self):
        window.current_manifest = buildManifest(currentManifest)
        window.internal_manifest = new.InternalManifest(current_manifest)
        window.context = new.CiphertextElectionContext(
            currentContext.number_of_guardians,
            currentContext.quorum,
            new.ElementModP(
                BigInt("0x" + currentContext.elgamal_public_key.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.commitment_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.manifest_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_base_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_extended_base_hash.data)),
            null
        )
        window.available_selections = \
            current_manifest.contests[0].ballot_selections
        window.encryption_seed = new.ElementModQ(BigInt("0x" + self.seed))

        window.create_encrypted_ballot = self.create_encrypted_ballot
        window.tracker = getElementByUuid(self.tracker_id)


@template('contest_vote_nir_list', Document, Card)
class ContestVoteCardNir(Div):
    def to_html(self, *content, view, form, **context):
        contest = view.get_object()
        if tour := contest.tour_set.first():
            tour_num_order = tour.num_order
        else:
            tour_num_order = 1

        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[contest.id]))

        max_selections = contest.votes_allowed

        blank = contest.candidate_set.filter(profile='blank').first()
        lists = contest.candidate_set.filter(profile=None).order_by('sequence').all()
        list_candidates = {}
        for l in lists:
            list_candidates[l.id] = list(contest.candidate_set.filter(profile=l.id).order_by('sequence'))

        all_candidates = contest.candidate_set.order_by('sequence').all()

        lists_checkboxes = MDCMultipleChoicesCheckbox(
            'selections',
            ((i + len(lists), CandidateDetail(candidate), candidate.id)
             for i, candidate
             in enumerate(list(lists) + [blank])),
            n=1
        )
        mdc_multiple_checkboxes = []
        for list_id, candidates in list_candidates.items():
            checkboxes = MDCMultipleChoicesCheckbox(
                'selections',
                ((i, CandidateDetail(candidate), candidate.id)
                for i, candidate
                in enumerate(candidates)),
                n=max_selections,
            )
            checkboxes.id = str(list_id)
            checkboxes.style.display = 'none'
            mdc_multiple_checkboxes.append(checkboxes)

        about = Markdown(escape(contest.about or '')) if contest.about else ''

        manifest_str = to_raw(contest.description)
        internal_manifest_str = to_raw(contest.metadata)
        context_str = to_raw(contest.context)

        voter = contest.voter_set.filter(
            user=view.request.user
        ).first()

        if voter:
            self.seed = voter.get_seed()
        else:
            self.seed = '1'

        tracker = TrackerDetailCSE(contest, view)
        self.tracker_id = tracker.id

        return super().to_html(
            Script('global = window;'),
            Script(mark_safe(
                f'window.tourNumOrder = {tour_num_order};'
                f'window.currentManifest = {manifest_str};'
                f'window.currentInternalManifest = {internal_manifest_str};'
                f'window.currentContext = {context_str};'
            )),
            Script(src='/static/js/electis.js'),
            H4(
                contest.name,
                Div(
                    Img(
                        src=contest.picture.url,
                        alt='contest-picture',
                        width="100%",
                        id='contest-picture',
                    ),
                    style='padding: 32px',
                ) if contest.picture else None,
                Div(
                    about,
                    cls='center-text body-2',
                    style='word-break: break-word'
                ),
                cls='center-text',
                style='word-break: break-word',
                id='contest-desc'
            ),
            DialogConfirmVoteForm(
                Ul(
                    *[Li(e) for e in form.non_field_errors()],
                    cls='error-list'
                ),
                CSRFInput(view.request),
                Input(
                    type='hidden',
                    name='ballot'
                ),
                ScrollingChoice(
                    Div(
                        lists_checkboxes,
                    ),
                    Div(
                        *mdc_multiple_checkboxes
                    ),
                    MDCButton(_('create ballot'), disabled=True, tabindex="-1", style=dict(margin='auto')),
                ),
                selections=all_candidates,
                max_selections=max_selections,
                method='POST',
                cls='form vote-form ' + contest.vote_template,
            ),
            tracker,
            cls='card'
        )

    def create_encrypted_ballot(choices):
        chosen = available_selections.filter(
            lambda elem: elem.candidate_id in choices
        )

        selections = [
            new.PlaintextBallotSelection(
                c.object_id,
                c.sequence_order,
                1, 0, None
            ) for c in chosen
        ]
        contests = [
            new.PlaintextBallotContest(
                current_manifest.contests[0].object_id,
                current_manifest.contests[0].sequence_order,
                selections
            )
        ]
        ballot = new.PlaintextBallot(
            crypto.randomUUID(),
            current_manifest.ballot_styles[0].object_id,
            contests
        )

        encrypted_ballot = encrypt_ballot(
            ballot, internal_manifest, context, encryption_seed
        )

        return encrypted_ballot

    def py2js(self):
        window.current_manifest = buildManifest(currentManifest)
        window.internal_manifest = new.InternalManifest(current_manifest)
        window.context = new.CiphertextElectionContext(
            currentContext.number_of_guardians,
            currentContext.quorum,
            new.ElementModP(
                BigInt("0x" + currentContext.elgamal_public_key.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.commitment_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.manifest_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_base_hash.data)),
            new.ElementModQ(
                BigInt("0x" + currentContext.crypto_extended_base_hash.data)),
            null
        )
        window.available_selections = \
            current_manifest.contests[0].ballot_selections
        window.encryption_seed = new.ElementModQ(BigInt("0x" + self.seed))

        window.create_encrypted_ballot = self.create_encrypted_ballot
        window.tracker = getElementByUuid(self.tracker_id)
