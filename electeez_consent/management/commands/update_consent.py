from django.core.management.base import BaseCommand
from django.conf import settings
from django.urls import reverse

from djelectionguard.models import Contest


class Command(BaseCommand):
    help = 'Update voters of running elections'

    def handle(self, *args, **options):
        contests = Contest.objects.filter(
            actual_end=None,
        ).exclude(
            actual_start=None
        )

        for contest in contests:
            contest.update_voters(auto_only=True)
            if contest.voter_set.exclude(open_email_sent=None).count():
                if settings.LINK_TO_CONTEST_LIST:
                    url = reverse('contest_list')
                else:
                    url = reverse('contest_vote', args=[contest.pk])
                contest.send_mail(
                    contest.open_email_title,
                    contest.open_email,
                    url,
                    'open_email_sent'
                )
