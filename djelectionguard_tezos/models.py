import json
import os

from django.db import models
from djtezos.models import Transaction
from pytezos import pytezos

from electeez_auth.models import User

from .interface import ContractInterface, contract_file
from .storage import election_storage


class ElectionContract(Transaction, ContractInterface):
    election = models.OneToOneField(
        'djelectionguard.contest',
        on_delete=models.CASCADE,
    )

    nft_contract = models.CharField(
        max_length=255,
        null=True,
        blank=True
    )

    nft_token_id = models.PositiveIntegerField(
        null=True,
        blank=True
    )

    is_valid = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.contract_micheline and not self.function:
            contract_path = os.path.join(
                os.path.dirname(__file__),
                f'tezos/{contract_file}',
            )
            with open(contract_path, 'r') as f:
                self.contract_micheline = json.loads(f.read())
            self.contract_name = 'election_compiled'
            self.args = election_storage(
                self.sender.address,
                self.nft_contract,
                self.nft_token_id
            )

        return super().save(*args, **kwargs)

    @property
    def is_internal(self):
        return self.sender.owner == User.objects.get_or_create(email='bank@elictis.io')[0]

    @property
    def explorer_link(self):
        return self.blockchain.explorer.format(self.contract_address)

    @property
    def onchain_contract(self):
        if not self.contract_address:
            return None

        client = pytezos.using(shell=self.blockchain.endpoint)
        contract = client.contract(self.contract_address)
        if contract:
            return contract.to_micheline()

    def compare_contracts(self):
        onchain = self.onchain_contract
        if not onchain:
            return False

        for entry in ['storage', 'parameters', 'code']:
            onchain_entry = self.get_entry(entry, onchain)
            local_entry = self.get_entry(entry, self.contract_micheline)
            if onchain_entry != local_entry:
                return False
        return True

    def get_entry(self, name, micheline):
        for entry in micheline:
            if entry.get('prim', None) == name:
                return entry
