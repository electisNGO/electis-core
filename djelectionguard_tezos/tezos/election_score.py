# SmartPy Code
import smartpy as sp

class Election(sp.Contract):
  def __init__(self, admin):
      self.init(
        admin=admin,
        open='',
        close='',
        manifest_hash='',
        manifest_url='',
        artifacts_hash='',
        artifacts_url='',
        scores=sp.map(tkey=sp.TAddress, tvalue=sp.TNat)
    )

  @sp.entry_point
  def open(self, date, manifest_hash, manifest_url, candidates):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.open=='')
      self.data.open = date
      self.data.manifest_hash = manifest_hash
      self.data.manifest_url = manifest_url
      sp.for candidate in candidates:
        self.data.scores[candidate] = 0

  @sp.entry_point
  def close(self, date):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.close=='')
      self.data.close = date

  @sp.entry_point
  def artifacts(self, artifacts_hash, artifacts_url, scores):
      sp.verify(sp.sender==self.data.admin)
      sp.verify(self.data.artifacts_hash=='')
      self.data.artifacts_hash = artifacts_hash
      self.data.artifacts_url = artifacts_url
      sp.for score in scores.items():
        sp.if self.data.scores.contains(score.key):
            self.data.scores[score.key] = score.value


@sp.add_test(name = "set_scores")
def test():
  scenario = sp.test_scenario()
  scenario.h1("Set scores")
  contract = Election(sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract
  scenario += contract.open(
    date='datestr',
    manifest_hash='efwegwdfqfewe',
    manifest_url='https://manifest.url',
    candidates=[
        sp.address('tz1W1en9UpMCH4ZJL8wQCh8JDKCZARyVx2co'),
        sp.address('tz1UrjyVZueYdd1C79cmGaazgGb7We6YtHav')
    ]).run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract.close('dateclose').run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  scenario += contract.artifacts(
    artifacts_hash='hash',
    artifacts_url='url',
    scores=sp.map({
    sp.address('tz1W1en9UpMCH4ZJL8wQCh8JDKCZARyVx2co'): 32,
    sp.address('tz1UrjyVZueYdd1C79cmGaazgGb7We6YtHav'): 43
  })).run(sender=sp.address('tz1bkeTbgLQULqrfy6BDVNwBqovPkKLUzict'))
  sp.add_compilation_target('election', contract, storage = None)

