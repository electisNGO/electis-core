from .interface import contract_file

if contract_file == 'election_compiled.json':
    from .election_components import *

elif contract_file == 'election_score_compiled.json':
    from .election_score_components import *

elif contract_file == 'election_full_compiled.json':
    from .election_full_components import *

elif contract_file == 'election_nft_compiled.json':
    from .election_nft_components import *
