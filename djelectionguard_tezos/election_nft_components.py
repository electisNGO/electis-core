from ryzom_django_mdc.html import *

from djlang.utils import gettext as _


class TezosFindNFT(Div):
    def __init__(self):
        dialog = MDCDialog(
            _('Choose the token you want'),
            MDCList(id='dialog-content'),
            actions=MDCDialogActions(
                MDCDialogCloseButton(_('Cancel')),
                style=dict(
                    display='flex',
                    justify_content='space-around'
                )
            )
        )
        self.dialog_id = str(dialog.id)

        super().__init__(
            dialog,
            TezosFindNFTButton(_('Browse my NFTs'))
        )

    def ondialogclosing():
        nft_input = document.querySelector('[name=asset]')
        nft_token_id = document.querySelector('[name=asset_id]')
        if this.dialog and this.dialog.token:
            nft_input.value = this.dialog.token
            nft_token_id.value = this.dialog.token_id
            nft_token_id.focus()
            nft_input.focus()

    def py2js(self):
        nft_finder = getElementByUuid(self.id)
        nft_finder.dialog = getElementByUuid(self.dialog_id)
        nft_finder.dialog.onclosing = self.ondialogclosing.bind(nft_finder)


class NFTListItem(MDCListItem):
    class HTMLElement:
        def connectedCallback(self):
            this.addEventListener('click', this.onclick.bind(this))

        def onclick(self):
            if this.classList.contains('mdc-list-item--disabled'):
                return
            dialog = this.closest('mdc-dialog')
            dialog.token = this.dataset.tokenAddress
            dialog.token_id = this.dataset.tokenId
            dialog.close()


class TezosFindNFTButton(MDCButton):
    class HTMLElement:
        def connectedCallback(self):
            if not window.client:
                window.client = new.beacon.DAppClient(
                    {'name': 'Electis.app'}
                )
            this.client = client
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self):
            network = document.querySelector(
                '[name=tznet]'
            ).value.toLowerCase()

            await this.client.requestPermissions({
                'network': {'type': network}
            })
            account = await this.client.getActiveAccount()

            rpc = f'https://api.{network}.tzkt.io/v1/tokens/balances?account={account.address}'
            nfts = await fetch(rpc).then(
                lambda res: res.json()
            )

            this.dialog = document.querySelector('mdc-dialog')
            dialog_content = this.dialog.querySelector('#dialog-content')
            dialog_content.innerHTML = ''
            for nft in nfts:
                metadata = nft.token.metadata
                decimal = parseInt(metadata.decimal)
                balance = parseInt(nft.balance)
                is_fa2 = nft.token.standard == 'fa2' and not decimal
                if is_fa2:
                    item = self.create_list_item(nft.token, balance)
                    dialog_content.appendChild(item)

            this.dialog.open()

        def create_list_item(self, token, balance):
            name = document.createElement('span')
            name.className = 'nft-slot-v nft-name'
            name.appendChild(document.createTextNode(token.metadata.name))

            bal = document.createElement('span')
            bal.className = 'nft-slot-v nft-balance'
            bal.appendChild(document.createTextNode(balance))
            bal.appendChild(document.createTextNode(token.metadata.symbol))

            addr = document.createElement('span')
            addr.className = 'nft-slot-v nft-address'
            addr.appendChild(document.createTextNode(token.contract.address))

            content = document.createElement('span')
            content.className = 'nft-slot-h nft-content'
            content.appendChild(name)
            content.appendChild(bal)
            content.appendChild(addr)

            ripple = document.createElement('span')
            ripple.className = 'mdc-list-item__ripple'

            text = document.createElement('span')
            text.className = 'mdc-list-item__text'
            text.appendChild(content)

            icon = document.createElement('span')
            icon.className = 'mdc-list-item__graphic'
            thumb_uri = token.metadata.thumbnailUri
            if thumb_uri.substr(0, 7) == 'ipfs://':
                thumb_uri = thumb_uri.substr(7)
                image = document.createElement('img')
                image.src = 'https://thumbs.dipdup.net/' + thumb_uri
                icon.appendChild(image)
            else:
                icon.appendChild(document.createTextNode(thumb_uri))

            list_item = document.createElement('nft-list-item')
            list_item.dataset.tokenAddress = token.contract.address
            list_item.dataset.tokenId = parseInt(token.tokenId)
            list_item.className = 'mdc-list-item'

            if balance < 1:
                list_item.classList.add('mdc-list-item--disabled')
            list_item.appendChild(ripple)
            list_item.appendChild(icon)
            list_item.appendChild(text)

            return list_item


class TezosSelfDeploy(MDCButtonRaised):

    class HTMLElement:
        def connectedCallback(self):
            if not window.client:
                window.client = new.beacon.DAppClient(
                    {'name': 'Electis.app'}
                )
            this.client = client
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self):
            network = document.querySelector(
                '[name=tznet]'
            ).value.toLowerCase()
            asset = document.querySelector(
                '[name=asset]'
            ).value
            asset_id = document.querySelector(
                '[name=asset_id]'
            ).value
            await client.requestPermissions({
                'network': {'type': network}
            })
            election_json = await fetch(
                '/tezos/election_contract.json'
            ).then(lambda res: res.json())
            account = await this.client.getActiveAccount()
            origination = await taquito.createOriginationOperation({
                code: election_json,
                storage: {
                    admin: account.address,
                    asset: asset,
                    token_id: asset_id,
                    open: '',
                    close: '',
                    manifest_hash: '',
                    manifest_url: '',
                    artifacts_hash: '',
                    artifacts_url: '',
                    scores: new.taquito.MichelsonMap({
                        'prim': 'map',
                        'args': [
                            {'prim': 'address'},
                            {'prim': 'int'}]
                    }),
                    signatures: new.taquito.MichelsonMap({
                        'prim': 'map',
                        'args': [
                            {'prim': 'address'},
                            {'prim': 'string'}
                        ]
                    }),
                    winners: [],
                }
            })
            operation = await this.client.requestOperation({
                operationDetails: [origination]
            })
            csrf = document.querySelector('[name=csrfmiddlewaretoken]').value
            data = new.FormData()
            data.append(
                'blockchain',
                document.querySelector('[name=tznet]').value
            )
            data.append('asset', asset)
            data.append('asset_id', asset_id)
            data.append('txhash', operation.transactionHash)
            data.append('csrfmiddlewaretoken', csrf)
            contract_created = await fetch(
                'external/',
                {
                    'method': 'POST',
                    'header': {'X-CSRFToken': csrf},
                    'body': data
                }
            )
            response = await contract_created.json()
            if response.status == 'watching':
                document.location.reload()


class TezosSelfOpen(MDCButton):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.wallet.BeaconWallet(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })
            rpc_endpoint = 'https://rpc.tzkt.io/' + this.dataset.tznet
            toolkit = new.taquito.TezosToolkit(rpc_endpoint)
            toolkit.setWalletProvider(this.client)
            contract = await toolkit.wallet.at(this.dataset.contractAddress)
            manifest = await fetch('../manifest/').then(lambda r: r.json())
            address = await this.client.getPKH()

            storage = await contract.storage()
            asset_addr = storage.asset
            asset_id = storage.token_id

            nft_contract = await toolkit.wallet.at(asset_addr)
            nft_storage = await nft_contract.storage()

            operator_opts = {
                owner: address,
                operator: contract.address,
                token_id: asset_id
            }

            is_operator = False
            if nft_storage.operators:
                is_operator = await nft_storage.operators.get(operator_opts)
            else:
                is_operator = await nft_storage.assets.operators.get([
                    address,
                    contract.address,
                    asset_id
                ])

            if not is_operator:
                ep = nft_contract.methodsObject.update_operators([
                    {add_operator: operator_opts}
                ])
                await ep.send()

            ep = contract.methodsObject.open({
                date: new.Date().toISOString(),
                manifest_url: this.dataset.manifestUrl,
                manifest_hash: this.dataset.manifestHash,
                candidates: [c.name.text[0].value for c in manifest.candidates]
            })
            params = ep.toTransferParams()
            limits = await toolkit.estimate.transfer(params)
            print(limits)
            await ep.send(limits)
            this.closest('form').submit()


class TezosSelfClose(MDCButtonOutlined):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })
            operation = await this.client.requestOperation({
                operationDetails: [{
                    kind: beacon.TezosOperationType.TRANSACTION,
                    amount: '0',
                    destination: this.dataset.contractAddress,
                    parameters: {
                        entrypoint: 'close',
                        value: {
                            string: new.Date().toISOString()
                        }
                    }
                }]
            })
            print(operation)
            this.closest('form').submit()


class TezosSelfPublish(MDCButton):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })

            rpc_endpoint = 'https://rpc.tzkt.io/' + this.dataset.tznet
            toolkit = new.taquito.TezosToolkit(rpc_endpoint)
            contract = await toolkit.contract.at(this.dataset.contractAddress)
            scores = await fetch('../scores/').then(lambda r: r.json())

            ep = contract.methodsObject.artifacts({
                artifacts_hash: this.dataset.artifactsHash,
                artifacts_url: this.dataset.artifactsUrl,
                scores: scores,
                num_winners: 1
            })
            transfer = await taquito.createTransferOperation(
                ep.toTransferParams()
            )
            operation = await this.client.requestOperation({
                operationDetails: [transfer]
            })
            print(operation)
            this.closest('form').submit()
