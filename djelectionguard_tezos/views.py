import os
import requests
import json
from django import forms
from django import http
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.views import generic
from django.urls import path, reverse

from djtezos.models import Account, Blockchain

from ryzom.html import template
from ryzom_mdc import *
from ryzom_django_mdc.html import *

from electeez_common.components import Document, Card, BackLink, MDCButton

from djelectionguard.views import Enforce2FAMixin

from djlang.utils import gettext as _

from .models import ElectionContract
from .interface import contract_file
from .components import *


User = get_user_model()


class BlockchainItem(Div):
    def __init__(self, account):
        blockchain = account.blockchain
        try:
            balance = account.balance
        except requests.exceptions.ConnectionError:
            balance = None

        super().__init__(
            H6(_('Smart contract on %(obj)s', obj=blockchain.name)),
            Div(
                Span(_('Wallet address:'), cls='overline'),
                ' ',
                account.address
            ),
            Div(
                Span(
                    Span(_('Balance needed: 2Tez'), cls='overline'),
                ),
                Span(
                    Span(_('Current balance:'), cls='overline'),
                    f' {balance if balance else 0}Tez'
                )
                if balance is not None else None,
                style='display: flex; justify-content: space-between;'
            ),
        )


@template('electioncontract_create', Document, Card)
class ElectionContractCard(Div):
    def to_html(self, *content, view, form, **context):
        self.backlink = BackLink(
            _('back'),
            reverse('contest_detail', args=[view.contest.id])
        )
        network_select_opts = []
        default_opt = 'mainnet'
        for bc in Blockchain.objects.filter(is_active=True):
            network_select_opts.append(
                dict(label=bc.name, value=bc.name, index=bc.id)
            )

        if len(network_select_opts):
            default_opt = network_select_opts[0]['value']

        return super().to_html(
            Script(src='/static/js/walletbeacon.min.js'),
            Script(src='/static/js/taquito.min.js'),
            H5(
                _('Choose the blockchain you want to deploy'
                  ' your election results to'),
                cls='center-text'),
            Div(
                _('As a way to bring trust, this election results and'
                  ' parameters will be shared and stored via a Tezos'
                  ' smart contract and an IPFS Link. To keep it simple'
                  ' you have two choices, one is to chose the '
                  ' free / testnet contract (fine for non official elections)'
                  ' or the main net one (you ll need to pay a small'
                  ' fee / just send the needed Tez fees to the Wallet address)'
                  ' version that can be used for official election as the'
                  ' equivalent of a legal proof of the election parameters'
                  ' and its results.'),
                cls='center-text body-2'),
            Form(
                Div(form.non_field_errors(), cls='red') if form.errors else None,
                MDCMultipleChoicesCheckbox(
                    'blockchain',
                    (
                        (i, BlockchainItem(account), account.blockchain.pk)
                        for i, account
                        in enumerate(context['accounts'])
                    ),
                    n=1),
                H6(
                    _('This choice cannot be changed,'
                      ' please choose carefully'),
                    cls='red center-text',
                    style='margin-bottom: 42px;'),
                Div(
                    MDCButtonOutlined(
                        _('refresh balances'),
                        type='button',
                        onclick='window.location.reload()'
                    ),
                    MDCButton(form.submit_label, type='submit'),
                    style='display: flex; justify-content: space-between'
                ),


                CSRFInput(view.request),
                method='POST',
                cls='form contract-form'
            ) if settings.TEZOS_USE_GENERIC_ACCOUNT else CSRFInput(view.request),
            Div(
                Div(
                    _('Or use your own Tezos account'), ':',
                    style=dict(
                        text_align='center',
                        margin='32px'
                    ),
                ) if settings.TEZOS_USE_GENERIC_ACCOUNT else '',
                Div(
                    Div(
                        MDCField(
                            MDCSelectOutlined(
                                name='tznet',
                                label='Select a network',
                                value=[default_opt],
                                optgroups=[(None, network_select_opts, 0)]
                            ),
                            name='tznet'
                        ),
                        style=dict(display='flex', flex_flow='column nowrap', align_self='stretch')
                    ),
                    Div(
                        MDCTextFieldOutlined(
                            MDCTextInput('asset', required=True),
                            label='NFT',
                            help_text=_('The contract address of the NFT prize')
                        ),
                        MDCTextFieldOutlined(
                            MDCTextInput('asset_id', required=True),
                            label='Token ID',
                            help_text=_('The token id of the NFT prize')
                        ),
                        TezosFindNFT(),
                        style=dict(display='flex', flex_flow='column nowrap', align_self='stretch')
                    ) if settings.ELECTIONCONTRACT_FILE == 'election_nft_compiled.json' else '',
                    Div(
                        TezosSelfDeploy(_('Deploy')),
                        style=dict(display='flex', flex_flow='column nowrap', align_self='stretch')
                    ),
                    style=dict(
                        display='flex',
                        flex_flow='column nowrap',
                    ),
                    cls='self-deploy-container'
                )
            ),
            cls='card')


class ElectionContractCreate(Enforce2FAMixin, generic.FormView):
    template_name = 'electioncontract_create'

    class form_class(forms.Form):
        blockchain = forms.ModelChoiceField(
            queryset=Blockchain.objects.filter(is_active=True),
            required=True
        )
        submit_label = _('Confirm Smart Contract')

        def clean(self):
            super().clean()
            blockchain = self.cleaned_data.get('blockchain')
            if blockchain is None:
                raise forms.ValidationError(_('Blockchain is a required field'))

            account = blockchain.account_set.filter(
                owner=User.objects.get_or_create(email='bank@elictis.io')[0]
            ).first()
            if account.balance <= 2:
                raise forms.ValidationError(_('Insufficient balance'))

    def dispatch(self, request, *args, **kwargs):
        self.contest = request.user.contest_set.filter(pk=kwargs['pk']).first()
        if not self.contest:
            return http.HttpResponseNotFound(_('Election not found'))
        if ElectionContract.objects.filter(election=self.contest).first():
            return http.HttpResponseRedirect(
                reverse('contest_detail', args=[self.contest.id])
            )
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)
        return super(generic.FormView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qs = Blockchain.objects.filter(is_active=True)
        context['accounts'] = []
        for blockchain in qs:
            account, created = Account.objects.get_or_create(
                blockchain=blockchain,
                owner=User.objects.get_or_create(email='bank@elictis.io')[0],
            )
            if created:
                account.generate_private_key()
                account.save()
            context['accounts'].append(account)

        return context

    def form_valid(self, form):
        blockchain = form.cleaned_data['blockchain']
        asset_addr = form.cleaned_data.get('asset', None)
        account, created = Account.objects.get_or_create(
            blockchain=blockchain,
            owner=User.objects.get_or_create(email='bank@elictis.io')[0],
        )
        ElectionContract.objects.create(
            sender=account,
            election=self.contest,
            is_valid=True,
            state='deploy',
        )
        messages.success(
            self.request,
            _('Blockchain contract created! Deployment in progress...'),
        )
        return http.HttpResponseRedirect(
            reverse('contest_detail', args=[self.contest.pk])
        )

    @classmethod
    def as_url(cls):
        return path(
            '<pk>/create/',
            login_required(cls.as_view()),
            name='electioncontract_create'
        )


class ElectionContractExternal(Enforce2FAMixin, generic.FormView):
    template_name = 'electioncontract_create'

    class form_class(forms.Form):
        blockchain = forms.ModelChoiceField(
            queryset=Blockchain.objects.filter(is_active=True),
            to_field_name='name',
        )
        asset = forms.CharField()
        asset_id = forms.IntegerField()
        txhash = forms.CharField()

    def dispatch(self, request, *args, **kwargs):
        self.contest = request.user.contest_set.filter(pk=kwargs['pk']).first()
        if not self.contest:
            return http.HttpResponseBadRequest(_('Election not found'))
        if ElectionContract.objects.filter(election=self.contest).first():
            return http.HttpResponseBadRequest(_('Contract already created'))
        if self.need_verification(request):
            return super().dispatch(request, *args, **kwargs)
        return super(generic.FormView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        return http.HttpResponseNotAllowed(('POST',))

    def form_valid(self, form):
        blockchain = form.cleaned_data['blockchain']
        asset_addr = form.cleaned_data.get('asset', None)
        asset_id = form.cleaned_data.get('asset_id', None)
        account, created = Account.objects.get_or_create(
            blockchain=blockchain,
            owner=self.request.user
        )
        ElectionContract.objects.create(
            sender=account,
            election=self.contest,
            nft_contract=asset_addr,
            nft_token_id=asset_id,
            txhash=form.cleaned_data['txhash'],
            state='watch',
        )
        messages.success(
            self.request,
            _('Blockchain contract created! Deployment in progress...'),
        )
        return http.JsonResponse(
            {'status': 'watching'}
        )

    def form_invalid(self, form):
        return http.JsonResponse(
            {
                'status': 'error',
                'data': form.errors
            }
        )

    @classmethod
    def as_url(cls):
        return path(
            '<pk>/create/external/',
            login_required(cls.as_view()),
            name='electioncontract_create_external'
        )


def election_json(request):
    path = os.path.dirname(__file__)
    with open(f'{path}/tezos/{contract_file}', 'r') as f:
        election_str = f.read()
        f.close()

    election = json.loads(election_str)
    return http.JsonResponse(election, safe=False)


urlpatterns = [
    ElectionContractCreate.as_url(),
    ElectionContractExternal.as_url(),
    path('election_contract.json', election_json, name='election_json')
]
