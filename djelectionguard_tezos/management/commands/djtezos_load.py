from django.db import (
    DEFAULT_DB_ALIAS, connections, transaction
)
from django.apps import apps
from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Load keys if given'

    def handle(self, *args, **options):

        self.using = DEFAULT_DB_ALIAS

        with transaction.atomic(using=self.using):
            Account = apps.get_model('djtezos', 'Account')
            Blockchain = apps.get_model('djtezos', 'Blockchain')
            User = apps.get_model('electeez_auth', 'User')
            from djtezos.models import encrypt
            for net, addr, key in settings.DJBLOCKCHAIN.get('ACCOUNTS', []):
                account = Account.objects.filter(blockchain__name=net).first()
                blockchain = Blockchain.objects.filter(name=net).first()
                bank = User.objects.filter(email='bank@elictis.io').first()
                if account or not blockchain or not bank:
                    continue

                Account.objects.create(
                    owner=bank,
                    blockchain=blockchain,
                    address=addr,
                    crypted_key=encrypt(bytes.fromhex(key))
                )

        if transaction.get_autocommit(self.using):
            connections[self.using].close()
