from ryzom_django_mdc.html import *

class TezosSelfDeploy(MDCButton):

    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self):
            network = document.querySelector(
                '[name=tznet]'
            ).value.toLowerCase()
            await this.client.requestPermissions({
                'network': {'type': network}
            })
            election_json = await fetch(
                '/tezos/election_contract.json'
            ).then(lambda res: res.json())
            account = await this.client.getActiveAccount()
            origination = await taquito.createOriginationOperation({
                code: election_json,
                storage: {
                    admin: account.address,
                    open: '',
                    close: '',
                    manifest_hash: '',
                    manifest_url: '',
                    artifacts_hash: '',
                    artifacts_url: '',
                }
            })
            operation = await this.client.requestOperation({
                operationDetails: [origination]
            })
            console.log(operation)
            csrf = document.querySelector('[name=csrfmiddlewaretoken]').value
            data = new.FormData()
            data.append(
                'blockchain',
                document.querySelector('[name=tznet]').value
            )
            data.append('txhash', operation.transactionHash)
            data.append('csrfmiddlewaretoken', csrf)
            contract_created = await fetch(
                'external/',
                {
                    'method': 'POST',
                    'header': {'X-CSRFToken': csrf},
                    'body': data
                }
            )
            response = await contract_created.json()
            if response.status == 'watching':
                document.location.reload()


class TezosSelfOpen(MDCButton):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })
            rpc_endpoint = 'https://rpc.tzkt.io/' + this.dataset.tznet
            toolkit = new.taquito.TezosToolkit(rpc_endpoint)
            contract = await toolkit.contract.at(this.dataset.contractAddress)

            ep = contract.methodsObject.open({
                date: new.Date().toISOString(),
                manifest_url: this.dataset.manifestUrl,
                manifest_hash: this.dataset.manifestHash,
            })
            transfer = await taquito.createTransferOperation(
                ep.toTransferParams()
            )
            operation = await this.client.requestOperation({
                operationDetails: [transfer]
            })
            print(operation)
            this.closest('form').submit()


class TezosSelfClose(MDCButtonOutlined):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })
            operation = await this.client.requestOperation({
                operationDetails: [{
                    kind: beacon.TezosOperationType.TRANSACTION,
                    amount: '0',
                    destination: this.dataset.contractAddress,
                    parameters: {
                        entrypoint: 'close',
                        value: {
                            string: new.Date().toISOString()
                        }
                    }
                }]
            })
            print(operation)
            this.closest('form').submit()


class TezosSelfPublish(MDCButton):
    class HTMLElement:
        def connectedCallback(self):
            this.client = new.beacon.DAppClient(
                {'name': 'Electis.app'}
            )
            this.addEventListener('click', this.onclick.bind(this))

        async def onclick(self, event):
            await this.client.requestPermissions({
                'network': {'type': this.dataset.tznet}
            })

            operation = await this.client.requestOperation({
                operationDetails: [{
                    kind: beacon.TezosOperationType.TRANSACTION,
                    amount: '0',
                    destination: this.dataset.contractAddress,
                    parameters: {
                        entrypoint: 'artifacts',
                        value: {
                            prim: 'Pair',
                            args: [
                                {string: this.dataset.artifactsHash},
                                {string: this.dataset.artifactsUrl}
                            ]
                        }
                    }
                }]
            })
            print(operation)
            location.href = this.dataset.contestUrl
