from .interface import contract_file


if contract_file == 'election_compiled.json':
    def election_storage(admin, *args, **kwargs):
        return {
            "prim": "Pair",
            "args": [
                {
                    "prim": "Pair",
                    "args": [
                        dict(string=admin),
                        {
                            "prim": "Pair",
                            "args": [dict(string=""), dict(string="")],
                        }
                    ]
                },
                {
                    "prim": "Pair",
                    "args": [
                        {
                            "prim": "Pair",
                            "args": [dict(string=""), dict(string="")],
                        },
                        {
                            "prim": "Pair",
                            "args": [dict(string=""), dict(string="")],
                        }
                    ]
                }
            ]
        }

elif contract_file == 'election_score_compiled.json':
    def election_storage(admin, *args, **kwargs):
        return {
            "prim": "Pair",
            "args": [
                {
                    "prim": "Pair",
                    "args": [
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": admin},
                                {"string": ""}
                            ]
                        },
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": ""},
                                {"string": ""}
                            ]
                        }
                    ]
                },
                {
                    "prim": "Pair",
                    "args": [
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": ""},
                                {"string": ""}
                            ]
                        },
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": ""},
                                []
                            ]
                        }
                    ]
                }
            ]
        }

elif contract_file == 'election_full_compiled.json':
    def election_storage(admin, *args, **kwargs):
        return {
            "prim": "Pair",
            "args": [
                {
                    "prim": "Pair",
                    "args": [
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": admin},
                                {"string": ""}
                            ]
                        },
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": ""},
                                {
                                    "prim": "Pair",
                                    "args": [
                                        {"string": ""},
                                        {"string": ""}
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "prim": "Pair",
                    "args": [
                        {
                            "prim": "Pair",
                            "args": [
                                {"string": ""},
                                {"string": ""}
                            ]
                        },
                        {
                            "prim": "Pair",
                            "args": [
                                [],
                                {
                                    "prim": "Pair",
                                    "args": [
                                        [],
                                        []
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }

elif contract_file == 'election_nft_compiled.json':
    def election_storage(admin, asset, token_id):
        return {
                "prim": "Pair",
                "args": [
                    {
                        "prim": "Pair",
                        "args": [
                            { "prim": "Pair", "args": [ { "string": admin }, { "prim": "Pair", "args": [ { "string": "" }, { "string": "" } ] } ] },
                            { "prim": "Pair", "args": [ { "string": asset }, { "prim": "Pair", "args": [ { "string": "" }, { "string": "" } ] } ] }
                            ]
                        },
                    {
                        "prim": "Pair",
                        "args": [
                            { "prim": "Pair", "args": [ { "string": "" }, { "prim": "Pair", "args": [ { "string": "" }, [] ] } ] },
                            { "prim": "Pair", "args": [ [], { "prim": "Pair", "args": [ { "int": token_id }, [] ] } ] }
                            ]
                        }
                    ]
                }
