""" Data migration to sanitize existing user names. """
import re
from django.db import migrations

from electeez_auth.models import User


def sanitize_user_names(apps, schema_editor):
    """ Sanitize existing user names by removing unauthorized characters.
    see views.UpdateProfileView for the list of authorized characters. ('[<>\\\/:"|?*]')
    """
    users = User.objects.all()

    def sub(name):
        return re.sub(r'[@.<>\\\/:;"|?{}#()^*\[\]]', '', name)

    for user in users:
        user.first_name = user.first_name and sub(user.first_name) or ''
        user.last_name = user.last_name and sub(user.last_name) or ''
        user.save()


class Migration(migrations.Migration):
    dependencies = [
        ('electeez_auth', '0019_alter_beacon_id_alter_idtoken_id_alter_siteaccess_id_and_more'),
    ]

    operations = [
        migrations.RunPython(sanitize_user_names),
    ]
