# Generated by Django 3.2.12 on 2022-03-16 07:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('electeez_auth', '0005_alter_beacon_user'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={},
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=255, verbose_name='email address'),
        ),
        migrations.AlterUniqueTogether(
            name='user',
            unique_together={('email', 'wallet')},
        ),
        migrations.AddConstraint(
            model_name='user',
            constraint=models.CheckConstraint(check=models.Q(models.Q(('email__isnull', False), ('wallet__isnull', True)), models.Q(('email__isnull', True), ('wallet__isnull', False)), _connector='OR'), name='electeez_auth_user_wallet_xor_email'),
        ),
    ]
