# Generated by Django 3.2.12 on 2022-03-31 02:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('electeez_auth', '0008_auto_20220316_0758'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='can_send_sms',
            field=models.BooleanField(default=False),
        ),
    ]
