from django import forms
from django.conf import settings
from django.urls import reverse
from django_registration.forms import RegistrationForm
from django_registration.backends.activation.views import RegistrationView

from captcha.fields import CaptchaTextInput

from .models import User
from ryzom_django_mdc.html import *
from electeez_common.components import Document, Card, BackLink, MDCButton, DialogConfirmForm

from djelectionguard.components import CircleIcon, TezosIcon
from two_factor.plugins.phonenumber.templatetags.phonenumber import device_action

from djlang.utils import gettext as _

from djelectionguard.models import Contest, Voter, Guardian


class GoogleIcon(CircleIcon):
    def __init__(self):
        super().__init__('google-icon', 'white', small=True)


class FacebookIcon(CircleIcon):
    def __init__(self):
        super().__init__('facebook-icon', 'fb-blue', small=True)


class AppleIcon(CircleIcon):
    def __init__(self):
        super().__init__('apple-icon', 'black', small=True)


class OAuthConnect(Div):
    def __init__(self):
        self.google_btn = MDCButtonOutlined(
            _('continue with %(brand)s', brand='Google'),
            icon=GoogleIcon(),
            tag='a',
            href=reverse('social:begin', args=['google-oauth2'])
        ) if settings.SOCIAL_AUTH_GOOGLE_ENABLED else None

        self.facebook_btn = MDCButton(
            _('continue with %(brand)s', brand='Facebook'),
            p=False,
            icon=FacebookIcon(),
            tag='a',
            href=reverse('social:begin', args=['facebook'])
        ) if settings.SOCIAL_AUTH_FACEBOOK_ENABLED else None

        self.apple_btn = MDCButton(
            _('continue with %(brand)s', brand='Apple'),
            icon=AppleIcon(),
            tag='a',
            href=reverse('social:begin', args=['apple-id'])
        )

        super().__init__(
            self.google_btn,
            self.facebook_btn,
            # self.apple_btn,
            cls='oauth',
            style='display: flex; flex-flow: column wrap;'
        )


class TzConnectBtn(MDCButtonOutlined):
    tag = 'tz-connect'

    def __init__(self):
        super().__init__(
            _('continue with %(brand)s', brand='Tezos'),
            icon=TezosIcon(small=True),
            data_dapp_name=_('Electis.app'),
            data_wallet_url=reverse('request_beacon'),
            data_login_url=reverse('beacon_login'),
        )

    class HTMLElement:
        def connectedCallback(self):
            this.wallet_url = self.dataset.walletUrl
            this.login_url = self.dataset.loginUrl
            this.client = new.beacon.DAppClient(
                {'name': self.dataset.dappName}
            )
            this.addEventListener('click', this.connect.bind(this))

        async def request_beacon(self):
            this.csrf = document.querySelector(
                '[name="csrfmiddlewaretoken"]'
            ).value
            this.wallet = await this.client.getActiveAccount()
            await fetch(this.wallet_url, {
                'method': 'POST',
                'headers': {'X-CSRFToken': this.csrf},
                'body': JSON.stringify({'wallet': this.wallet.address})
            }).then(
                lambda response: response.json()
            ).then(
                lambda result: this.sign_payload(result.payload)
            )

        async def sign_payload(self, payload):
            await this.client.requestSignPayload({
                'signingType': beacon.SigningType.MICHELINE,
                'payload': payload
            }).then(
                lambda signed: this.login(signed)
            )

        async def login(self, signed):
            await fetch(this.login_url, {
                'method': 'POST',
                'headers': {'X-CSRFToken': this.csrf},
                'body': JSON.stringify({
                    'wallet': this.wallet.address,
                    'signature': signed.signature,
                    'publicKey': this.permissions.publicKey
                })
            }).then(
                lambda response: response.json()
            ).then(
                lambda result: this.redirect_or_error(result)
            )

        async def request_permission(self):
            this.permissions = await this.client.requestPermissions()
            this.request_beacon()

        async def connect(self):
            this.request_permission()

        def redirect_or_error(self, log_response):
            print(log_response)
            if log_response.status == 'logged':
                location.href = '/'


class TzConnect(Div):
    def __init__(self):
        super().__init__(
            TzConnectBtn(),
            cls='tz',
            style='display: flex; flex-flow: column wrap;'
        )


class RegistrationForm(RegistrationForm):
    password2 = forms.CharField(
        label=_('Repeat password'),
        required=True,
        widget=forms.PasswordInput,
    )

    class Meta(RegistrationForm.Meta):
        model = User


@template('django_registration/registration_form.html', Document, Card)
class RegistrationFormViewComponent(Html):
    def to_html(self, *content, view, form, **context):
        return super().to_html(
            *content,
            H4(_('Looks like you need to register'), cls='center-text'),
            Form(
                CSRFInput(view.request),
                form,
                MDCButton(_('Register')),
                method='POST',
                cls='form card'),
        )


RegistrationView.form_class = RegistrationForm


@template('django_registration/registration_closed.html', Document, Card)
class RegistrationClosedViewComponent(Html):
    def to_html(self, *content, view, **context):
        return super().to_html(
            *content,
            H4(_('Registration closed'), cls='center-text'),
        )


@template('registration/login.html', Document, Card)
class LoginFormViewComponent(Div):
    def to_html(self, *content, view, form, **kwargs):
        if view.request.user.is_authenticated:
            self.backlink = BackLink(
                _('my elections'),
                reverse('contest_list')
            )

        tz_connect = settings.TEZOS_AUTH_ENABLED and TzConnect() or None
        oauth_connect = settings.SOCIAL_AUTH_ENABLED and OAuthConnect() or None
        idtoken_connect = (
            settings.IDTOKEN_AUTH_ENABLED
            or settings.IDTOKEN_ANON_AUTH_ENABLED
            and A(_('Login with an identification token'), href='/accounts/token/')
            or None
        )

        return super().to_html(
            Script(src='/static/js/walletbeacon.min.js'),
            Form(
                H4(_('Welcome to Electis.app'), style='text-align: center;'),
                tz_connect,
                oauth_connect,
                idtoken_connect,
                (
                    (tz_connect or oauth_connect)
                    and Span(
                        _('Or enter email and password:'),
                        cls='center-text'
                    )
                    or Span(
                        _('Enter email and password:'),
                        cls='center-text'
                    )
                ),
                CSRFInput(view.request),
                Div(
                    form,
                    cls='checkbox-field'
                ),
                Div(
                    MDCTextButton(
                        _('forgot password'),
                        tag='a',
                        href=reverse('password_reset')),
                    MDCButton(_('Continue')),
                    style='display: flex; justify-content: space-between'),
                method='POST',
                cls='form card'),
            cls='')


@template('idtoken_login.html', Document, Card)
class IDTokenLoginFormViewComponent(Div):
    def to_html(self, *content, view, form, **kwargs):
        if view.request.user.is_authenticated:
            self.backlink = BackLink(
                _('my elections'),
                reverse('contest_list')
            )

        return super().to_html(
            Form(
                H4(_('Welcome to Electis.app'), style='text-align: center;'),
                Span(
                    _('Enter ID token:'),
                    cls='center-text'
                ),
                CSRFInput(view.request),
                Div(
                    form,
                    cls='checkbox-field'
                ),
                Div(
                    MDCButton(_('Continue')),
                    style='display: flex; justify-content: space-between'),
                method='POST',
                cls='form card'),
            cls='')


@template('defender_template', Document, Card)
class DefenderLoginFailed(Div):
    def to_html(self, *content, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:login')
        )
        return super().to_html(
            H4(_('Welcome to Electis.app'), style='text-align: center;'),
            H6(_(
                'You have used all your login attempts. Please try again in 5min'
            ), style='text-align: center'),
            cls='card'
        )


@template('two_factor/core/login.html', Document, Card)
class TwoFactorLoginComponent(Div):
    def to_html(self, *content, view, form, **kwargs):
        wizard = kwargs['wizard']

        if isinstance(wizard['steps'], dict):
            current = wizard['steps']['current']
        else:
            current = wizard['steps'].current

        if current == 'auth':
            if view.request.user.is_authenticated:
                self.backlink = BackLink(
                    _('my elections'),
                    reverse('contest_list')
                )

            tz_connect = settings.TEZOS_AUTH_ENABLED and TzConnect() or None
            oauth_connect = settings.SOCIAL_AUTH_ENABLED and OAuthConnect() or None
            idtoken_connect = (
                settings.IDTOKEN_AUTH_ENABLED
                or settings.IDTOKEN_ANON_AUTH_ENABLED
                and MDCButtonOutlined(
                        _('Login with an identification token'),
                        tag='a',
                        href='/accounts/token/'
                    )
                or None
            )

            return super().to_html(
                Script(src='/static/js/walletbeacon.min.js'),
                Form(
                    wizard['management_form'],
                    H4(_('Welcome to Electis.app'), style='text-align: center;'),
                    tz_connect,
                    oauth_connect,
                    (
                        (tz_connect or oauth_connect)
                        and Span(
                            _('Or enter email and password:'),
                            cls='center-text'
                        )
                        or Span(
                            _('Enter email and password:'),
                            cls='center-text'
                        )
                    ),
                    CSRFInput(view.request),
                    wizard['form'],
                    Div(
                        MDCTextButton(
                            _('forgot password'),
                            tag='a',
                            href=reverse('password_reset')),
                        MDCButton(_('Continue')),
                        style='display: flex; justify-content: space-between'
                    ),
                    Div(
                        Span('Or', style='margin: 24px'),
                        idtoken_connect,
                        style=dict(
                            display='flex',
                            flex_flow='column nowrap',
                            align_items='center',
                        )
                    ) if idtoken_connect else None,
                    method='POST',
                    cls='form card ' + wizard['form'].__class__.__name__),
                cls='')

        elif current == 'token':
            self.backlink = BackLink(
                _('Back'), reverse('two_factor:login')
            )
            device = kwargs['device']

            action = reverse('otp_2fa_login')
            if redirect := view.request.GET.get('redirect'):
                action += '?next=' + redirect

            if hasattr(device, 'method') and device.method in ['sms', 'call']:
                return super().to_html(
                    Form(
                        H4(_('Welcome to Electis.app'), style='text-align: center;'),
                        CSRFInput(view.request),
                        H6(_('Please type the code received'), style='text-align: center'),
                        wizard['management_form'] if 'management_form' in wizard else None,
                        wizard['form'],
                        Div(
                            MDCButton(_('Continue')),
                            style='display: flex; justify-content: space-between'),
                        P(_('As a last resort, you can use a backup token:')),
                        MDCButtonOutlined(
                            _('Use backup token'),
                            type='submit',
                            value='backup',
                            name='wizard_goto_step',
                            style=dict(
                                width='100%'
                            )
                        ),
                        method='POST',
                        cls='form card ' + wizard['form'].__class__.__name__),
                    cls='')

            else:
                return super().to_html(
                    Form(
                        H4(_('Welcome to Electis.app'), style='text-align: center;'),
                        CSRFInput(view.request),
                        H6(_('Please type the code from your generator'), style='text-align: center'),
                        wizard['management_form'] if 'management_form' in wizard else None,
                        wizard['form'],
                        Div(
                            MDCButton(_('Continue')),
                            style='display: flex; justify-content: space-between'),
                        Div(
                            P(_('Or, alternatively, use one of your backup phones:')),
                            *[
                                MDCButtonOutlined(
                                    device_action(phone),
                                    name='challenge_device',
                                    value=phone.persistent_id,
                                    type='submit',
                                    style=dict(width='100%')
                                )
                                for phone in kwargs['other_devices']
                            ]
                        ) if kwargs.get('other_devices') else None,
                        P(_('As a last resort, you can use a backup token:')),
                        MDCButtonOutlined(
                            _('Use backup token'),
                            type='submit',
                            value='backup',
                            name='wizard_goto_step',
                            style=dict(
                                width='100%'
                            )
                        ),
                        method='POST',
                        action=action,
                        cls='form card ' + wizard['form'].__class__.__name__),
                    cls='')

        else:
            self.backlink = BackLink(
                _('Back'), reverse('two_factor:login')
            )
            return super().to_html(
                Form(
                    H4(_('Welcome to Electis.app'), style='text-align: center;'),
                    CSRFInput(view.request),
                    H6(_(
                        'Use this form for entering backup tokens for logging in.'
                        ' These tokens have been generated for you to print and'
                        ' keep safe. Please enter one of these backup tokens to'
                        ' login to your account.'
                    ), style='text-align: center'),
                    wizard['management_form'] if 'management_form' in wizard else None,
                    wizard['form'],
                    Div(
                        MDCButton(_('Continue')),
                        style='display: flex; justify-content: space-between'),
                    method='POST',
                    cls='form card'),
                cls='')


@template('two_factor/profile/profile.html', Document, Card)
class TwoFactorProfile(Div):
    def to_html(self, *content, **kwargs):
        default_msg = ''
        secondary_methods = ''
        backup_tokens = ''
        setup = ''

        self.backlink = BackLink(
            _('Back'), reverse('profile')
        )

        if default_device := kwargs.get('default_device', None):
            default_device_type = kwargs['default_device_type']
            if default_device_type == 'TOTPDevice':
                default_msg = P(_('Tokens will be generated by your token generator.'))
            elif default_device_type == 'PhoneDevice':
                primary = device_action(default_device)
                default_msg = P(_('Primary method: %(msg)s', msg=primary))

            if kwargs.get('available_phone_methods', None):
                secondary_methods = Div(
                    H5(_('Backup phone numbers')),
                    P(_(
                        'If your primary method is not available, we are able to'
                        ' send backup tokens to the phone number listed below.'
                    )),
                    Ul(
                        *[
                            Li(
                                Form(
                                    device_action(phone),
                                    MDCButtonOutlined(_('Unregister'), type='submit'),
                                    method='POST',
                                    action=reverse('two_factor:phone_delete', args=[phone.id]),
                                    onsubmit=f'return confirm("{_("Are you sure?")}")',
                                    style=dict(
                                        display='flex',
                                        flex_flow='row wrap',
                                        align_items='baseline',
                                        justify_content='space-between'
                                    )
                                )
                            ) for phone in kwargs.get('backup_phones', [])
                        ]
                    ),
                    MDCButtonOutlined(
                        _('Add phone number'),
                        icon='add',
                        tag='a',
                        href=reverse('two_factor:phone_create'),
                    )
                )

            backup_tokens = Div(
                H5(_('Backup tokens')),
                P(_(
                    'If you don\'t have any device with you, you can access'
                    ' your account using backup tokens'
                )),
                P(_(
                    'You have %(count)s token remaing',
                    n=kwargs.get('backup_tokens', 0),
                    count=kwargs.get('backup_tokens', 0)
                )),
                MDCButtonOutlined(
                    _('Show codes'),
                    tag='a',
                    href=reverse('two_factor:backup_tokens')
                )
            )

            setup = Div(
                H5(_('Disable Two-Factor authentication')),
                P(_(
                    'However we strongly discourage you to do so,'
                    ' you can disable two-factor authentication for'
                    ' your account.'
                )),
                MDCButtonOutlined(
                    _('Disable Two-Factor authentication'),
                    tag='a',
                    href=reverse('two_factor:disable'),
                    style='color: var(--danger)'
                )
            )

        else:
            setup = Div(
                H5(_('Enable Two-Factor authentication')),
                P(_(
                    'Two-Factor authentication is not enabled for'
                    ' your account. Enable Two-Factor authentication'
                    ' for enhanced account security'
                )),
                MDCButtonOutlined(
                    _('Enable Two-Factor authentication'),
                    tag='a',
                    href=reverse('two_factor:setup')
                )
            )

        return super().to_html(
            H4(_('Account security'), style='text-align: center'),
            default_msg,
            secondary_methods,
            backup_tokens,
            setup,
            cls=''
        )


@template('two_factor/profile/disable.html', Document, Card)
class TwoFactorDisable(Div):
    def to_html(self, *content, view, form, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:profile')
        )

        return super().to_html(
            H4(_('Disable Two-factor authentication'), style='text-align: center'),
            P(_(
                'You are about to disable two-factor authentication.'
                ' This weakens your account security, are you sure?'
            )),
            Form(
                CSRFInput(view.request),
                Div(
                    form,
                    cls='checkbox-field'
                ),
                MDCButtonOutlined(
                    _('Disable'),
                    type='submit',
                    style=dict(color='var(--danger)'),
                ),
                method='POST',
                cls='form'
            )
        )


@template('two_factor/core/setup.html', Document, Card)
class TwoFactorSetup(Div):
    def to_html(self, *content, view, wizard, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:profile')
        )

        form = Form(
            CSRFInput(view.request),
            wizard['form'],
            wizard['management_form'],
            TwoFactorWizardAction(wizard),
            method='POST',
            cls='form'
        )
        print(wizard['steps'].current)

        if wizard['steps'].current == 'welcome':
            inner_step = Div(
                P(_(
                    'You are about to take your account security'
                    ' to the next level. Follow the steps in this'
                    ' wizard to enable two-factor authentication.'
                ))
            )
        elif wizard['steps'].current == 'method':
            inner_step = Div(
                P(_(
                    'Please select which authentication method you'
                    ' would like to use'
                ))
            )
            form = Form(
                CSRFInput(view.request),
                Div(
                    RadioSelectOption(
                        name='method-method',
                        count=0,
                        label=_('Token generator'),
                        value='generator',
                        required=True
                    ),
                    RadioSelectOption(
                        name='method-method',
                        count=1,
                        label=_('WebAuth'),
                        value='webauthn',
                        required=True
                    ),
                    RadioSelectOption(
                        name='method-method',
                        count=2,
                        label=_('Phone call'),
                        value='call',
                        required=True
                    ),
                    RadioSelectOption(
                        name='method-method',
                        count=3,
                        label=_('Text message'),
                        value='sms',
                        required=True
                    ),
                    style=dict(margin='24px 0')
                ),
                wizard['management_form'],
                TwoFactorWizardAction(wizard),
                method='POST',
                cls='form'
            )

        elif wizard['steps'].current == 'generator':
            inner_step = Div(
                P(_(
                    'To start using a token generator, please use'
                    ' your smartphone to scan the QR code below.'
                    ' For example, use Google Authenticator. Then,'
                    ' enter the token generated by the app.'
                )),
                P(Img(src=kwargs.get('QR_URL', ''), alt='QR code'))
            )
        elif wizard['steps'].current == 'webauthn':
            inner_step = Div(
                P(_(
                    'webauth'
                ))
            )
        elif wizard['steps'].current == 'sms':
            inner_step = Div(
                P(_(
                    'Please enter the phone number your wish to'
                    ' receive text messages on. This will be validated'
                    ' in the next step.'
                ))
            )
        elif wizard['steps'].current == 'call':
            inner_step = Div(
                P(_(
                    'Please enter the phone number your wish to'
                    ' be called on. This will be validated'
                    ' in the next step.'
                ))
            )
        elif wizard['steps'].current == 'validation':
            if kwargs.get('challenge_succeeded', False):
                device = kwargs.get('device', None)
                if device and getattr(device, 'method', None):
                    if device.method == 'call':
                        inner_step = Div(
                            P(_(
                                'We are calling your phone right now,'
                                ' please enter the digits you hear.'
                            ))
                        )
                    elif device.method == 'sms':
                        inner_step = Div(
                            P(_(
                                'We sent you a text message, please'
                                ' enter the tokens we sent.'
                            ))
                        )
            else:
                inner_step = Div(
                    P(_(
                        'We\'ve encountered an issue with the selected'
                        ' authentication method. Please go back and'
                        ' verify that you entered your information'
                        ' correctly, try again, or use a different'
                        ' authentication method instead. If the issue'
                        ' persist, contact the administrator.'
                    ))
                )

        return super().to_html(
            H4(_('Enable Two-factor authentication'), style='text-align: center'),
            inner_step,
            form
        )


@template('two_factor/core/setup_complete.html', Document, Card)
class TwoFactorSetupComplete(Div):
    def to_html(self, *content, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:profile')
        )

        phone_methods = kwargs.get('phone_methods', None)
        if not phone_methods:
            inner = Div(
                P(
                    MDCButtonOutlined(
                        _('Back to account security'),
                        tag='a',
                        href=reverse('two_factor:profile'),
                    )
                )
            )
        else:
            inner = Div(
                P(_(
                    'However, it might happen that you don\'t have access to'
                    ' your primary token device. To enabled account recovery,'
                    ' add a phone number.'
                )),
                Div(
                    MDCButtonOutlined(
                        _('Back to account security'),
                        tag='a',
                        href=reverse('two_factor:profile')
                    ),
                    MDCButtonOutlined(
                        _('Add a phone number'),
                        tag='a',
                        href=reverse('two_factor:phone_create')
                    ),
                    style=dict(
                        display='flex',
                        flex_flow='row nowrap',
                        justify_content='space-between'
                    )
                )
            )

        return super().to_html(
            H4(_('Enable Two-factor authentication'), style='text-align: center'),
            P(_('Congratulation, you\'ve successfully enabled two-factor authentication')),
            inner
        )


@widget_template('django/forms/widgets/radio_option.html')
class RadioSelectOption(Div):
    def __init__(self, name, count, label, value, **attrs):
        super().__init__(
            Div(
                Input(
                    type='radio',
                    name=name,
                    id=f"{name}_id_{count}",
                    cls='mdc-radio__native-control',
                    value=value,
                    **attrs
                ),
                Div(
                    Div(cls='mdc-radio__outer-circle'),
                    Div(cls='mdc-radio__inner-circle'),
                    cls='mdc-radio__background'
                ),
                Div(cls='mdc-radio__ripple'),
                cls='mdc-radio',
                style=dict(width='20px'),
            ),
            Label(label, **{'for': f"{name}_id_{count}"}),
            cls='mdc-form-field',
            style=dict(
                flex_flow='row wrap',
            )
        )


@template('two_factor/core/phone_register.html', Document, Card)
class TwoFactorRegisterPhone(Div):
    def to_html(self, *content, form, view, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:profile')
        )

        wizard = kwargs['wizard']

        subtitle = None
        if wizard['steps'].current == 'setup':
            subtitle = P(_(
                'You\'ll be adding a backup phone number to your account.'
                ' This number will be used if your primary method of'
                ' registration is not available'
            ))
            form = Form(
                CSRFInput(view.request),
                wizard['form']['number'],
                B(_('How to receive your codes:')),
                Div(
                    RadioSelectOption(
                        name='setup-method',
                        count=0,
                        label=_('Call'),
                        value='call',
                        required=True
                    ),
                    RadioSelectOption(
                        name='setup-method',
                        count=1,
                        label=_('Text message'),
                        value='sms',
                        required=True
                    ),
                    style=dict(margin='24px 0')
                ),
                wizard['management_form'],
                TwoFactorWizardAction(**kwargs),
                method='POST',
                cls='form'
            )

        elif wizard['steps'].current == 'validation':
            subtitle = P(_(
                'We\'ve sent a token to your phone number. Please enter'
                ' the token you\'ve received.'
            ))
            form = Form(
                CSRFInput(view.request),
                wizard['form'],
                wizard['management_form'],
                TwoFactorWizardAction(**kwargs),
                method='POST',
                cls='form'
            )

        return super().to_html(
            H4(_('Add backup phone'), style='text-align: center'),
            subtitle,
            form
        )


@template('two_factor/core/backup_tokens.html', Document, Card)
class TwoFactorBackupTokens(Div):
    def to_html(self, *content, form, view, **kwargs):
        self.backlink = BackLink(
            _('Back'), reverse('two_factor:profile')
        )

        device = kwargs['device']

        if device.token_set.count():
            device_content = Div(
                Ul(
                    *[Li(token.token)
                        for token in device.token_set.all()]
                ),
                P(_(
                    'Print these tokens and keep them somewhere safe.'
                ))
            )
        else:
            device_content = Div(
                P(_('You don\'t have any backup codes yet.'))
            )

        return super().to_html(
            H4(_('Backup tokens'), style='text-align: center'),
            P(_(
                'Backup tokens can be used when your primary and backup'
                ' phone numbers aren\'t available. The backup tokens below'
                ' can be used for login verification. If you\'ve used all'
                ' your backup tokens, you can generate a new set of backup'
                ' tokens. Only the backup tokens shown below will be valid.'
            )),
            device_content,
            Form(
                CSRFInput(view.request),
                form,
                MDCButtonOutlined(
                    _('Generate tokens'),
                    type='submit'
                ),
                method='POST',

            )
        )


class TwoFactorWizardAction(Div):
    def __init__(self, wizard, **kwargs):
        action = MDCButtonOutlined(
            _('Next'),
            type='submit'
        )

        super().__init__(
            action
        )


@template('registration/logged_out.html', Document, Card)
class LogoutViewComponent(Div):
    def __init__(self, *content, **context):
        super().__init__(
            H4(_('You have been logged out'), style='text-align: center'),
            Div(
                _('Thank you for spending time on our site today.'),
                cls='section'),
            Div(
                MDCButton(
                    _('Login again'),
                    tag='a',
                    href=reverse('two_factor:login')),
                style='display:flex; justify-content: center;'),
            cls='card',
            style='text-align: center'
        )


@template('registration/password_reset_form.html', Document, Card)
class PasswordResetCard(Div):
    def to_html(self, *content, view, form, **context):
        return super().to_html(
            H4(_('Reset your password'), style='text-align: center;'),
            Form(
                CSRFInput(view.request),
                form,
                MDCButton(_('Reset password')),
                method='POST',
                cls='form'),
            cls='card')


@template('registration/password_reset_confirm.html', Document, Card)
class PasswordResetConfirm(Div):
    def to_html(self, *content, view, form, **context):
        if form:
            return super().to_html(
                H4(_('Reset your password'), style='text-align: center;'),
                Form(
                    CSRFInput(view.request),
                    form,
                    MDCButton(_('confirm')),
                    method='POST',
                    cls='form'),
                cls='card')
        else:
            return super().to_html(
                H4(_('Sorry, this link as already been used'), style='text-align: center;'),
                Div(
                    _('You may go ahead and'), ' ',
                    A(_('login'), href=reverse('two_factor:login')),
                    style='text-align: center'
                ),
                cls='card')


@template('registration/password_reset_complete.html', Document, Card)
class PasswordResetComplete(Div):
    def __init__(self, **context):
        super().__init__(
            H4(_('Your password have been reset'), cls='center-text'),
            Div(
                _('You may go ahead and'), ' ',
                A(_('login'), href=reverse('two_factor:login')),
                style='text-align: center'
            ),
            cls='card')


@template('registration/password_reset_done.html', Document, Card)
class PasswordResetDoneCard(Div):
    def __init__(self, **context):
        super().__init__(
            H4(
                _('A link has been sent to your email address'),
                style='text-align:center'
            ),
            A(_('Go to login page'), href=reverse('two_factor:login')),
            cls='card',
            style='text-align: center;'
        )


@template('django_registration/registration_complete.html', Document, Card)
class RegistrationCompleteCard(Div):
    def __init__(self, **context):
        super().__init__(
            H4(_('Check your emails to finish !'), style='text-align: center'),
            Div(
                _('An activation link has been sent to your email address, '
                  'please open it to finish the signup process.'),
                style='margin-bottom: 24px'
            ),
            Div(
                _('Then, come back and login to participate'
                  ' to your election.'),
                style='margin-bottom: 24px;'
            ),
            cls='card',
            style='text-align: center'
        )


@template('django_registration/activation_complete.html', Document, Card)
class ActivationCompleteCard(Div):
    def __init__(self, **context):
        super().__init__(
            H4(
                _('Your account has been activated !'),
                style='text-align: center'
            ),
            Div(
                _('You may now'), ' ',
                A(_('login'), href=reverse('two_factor:login')),
                ' ', _('and particiapte to an election'),
                style='margin-bottom: 24px'
            ),
            cls='card',
            style='text-align: center'
        )


@template('django_registration/activation_failed.html', Document, Card)
class ActivationFailureCard(Div):
    def __init__(self, **context):
        super().__init__(
            H4(_('Account activation failure'), style='text-align: center'),
            Div(
                _('Most likely your account has already been activated.'),
                style='margin-bottom: 24px'
            ),
            cls='card',
            style='text-align: center'
        )


@template('electeez_auth/otp_login.html', Document, Card)
class OTPLoginForm(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            H4(
                _('Proceed to automatic authentification'),
                style='text-align: center'
            ),
            Form(
                CSRFInput(context['view'].request),
                MDCButton(_('Click here to continue')),
                style='display: flex; justify-content: center',
                method='post',
            ),
            cl='card',
            **context,
        )


@widget_template('captcha/widgets/captcha.html')
class CaptchaWidget(MDCField):
    @classmethod
    def from_boundfield(cls, bf, **attrs):
        ctinput = bf.field.widget
        ctinput.fetch_captcha_store('captcha', None, {'required': True, 'id': 'id_captcha'})
        context = widget_context(bf) | ctinput.get_context('captcha', None, {})

        hidden_context = context_attrs(context['subwidgets'][0])
        hidden_context['value'] = ctinput._key
        input_context = context_attrs(context['subwidgets'][1])
        return cls(
            Input(**hidden_context),
            Div(
                Span(Img(src=context['image'], id='id_captcha_img', style=dict(height='75px'))),
                Div(
                    MDCButtonOutlined('', icon='refresh', id='refresh-captcha', type='button', data_refresh_url=ctinput.refresh_url(), style='margin: auto; margin-left: 5px; margin-bottom: 4px;'),
                    MDCButtonOutlined('', icon='headphones', id='audio-captcha', tag='a', href=ctinput.audio_url(), style='margin: auto; margin-left: 5px;'),
                ),
                style=dict(
                    display='flex',
                    flex_flow='row wrap',
                    width='304px',
                    margin='auto'
                )
            ),
            MDCTextFieldOutlined(
                Input(**input_context),
                label=input_context.get('label', 'Captcha value'),
            ),
            style=dict(
                width='304px',
                margin='auto'
            ),
            **field_kwargs(bf),
        )

    def refreshCaptcha(event):
        def set_captcha_values(key, image, audio):
            getElementByUuid('id_captcha_0').value = key
            getElementByUuid('id_captcha_img').src = image
            getElementByUuid('audio-captcha').href = audio

        fetch(this.dataset.refreshUrl, {
            method: 'GET',
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        }).then(
            lambda response: response.json()
        ).then(
            lambda data: set_captcha_values(data.key, data.image_url, data.audio_url)
        )

    def playAudioCaptcha(event):
        new.Audio(this.href).play()
        event.preventDefault()

    def py2js(self):
        refreshCaptchaBtn = getElementByUuid('refresh-captcha')
        refreshCaptchaBtn.addEventListener('click', self.refreshCaptcha.bind(refreshCaptchaBtn))
        audioCaptchaBtn = getElementByUuid('audio-captcha')
        audioCaptchaBtn.addEventListener('click', self.playAudioCaptcha.bind(audioCaptchaBtn))


@template('electeez_auth/otp_send.html', Document, Card)
class OTPSendCard(Div):
    def to_html(self, *content, view, form, **context):
        self.backlink = BackLink(
            _('Back to login'),
            reverse('login')
        )
        content = super().to_html(
            H4(_('Receive a magic link by email'), style='text-align: center'),
            Form(
                form,
                CSRFInput(view.request),
                MDCButton(form.submit_label),
                method='POST',
                cls='form'),
            cls='card',)

        if view.request.GET.get('next', ''):
            content += Div(
                _('To proceed to your '),
                A(_('secure link'), href=view.request.GET['next'])
            ).to_html(**context)

        return content


@template('electeez_auth/otp_email_success.html', Document, Card)
class EmailSuccess(Div):
    def to_html(self, **context):
        self.backlink = BackLink(
            _('Back to login'),
            reverse('login')
        )

        return super().to_html(
            H4(_('If you were registered, an email has been sent to your address'), style='text-align:center'),
            cls='card',
        )


@template('profile_edit', Document, Card)
class ProfileEditCard(Div):
    def to_html(self, *content, view, form, **context):
        self.backlink = BackLink(
            _('my profile'),
            reverse('profile')
        )

        user = context.get('object')

        return super().to_html(
            H5(_('%(user)s\'s profile', user=user), style='text-align:center'),
            H6(_('Update your informations')),
            Form(
                form,
                CSRFInput(view.request),
                MDCButton(_('Update')),
                method='POST'
            )
        )


@template('profile', Document, Card)
class ProfileCard(Div):
    def to_html(self, *content, user, view, form, **context):
        self.backlink = BackLink(
            _('my elections'),
            reverse('contest_list')
        )

        organized = Contest.objects.filter(mediator=user).count()
        voter = Voter.objects.filter(user=user)
        voter_for = voter.count()
        voted_for = voter.exclude(casted_at=None).count()
        guardian_for = Guardian.objects.filter(user=user).count()
        return super().to_html(
            H5(_('%(user)s\'s profile', user=user), style='text-align:center'),
            H6(_('Account details')),
            Div(
                Table(
                    MDCDataTableTbody(
                        MDCDataTableTr(
                            MDCDataTableTd(_('Email:')),
                            MDCDataTableTd(user.email or '--'),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('First name:')),
                            MDCDataTableTd(user.first_name or '--'),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('Last name:')),
                            MDCDataTableTd(user.last_name or '--'),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('Tezos wallet address:')),
                            MDCDataTableTd(user.wallet or '--'),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('Tezos profile:')),
                            MDCDataTableTd(user.profile or '--'),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('You have organized:')),
                            MDCDataTableTd(organized, ' ', _('elections', n=organized)),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('You have been invited to be guardian of:')),
                            MDCDataTableTd(voter_for, ' ', _('elections', n=guardian_for)),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('You have been invited to vote for:')),
                            MDCDataTableTd(voter_for, ' ', _('elections', n=voter_for)),
                        ),
                        MDCDataTableTr(
                            MDCDataTableTd(_('You have voted for:')),
                            MDCDataTableTd(voted_for, ' ', _('elections', n=voted_for)),
                        ),
                    )
                ),
                MDCButtonOutlined(
                    _('Update account informations'),
                    tag='a',
                    href=reverse('profile_edit'),
                    style=dict(
                        margin='auto',
                        margin_top='32px'
                    )
                )
            ),
            H6(_('Account security')),
            Div(
                MDCButtonOutlined(
                    _('Go to account security settings'),
                    tag='a',
                    href=reverse('two_factor:profile')
                ),
                style=dict(
                    display='flex',
                    justify_content='space-around'
                )
            ),
            H6(_('Delete account')),
            Div(
                Span(_(
                    'Please be careful, you won\'t be able to recover'
                    ' your data after deleting your account'
                )),
                Div(
                    DialogConfirmForm(
                        form,
                        CSRFInput(view.request),
                        MDCButtonOutlined(
                            _('Delete my account'),
                            False,
                            style=dict(
                                width='100%',
                                height='56px'
                            )
                        ),
                        method='POST',

                    ),
                    style=dict(
                        margin='auto',
                        margin_top='32px',
                        max_width='300px'
                    ),
                    cls='red-button-container'
                )
            ),
            cls='card',
        )


@template('django_registration/activation_email_body.txt')
class EmailBody(Text):
    def __init__(self, *a, **k):
        super().__init__('')

    def to_html(self, **context):
        url = ''.join([
            settings.BASE_URL,
            reverse(
                'django_registration_activate',
                args=[context['activation_key']]
            )
        ])
        site = context['site']
        return _(
            'ACTIVATION_EMAIL_BODY',
            allow_unsecure=True,
            site_name=site.name,
            activation_url=url
        )
