from django.contrib.auth.backends import ModelBackend

from electeez_sites.models import Site


class SiteModelBackend(ModelBackend):
    def user_can_authenticate(self, user):
        site = Site.objects.get_current()
        if user.is_active and user.is_superuser:
            return True
        return user.is_active and user.siteaccess_set.filter(site=site).first()
