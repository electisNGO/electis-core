import hashlib
import json
import time
import re
import requests

import django_otp

from django import forms
from django import http
from django.db import transaction
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.views import PasswordResetView
from django.contrib.auth.views import PasswordResetConfirmView
from django.core.exceptions import ValidationError
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.views import generic
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.urls import reverse

from django_registration.backends.activation.views import RegistrationView, ActivationView
from two_factor.plugins.phonenumber.utils import backup_phones
from two_factor.utils import default_device
from two_factor.views import LoginView
from two_factor.forms import AuthenticationTokenForm
from django_otp.plugins.otp_static.models import StaticDevice

from pytezos import pytezos

from captcha.fields import CaptchaField

from electeez_common.mail import send_mail
from electeez_sites.models import Site
from djlang.utils import gettext as _
from djlang.models import mark_safe

from .models import Token, User, Beacon, IDToken
from .components import *


def user_of_site(user):
    site = Site.objects.get_current()
    return user if (
            user.is_superuser or
            user.siteaccess_set.filter(site=site).first()
        ) else None


def get_username_from_request(request):
    """ unloads username from default POST request """
    for field in ['auth-username', 'token']:
        if field in request.POST:
            return request.POST[field][:255]
    return None


class PasswordResetForm(PasswordResetForm):
    captcha = CaptchaField()

    class Meta:
        model = User
        fields = ['email']

    def get_users(self, email):
        active_users = super().get_users(email)
        return (
            u for u in active_users
            if user_of_site(u)
        )


class PasswordResetView(PasswordResetView):
    form_class = PasswordResetForm


class RegistrationView(RegistrationView):
    def get_form(self, *args, **kwargs):
        return super().get_form(*args, **kwargs)

    def create_inactive_user(self, form):
        return super().create_inactive_user(form)


class OTPSend(generic.FormView):
    template_name = 'electeez_auth/otp_send.html'

    class form_class(forms.Form):
        email = forms.EmailField()
        captcha = CaptchaField()
        submit_label = _('Send magic link')

        def clean_email(self):
            value = self.cleaned_data['email']
            self.user = User.objects.filter(
                email__iexact=value
            ).first()
            if not self.user or not user_of_site(self.user):
                raise ValidationError(
                    _('Could not find registration with email: %(email)s',
                        email=value)
                )
            return value

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'] = dict(
            email=self.request.GET.get('email', '')
        )
        return kwargs

    def form_valid(self, form):
        LINK = form.user.otp_new(
            redirect=self.request.GET.get('redirect', None)
        ).get_url()

        send_mail(
            _('Your magic link'),
            _('Hello, This is the magic link you have requested: %(link)s',
                allow_insecure=True, link=LINK),
            settings.DEFAULT_FROM_EMAIL,
            [form.cleaned_data['email']],
        )
        return http.HttpResponseRedirect(self.get_success_url())


    def form_invalid(self, form):
        if 'email' in form.errors:
            del form.errors['email']
        if 'captcha' in form.errors:
            return super().form_invalid(form)

        messages.success(self.request, _('Link sent by email'))
        return http.HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.request.GET.get(
            'next',
            reverse('otp_email_success'),
        )

class OTPEmailSuccess(generic.TemplateView):
    template_name = 'electeez_auth/otp_email_success.html'


class OTPLogin(generic.FormView):
    template_name = 'electeez_auth/otp_login.html'
    form_class = forms.Form

    def get(self, request, *args, **kwargs):
        token = Token.objects.filter(token=kwargs['token']).first()
        if not token or not user_of_site(token.user):
            return http.HttpResponseNotFound()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        token = Token.objects.filter(token=kwargs['token']).first()
        if not token or not user_of_site(token.user):
            messages.error(request, _('Invalid magic link.'))
            return http.HttpResponseRedirect(reverse('otp_send'))

        if token.used or token.expired:
            redirect = reverse('otp_send') + '?redirect=' + token.redirect
            if token.used:
                messages.success(request, _('Magic link already used.'))
                return http.HttpResponseRedirect(redirect)
            else:
                messages.success(request, _('Expired magic link.'))
                return http.HttpResponseRedirect(redirect)

        if not default_device(token.user):
            token.used = timezone.now()
            token.save()
            login(
                request, token.user, 'electeez_auth.backends.SiteModelBackend'
            )
            return http.HttpResponseRedirect(
                request.GET.get(
                    'next',
                    token.redirect or reverse('contest_list'),
                )
            )

        request.session['_2fa_token'] = token.token
        return http.HttpResponseRedirect(
            reverse('otp_2fa_login') + f'?redirect={token.redirect}'
        )


class LoginView(LoginView):
    template_name = 'two_factor/core/login.html'
    form = AuthenticationTokenForm

    def get(self, request, *args, **kargs):
        token = self.get_token()

        if not token:
            messages.success(request, _('Invalid magic link.'))
            return http.HttpResponseRedirect(reverse('otp_send'))

        if token.used or token.expired:
            redirect = reverse('otp_send') + '?redirect=' + token.redirect
            if token.used:
                messages.success(request, _('Magic link already used.'))
                return http.HttpResponseRedirect(redirect)
            else:
                messages.success(request, _('Expired magic link.'))
                return http.HttpResponseRedirect(redirect)

        self.token = token
        user = token.user
        user.backend = 'electeez_auth.backends.SiteModelBackend'

        self.storage.reset()
        self.storage.authenticated_user = user
        self.storage.data['authentication_time'] = int(time.time())
        self.storage.current_step = 'token'

        return self.render(self.get_form())

    def get_success_url(self):
        redirect_url = '/'

        token = self.get_token()
        if token:
            token.used = timezone.now()
            token.save()
            self.request.user = token.user
            self.request.session['tfa_completed'] = True
            django_otp.login(self.request, self.device)
            if token.redirect:
                redirect_url = token.redirect

        return self.request.GET.get('next', redirect_url)

    def get_token(self):
        qs_token = self.request.session.get('_2fa_token')
        if not qs_token:
            return None
        token = Token.objects.filter(token=qs_token).first()
        del self.request.session['_2fa_token']
        return token

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        token = self.get_token()
        if token:
            kwargs['user'] = token.user
            self.device = default_device(token.user)
            kwargs['initial_device'] = self.device
        else:
            self.device = kwargs['initial_device']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.storage.authenticated_user

        if 'device' not in context:
            context['device'] = default_device(user)
        context['other_device'] = [
            phone for phone in backup_phones(user)
            if phone != context['device']]

        try:
            context['backup_tokens'] = user.staticdevice_set\
                .get(name='backup').token_set.count()
        except StaticDevice.DoesNotExist:
            context['backup_tokens'] = 0

        return context


def get_wallet_details(wallet):
    apiurl = settings.TEZOS_API_URL
    account_url = f'{apiurl}v1/accounts/{wallet}/'
    response = requests.get(account_url)
    if response.status_code == 200:
        try:
            return response.json()
        except Exception:
            pass
    return None


def char2bytes(p):
    if isinstance(p, str):
        p = p.encode().hex()
    else:
        p = str(p)
    return p.encode().hex()


def encode_payload(p):
    b = char2bytes(p)
    return '0501' + char2bytes(len(b)) + b


def beacon_login(request):
    if request.method != 'POST':
        return http.HttpResponseNotAllowed(('POST',))

    data = json.loads(request.body.decode('utf-8'))

    if 'wallet' not in data or 'signature' not in data:
        return http.JsonResponse({
            'error': _('Invalid signature or token expired')
        })

    if 'publicKey' not in data:
        return http.JsonResponse({
            'error': _('PublicKey not provided')
        })

    beacon = Beacon.objects.filter(wallet=data['wallet'], used=None).first()
    if not beacon:
        return http.HttpResponseNotFound()

    if beacon.expired:
        return http.JsonResponse({
            'error': _('Invalid signature or token expired')
        })

    client = pytezos.using(
        settings.TEZOS_RPC_URL,
        key=data['publicKey']
    )

    if client.key.public_key_hash() != data['wallet']:
        return http.JsonResponse({
            'error': _('Wallet and publicKey mismatch')
        })

    try:
        if client.key.verify(data['signature'], beacon.encoded):
            user = User.objects.filter(wallet=data['wallet']).first()
            site = request.get_current_site()
            if not user:
                user = User.objects.create_user(wallet=data['wallet'])
            if not user.siteaccess_set.filter(site=site).count():
                user.siteaccess_set.create(site=site)
            beacon.used = timezone.now()
            beacon.save()
            login(
                request,
                user,
                backend='electeez_auth.backends.SiteModelBackend'
            )
            user.get_profile()
            return http.JsonResponse(dict(status='logged'))
        return http.JsonResponse({
            'error': _('Invalid signature or token expired')
        })
    except Exception as e:
        print('exception occured:', e)
        return http.JsonResponse({
            'error': _('Invalid signature or token expired')
        })


def request_beacon(request):
    if request.method != 'POST':
        return http.HttpResponseNotAllowed(('POST',))

    data = json.loads(request.body.decode("utf-8"))

    if wallet := data.get('wallet', None):
        if beacon := Beacon.objects.filter(wallet=wallet, used=None).first():
            if beacon.expired:
                beacon.renew()
            return http.JsonResponse(dict(payload=beacon.encoded))

        beacon = Beacon.objects.create(wallet=wallet)
        return http.JsonResponse(dict(payload=beacon.encoded))


class TokenAnonLoginView(generic.FormView):
    template_name = 'idtoken_login.html'

    class form_class(forms.Form):
        token = forms.CharField(
            label=_('Token ID')
        )

        def clean_token(self):
            value = self.cleaned_data['token']
            if not re.match(r'[A-Z0-9]{5}[\- ]?[A-Z0-9]{5}[\- ]?[A-Z0-9]{5}', value):
                raise forms.ValidationError(_('Invalid token'))

            return value

    def form_valid(self, form):
        token = form.cleaned_data['token']

        # remove dashed and spaces
        token = re.sub(r'[\- ]', '', token)
        # split each 5 chars and join by dash
        token_hash = token

        id_token = IDToken.objects.filter(token=token_hash).first()
        if not id_token:
            form.add_error(None, _('Credentials mismatch'))
            return self.form_invalid(form)

        if id_token.used:
            existing = User.objects.filter(email=id_token.email).first()
            login(
                self.request,
                existing,
                'electeez_auth.backends.SiteModelBackend'
            )
        else:
            try:
                with transaction.atomic():
                    new_user = User(email='anonymous@electis.io')
                    new_user.anonymise()
                    new_user.is_active = True
                    new_user.profile = token
                    new_user.save()
                    id_token.email = new_user.email
                    id_token.used = timezone.now()
                    id_token.from_qr = bool(self.request.GET.get('t', False))
                    id_token.save()
                    id_token.contest.voter_set.create(user=new_user)
                    login(
                        self.request,
                        new_user,
                        'electeez_auth.backends.SiteModelBackend'
                    )

            except Exception as e:
                raise e

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('contest_list')


class TokenLoginView(generic.FormView):
    template_name = 'registration/login.html'

    class form_class(forms.Form):
        email = forms.EmailField()
        token = forms.CharField(
            label=_('Token ID')
        )
        birthdate = forms.DateField(
            label=_('Birthdate')
        )
        consent = forms.BooleanField(
            label=_('TOKEN_LOGIN_CONSENT_LABEL'),
            required=True,
        )

        def clean_token(self):
            value = self.cleaned_data['token']
            if not re.match(r'[A-Z0-9]{9,11}', value):
                raise forms.ValidationError(_('Invalid token'))
            return value

    def get_form(self):
        form = super().get_form()
        form.initial['token'] = self.request.GET.get('t', None)
        return form

    def form_valid(self, form):
        email = form.cleaned_data['email']
        site = Site.objects.get_current()
        existing = User.objects.filter(email=email, site_access__site=site).first()
        if existing and existing.is_active:
            form.add_error('email', _('Email already used'))
            return self.form_invalid(form)

        token = form.cleaned_data['token']
        birthdate = form.cleaned_data['birthdate']
        plain_token = token + birthdate.isoformat()
        token_hash = hashlib.sha256(plain_token.encode()).hexdigest()

        id_token = IDToken.objects.filter(token=token_hash).first()
        if not id_token:
            form.add_error(None, _('Credentials mismatch'))
            return self.form_invalid(form)

        if id_token.used:
            if existing and id_token.email == existing.email:
                form.add_error('token', _('Token used'))
            elif existing:
                exsiting.delete()

        try:
            with transaction.atomic():
                new_user = User.objects.create_user(email=email)
                send_create_password_email(new_user)
                id_token.email = email
                id_token.used = timezone.now()
                id_token.from_qr = bool(self.request.GET.get('t', False))
                id_token.save()

        except Exception as e:
            raise e

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('django_registration_complete')


def send_create_password_email(user):
    subject_template_name = 'registration/password_create_subject.txt'
    email_template_name = 'registration/password_create_email.html'
    token_generator = default_token_generator
    from_email = settings.DEFAULT_FROM_EMAIL

    current_site = Site.objects.get_current()
    context = {
        'email': user.email,
        'domain': current_site.domain,
        'subject': _(
            'Password creation on %(site_name)s',
            site_name=current_site.name
        ),
        'body_header': _(
            'You\'re receiving this email because you confirmed your user account at %(site_name)s',
            site_name=current_site.name,
            allow_unsecure=True
        ).replace('\n', '<br>'),
        'body_footer': _(
            'Thanks for using our site! The %(site_name)s team',
            site_name=current_site.name,
            allow_unsecure=True
        ).replace('\n', '<br>'),
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'user': user,
        'token': token_generator.make_token(user),
        'protocol': settings.PROTO,
    }

    subject = loader.render_to_string(subject_template_name, context, using='django')
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context, using='django')

    send_mail(subject, body, from_email, [user.email])


class CreatePasswordView(PasswordResetConfirmView):
    def dispatch(self, *a, **kw):
        return super().dispatch(*a, **kw)

    def get(self, *args, **kwargs):
        self.user.is_active = True
        self.user.save()
        token = IDToken.objects.filter(email=self.user.email).first()
        if token and token.contest:
            if not token.contest.voter_set.filter(user=self.user).first():
                token.contest.voter_set.create(user=self.user)
        return super().get(*args, **kwargs)


class ProfileUpdateView(generic.UpdateView):
    template_name = 'profile_edit'
    model = User

    def dispatch(self, request, *args, **kwargs):
        self.object = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        return reverse('profile')

    class form_class(forms.ModelForm):
        class Meta:
            model = User
            fields = ['first_name', 'last_name', 'contact_email']

        def sanitize(self, value):
            if re.search(r'[@.<>\\\/:;"|?{}#()^*\[\]]', value):
                raise forms.ValidationError(_('Invalid value'))
            return value

        def clean_first_name(self):
            return self.sanitize(self.cleaned_data['first_name'])

        def clean_last_name(self):
            return self.sanitize(self.cleaned_data['last_name'])
            

class ProfileView(generic.UpdateView):
    template_name = 'profile'
    model = User

    class form_class(forms.ModelForm):
        password = forms.CharField(
            label=_('Password'),
            widget=forms.PasswordInput,
        )

        class Meta:
            model = User
            fields = ['password']

        def clean_password(self):
            """ If password does not match current user's
            password, raise ValidationError. """
            password = self.cleaned_data['password']
            if not self.instance.check_password(password):
                raise forms.ValidationError(_('Invalid password'))
            return password

        def clean(self):
            """ If user has voters or guardians in running elections, do not allow
            validation """
            filters = {
                'contest__actual_start__isnull': False,
                'contest__actual_end__isnull': True
            }
            if (self.instance.guardian_set.filter(**filters).count() or 
                self.instance.voter_set.filter(**filters).count()):
                raise forms.ValidationError(_('You cannot delete your account while elections are running'))
            return super().clean()

    def dispatch(self, request, *args, **kwargs):
        self.object = request.user
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.instance
        user.anonymise()
        return http.HttpResponseRedirect(reverse('home'))

    def get_object(self):
        return self.request.user
