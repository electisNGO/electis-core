from datetime import timedelta
import pytest

from django.core.management import call_command
from django.utils import timezone
from django.urls import reverse
from electeez_auth.models import User
from electeez_sites.models import Site



@pytest.mark.django_db
def test_otp(client, site):
    user = User.objects.create_user(email='otp@example.com')
    token = user.otp_new(redirect='valid')

    response = client.post(token.path)
    assert response['Location'] == 'valid'

    # can't use the link twice
    response = client.post(token.path)
    assert response['Location'] != 'valid'

    # try expired link
    token = user.otp_new()
    token.otp_expiry = timezone.now() - timedelta(minutes=1)
    token.save()
    response = client.post(token.path)
    assert response['Location'] != 'valid'


@pytest.mark.django_db
def test_user_create(client, site):
    user_create_url = reverse('django_registration_register')
    response = client.post(
        user_create_url,
        dict(
            email='thomas@electis.io',
            password1='123QWEQdfg@',
            password2='123QWEQdfg@'
        )

    )

    assert response.status_code == 302


@pytest.mark.django_db
def test_password_must_contain_numbers(client, site):
    user_create_url = reverse('django_registration_register')
    response = client.post(
        user_create_url,
        dict(
            email='thomas@electis.io',
            password1='asdQWEQdfg@',
            password2='asdQWEQdfg@'
        )

    )

    assert response.status_code == 200


@pytest.mark.django_db
def test_password_must_contain_capitals(client, site):
    user_create_url = reverse('django_registration_register')
    response = client.post(
        user_create_url,
        dict(
            email='thomas@electis.io',
            password1='asdqwertdfg@',
            password2='asdqwerrdfg@'
        )

    )

    assert response.status_code == 200
