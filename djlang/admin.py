from urllib.parse import parse_qsl

from django.db import models
from django.db.models import Q
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.core.paginator import EmptyPage, InvalidPage, Paginator
from django.urls import path, reverse
from django import forms, http

from .models import Language, Text


class InlineChangeList(object):
    can_show_all = True
    multi_page = True
    get_query_string = ChangeList.__dict__['get_query_string']
    search_fields = ('key', 'val', 'nval')

    def __init__(self, request, page_num, paginator):
        self.show_all = 'all' in request.GET
        self.page_num = page_num
        self.paginator = paginator
        self.result_count = paginator.count
        self.params = dict(request.GET.items())


class PaginatedTabularInline(admin.TabularInline):
    per_page = 100
    template = 'admin/edit_inline/tabular_paginated.html'
    extra = 0
    can_delete = True

    def get_formset(self, request, obj=None, **kwargs):
        formset_class = super(PaginatedTabularInline, self).get_formset(
            request, obj, **kwargs)

        class PaginationFormSet(formset_class):
            def __init__(self, *args, **kwargs):
                super(PaginationFormSet, self).__init__(*args, **kwargs)

                qs = self.queryset.order_by('pk')

                search = request.GET.get('q', '')
                if '_changelist_filters' in request.GET:
                    query_params = dict(
                        parse_qsl(request.GET['_changelist_filters'])
                    )
                    if not search:
                        search = query_params.get('q', '')

                if search:
                    qs = qs.filter(
                        Q(key__contains=search)
                        | Q(val__contains=search)
                        | Q(nval__contains=search)
                    )

                paginator = Paginator(qs, self.per_page)
                try:
                    page_num = int(request.GET.get('page', ['0'])[0])
                except ValueError:
                    page_num = 0

                try:
                    page = paginator.page(page_num + 1)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)

                self.page = page
                self.cl = InlineChangeList(request, page_num, paginator)
                self.paginator = paginator

                if self.cl.show_all:
                    self._queryset = qs
                else:
                    self._queryset = page.object_list

        PaginationFormSet.per_page = self.per_page
        return PaginationFormSet


class TextAdmin(PaginatedTabularInline):
    model = Text
    fields = ('key', 'val', 'nval')
    formfield_overrides = {
        models.TextField: {
            'widget': forms.Textarea(attrs={'rows': 4, 'cols': 40})
        }
    }


class LanguageAdmin(admin.ModelAdmin):
    model = Language
    inlines = [TextAdmin]
    list_display = ('name', 'site')
    fields = ('name', 'iso', 'site')
    search_fields = ('name', 'text__key', 'text__val', 'text__nval')
    change_list_template = 'admin/djlang_load_form.html'

    def get_readonly_fields(self, request, obj=None):
        if obj and obj.site_id == settings.DEFAULT_SITE_ID:
            return ('site',)
        else:
            return ()

    def get_urls(self):
        urls = super().get_urls()
        my_urls = path(
            'load/',
            self.admin_site.admin_view(self.load),
            name='djlang_load'
        )
        return [my_urls] + urls

    def load(self, request):
        loader = Loader()
        val, msg = loader.load(request.GET.get('q'))
        from django.contrib import messages
        if val:
            messages.error(request, msg)
        else:
            messages.success(request, msg)

        return http.HttpResponseRedirect(
            reverse('admin:djlang_language_changelist')
        )


class Loader:
    def create_text(self, **kw):
        lang = Language.objects.filter(iso=kw['language__iso']).first()
        if lang:
            print(f'creating {dict(**kw)}')
            del kw['language__iso']
            kw['language_id'] = lang.id
            Text.objects.create(**kw)

    def load(self, url):
        import json
        import requests
        try:
            res = requests.get(url)
            data = json.loads(res.content)
            for t in data:
                try:
                    t.pop('id')
                    existing = Text.objects.get(
                        language__iso=t['language__iso'],
                        key=t['key']
                    )
                    ex_dict = dict(
                        key=existing.key,
                        val=existing.val,
                        nval=existing.nval,
                        language_id=existing.language_id
                    )
                    if ex_dict == t:
                        continue
                except Text.DoesNotExist:
                    self.create_text(**t)
                except Text.MultipleObjectsReturned:
                    Text.objects.filter(
                        language__iso=t['language__iso'],
                        key=t['key']
                    ).delete()
                    self.create_text(**t)
                except Exception as e:
                    print(e)
                    return 1, e
                else:
                    existing.val = t['val']
                    existing.nval = t['nval']
                    existing.save()

            return 0, 'Lang data loaded successfully'
        except Exception as e:
            print(e)
            return 1, 'Could not load lang data'


admin.site.register(Language, LanguageAdmin)
