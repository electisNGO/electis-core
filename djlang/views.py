import json

from django.http import HttpResponse, JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import View
from django.urls import path

from electeez_sites.models import Site
from .models import Text, Language


def text_view(request):
    data = list(
        Text.objects.order_by(
            'id', 'key'
        ).distinct(
            'id', 'key'
        ).values(
            'id', 'key', 'language_id', 'language__iso', 'val', 'nval'
        )
    )
    return HttpResponse(
        json.dumps(data, ensure_ascii=False),
        content_type='application/json'
    )


class ChangePreferedLanguage(View, LoginRequiredMixin):
    def post(self, request):
        data = json.loads(request.body)
        if 'language_iso' not in data:
            return JsonResponse({'status': 'error', 'message': 'language_iso is required'})
        site = Site.objects.get_current()
        language = Language.objects.filter(iso=data['language_iso'], site=site).first()
        if not language:
            return JsonResponse({'status': 'error', 'message': 'language not found'})

        user = request.user
        if user.is_authenticated:
            user_pl = user.preferedlanguage_set.first()
            if user_pl:
                user_pl.language = language
                user_pl.save()
            else:
                user.preferedlanguage_set.create(language=language)

            user.save()

        response = JsonResponse({'status': 'ok'})
        response.set_cookie('prefered_language', language.iso)
        return response


urlpatterns = [
    path('', text_view, name='text'),
    path('prefered-language/', ChangePreferedLanguage.as_view(), name='prefered-language')
]
