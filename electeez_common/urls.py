from django import http
from django.contrib import admin, messages
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.urls import include, path, reverse
from django.utils.decorators import method_decorator
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.shortcuts import render
from django.template import RequestContext
from django.templatetags.static import static as static_url
from two_factor.urls import urlpatterns as tf_urls
from two_factor.gateways.twilio.urls import urlpatterns as tf_twilio_urls

from djlang.utils import gettext as _


urlpatterns = [
    path(
        r'^favicon\.ico$',
        generic.RedirectView.as_view(url='/static/images/favicon.ico')
    ),
]


@method_decorator(csrf_exempt, name='dispatch')
class HomeView(generic.TemplateView):
    def dispatch(self, request, *args, **kwargs):
        url = reverse('login')

        if request.user.is_authenticated:
            if request.user.is_superuser:
                url = getattr(settings, 'ADMIN_HOME', '/admin/')
            elif request.user.is_staff:
                url = getattr(settings, 'STAFF_HOME', '/admin/')
            else:
                url = reverse('contest_list')

        elif home_page := getattr(settings, 'STATIC_HOME_PAGE', None):
            url = static_url(home_page)

        elif template := getattr(settings, 'HOME_TEMPLATE', None):
            self.template_name = template
            return super().dispatch(request, *args, **kwargs)

        return http.HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        if 'inputEmail' in request.POST:
            messages.success(
                request,
                _('You have successfully subscribed to %(app)s mailing list',
                    app=_('Neuilly Vote'))
            )
        return self.get(request, *args, **kwargs)


urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('admin/defender/', include('defender.urls')),
    path('accounts/', include('electeez_auth.urls')),
    path('contest/', include('djelectionguard.urls')),
    path('tezos/', include('djelectionguard_tezos.views')),
    path('lang/', include('djlang.views')),
    path('2fa/', include(tf_urls)),
    path('twilio/', include(tf_twilio_urls)),
    path('social_2fa/', include("social_2fa.urls")),
    path('captcha', include('captcha.urls')),
    path('', HomeView.as_view(), name='home'),
)


class LegalView(generic.TemplateView):
    template_name = 'legal'


class DataPolicyView(generic.TemplateView):
    template_name = 'policy'


class CookieView(generic.TemplateView):
    template_name = 'cookies'


class GTUView(generic.TemplateView):
    template_name = 'gtu'


urlpatterns += i18n_patterns(
    path('legal/', LegalView.as_view(), name='legal'),
    path('policy/', DataPolicyView.as_view(), name='policy'),
    path('cookies/', CookieView.as_view(), name='cookies'),
    path('gtu/', GTUView.as_view(), name='gtu'),
)


if settings.DEBUG:
    urlpatterns.append(
        path('bundles/', include('ryzom_django.bundle')),
    )


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns('/static/')
urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]


def exception_handler(status, template, msg, submsg):
    def handler(request, exception=None, reason=''):
        RequestContext(request)
        response = render(
            request,
            template,
            status=status,
            context=dict(
                request=request,
                status=status,
                view=None,
                err_code=status,
                message=msg,
                submessage=submsg,
            ),
        )
        response.status_code = status
        return response

    return handler


handle_exc_400 = exception_handler(
    400, '400.html', _('Bad request'),
    _('Sorry, the request send to our server was malformed.')
)
handle_exc_403 = exception_handler(
    403, '403.html', _('Permission denied'),
    _('Sorry, you are not allowed to access this page.')
)
handle_exc_404 = exception_handler(
    404, '404.html', _('Page not found'),
    _('Sorry, the requested page was not found on this server.')
)
handle_exc_500 = exception_handler(
    500, '500.html', _('Server error'),
    _('Sorry, an error occured, it has been reported to our services.')
)

handler400 = 'electeez_common.urls.handle_exc_400'
handler403 = 'electeez_common.urls.handle_exc_403'
handler404 = 'electeez_common.urls.handle_exc_404'
handler500 = 'electeez_common.urls.handle_exc_500'
