from django.conf import settings

from redis.client import StrictRedis

from celery import shared_task
from celery.utils.log import get_task_logger
import json
    
logger = get_task_logger(__name__)

from django.core.management import call_command


redis_client = StrictRedis.from_url(settings.REDIS_SERVER)


def get_command_key(command):
    return f"{command}_is_running"

def is_running(command):
    return int(redis_client.get(get_command_key(command)) or 0)

def set_running(command):
    redis_client.set(get_command_key(command), 1)

def end_running(command):
    redis_client.set(get_command_key(command), 0)

def call_command_cron(command: str):
    print(f'Calling commang {command}')
    if not is_running(command):
        set_running(command)
        try:
            logger.debug(f"Starting {command}")
            call_command(command)
        except Exception as e:
            pass
        end_running(command)
        logger.debug(f"Finishing {command}")
    else:
        print(f'{command} is already running')

@shared_task
def djtezos_sync():
    call_command_cron("djtezos_sync")

@shared_task
def djtezos_write():
    call_command_cron("djtezos_write")

@shared_task
def djtezos_balance():
    call_command_cron("djtezos_balance")

@shared_task
def consent():
   call_command_cron("update_consent")

@shared_task
def tally():
    call_command_cron("tally")

@shared_task
def start_ceremony():
    call_command_cron("start_ceremony")

@shared_task
def open_contest():
    call_command_cron('open_contest')

@shared_task
def close_contest():
    call_command_cron('close_contest')
