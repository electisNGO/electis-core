import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from django.core.asgi import get_asgi_application

from ryzom_django_channels.consumers import Consumer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'electis.settings')

django_asgi_app = get_asgi_application()

application = ProtocolTypeRouter({
    # Django's ASGI application to handle traditional HTTP requests
    'http': django_asgi_app,

    'websocket': AuthMiddlewareStack(
        URLRouter([
            path("ws/ddp/", Consumer.as_asgi()),
        ])
    ),
})
