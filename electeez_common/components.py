from django.conf import settings
from django.contrib import messages
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse

from sass_processor.processor import sass_processor

from ryzom.contrib.django import Static
from ryzom_django_mdc import html
from ryzom_django_mdc.html import *

from py2js.renderer import autoexec

from djlang.models import Language, flags_dict
from djlang.utils import gettext as _

from electeez_sites.models import Site


class DButton(Button):
    def onclick(event):
        def set_clickable(elem):
            elem.dataset.clicked = ''

        if not event.target or not event.target.closest:
            return

        elem = event.target.closest('a, button')

        if not elem:
            return

        if elem.dataset.clicked == 'true':
            event.preventDefault()
            event.stopPropagation()
        else:
            elem.dataset.clicked = 'true'
            setTimeout(set_clickable, 1000, elem)

    def py2js(self):
        self.addEventListener('click', self.onclick)


class MDCTextButton(DButton):
    def __init__(self, text, icon=None, **kwargs):
        content = [Span(cls='mdc-button__ripple')]
        if icon:
            content.append(MDCIcon(icon))
        content.append(Span(text, cls='mdc-button__label'))
        super().__init__(
            *content,
            cls='mdc-button',
            **kwargs,
        )


class MDCButtonOutlined(DButton):
    def __init__(self, text, p=True, icon=None, **kwargs):
        black = 'black-button' if p else ''
        content = [Span(cls='mdc-button__ripple')]
        if icon and isinstance(icon, str):
            content.append(MDCIcon(icon))
        elif icon:
            content.append(icon)
        content.append(Span(text, cls='mdc-button__label'))
        super().__init__(
            *content,
            cls=f'mdc-button mdc-button--outlined {black}',
            **kwargs
        )


class MDCButton(DButton):
    def __init__(self, text=None, p=True, icon=None, **kwargs):
        black = 'black-button' if p else ''
        kwargs.setdefault('raised', True)

        if icon and isinstance(icon, str):
            content = [MDCIcon(icon)]
        elif icon:
            content = [icon]
        else:
            content = []

        if text:
            content.append(Span(text, cls='mdc-button__label'))

        raised = 'mdc-button--raised' if kwargs.pop('raised', None) else ''
        super().__init__(
            *content,
            cls=f'mdc-button {raised} {black}',
            **kwargs
        )


class DialogConfirmForm(Form):
    def __init__(self, *content, **attrs):

        actions = MDCDialogActions(
            MDCDialogCloseButtonOutlined(_('cancel')),
            MDCDialogAcceptButton(
                _('confirm'),
                addcls='mdc-button--raised',
                style=dict(
                    color='white',
                    background='var(--danger)'
                )
            ),
            style={
                'display': 'flex',
                'justify-content': 'space-around',
            },
            addcls='red-button-container'
        )

        super().__init__(
            *content,
            MDCDialog(
                _('Confirm your choice'),
                Div(
                    _(
                        'Be careful, this will definitively delete your account.'
                    ),
                ),
                actions=Div(
                    actions,
                    Div(
                        Span(id='remaining'),
                        style=dict(
                            background='aliceblue',
                            text_align='center',
                            padding='12px',
                            margin='24px',
                            margin_top='0'
                        ),
                    ),
                ),
            ),
            **attrs
        )

    def ondialogclosing(event):
        if event.detail.action == 'accept':
            this.form.submit()

    def handle_submit(event):
        event.preventDefault()
        this.dialog = this.querySelector('mdc-dialog')
        this.dialog.onclosing = self.ondialogclosing
        this.dialog.form = this
        this.dialog.open()

    def py2js(self):
        form = getElementByUuid(self.id)
        form.addEventListener('submit', self.handle_submit.bind(form))


class MDCLinearProgress(html.Div):
    def __init__(self, **kwargs):
        super().__init__(
            html.Div(
                html.Div(cls='mdc-linear-progress__buffer-bar'),
                cls='mdc-linear-progress__buffer'),
            html.Div(
                html.Span(cls='mdc-linear-progress__bar-inner'),
                cls='mdc-linear-progress__bar mdc-linear-progress__primary-bar'
            ),
            html.Div(
                html.Span(cls='mdc-linear-progress__bar-inner'),
                cls='mdc-linear-progress__bar '
                    'mdc-linear-progress__secondary-bar'
            ),
            role='progressbar',
            cls='mdc-linear-progress',
            data_mdc_auto_init='MDCLinearProgress',
            aria_label='Contract deployment progress',
            aria_valuemin=0,
            aria_valuemax=1,
            **kwargs
        )


class PreferedLanguageWidget(html.Div):
    sass = """
.prefered-language-widget
    form
        .mdc-select--outlined
            .mdc-notched-outline
                display: none
            .mdc-select__anchor
                height: 36px
                width: auto
                padding: 0 8px
                .mdc-select__dropdown-icon
                    display: none
                .mdc-select__selected-text
                    font-family: 'BabelStone Flags'
            .mdc-menu
                min-width: 64px
                .mdc-list
                    .mdc-list-item
                        font-family: 'BabelStone Flags'
                        justify-content: center
"""
    def __init__(self, request, **kwargs):
        default = 'fr'
        site = Site.objects.get_current()
        if lang := Language.objects.filter(site=site, default=True).first():
            default = lang.iso

        if request and request.COOKIES.get('prefered_language'):
            default = request.COOKIES['prefered_language']

        if request and request.user.is_authenticated:
            if pl := request.user.preferedlanguage_set.first():
                default = pl.language.iso

        super().__init__(
            html.Form(
                CSRFInput(request),
                html.MDCSelectOutlined(
                    optgroups=[[
                        None,
                        [
                            dict(index=i, label=flags_dict[iso], value=iso)
                            for i, (iso, lang) in enumerate(settings.LANGUAGES)
                        ],
                        0
                    ]],
                    label='',
                    name='iso',
                    value=[default]
                ),
                action=reverse('prefered-language'),
                method='post',
            ),
            cls='prefered-language-widget',
        )

    class HTMLElement:
        def connectedCallback(self):
            window.addEventListener('load', self.init.bind(self))

        def init(self):
            this.select = this.querySelector('.mdc-select')
            this.form = this.select.closest('form')
            this.select.addEventListener('MDCSelect:change', this.onchange.bind(this))

        async def onchange(self, event):
            response = await fetch(
                this.form.action,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'X-CSRFToken': this.form.querySelector('input[name="csrfmiddlewaretoken"]').value
                    },
                    body: JSON.stringify({'language_iso': event.detail.value})
                }
            )
            if response.ok:
                res = await response.json()
                if res.status == 'ok':
                    location.reload()


class TopPanel(html.Div):
    def __init__(self, request=None, **kwargs):
        self.user = user = request.user if request else None

        if user and user.is_authenticated:
            user = A(
                user.display_name,
                href=reverse('profile')
            )
            text = _('Hello,')
            view = kwargs.get('view', None)
            if view and view.__class__.__name__ == 'SetupView':
                account_btn = None
            else:
                account_btn = Form(
                    CSRFInput(request),
                    MDCButton(_('log out'), type='submit', style=dict(width='120px')),
                    action=reverse('logout'),
                    method='post',
                )
        else:
            text = ''
            view = kwargs.get('view', None)
            if view and view.__class__.__name__ == 'LoginView':
                url = reverse('django_registration_register')
                if Site.objects.get_current().registration_allowed:
                    account_btn = MDCButton(
                        _('sign up'),
                        tag='a',
                        href=url,
                        style=dict(width='120px')
                    )
                else:
                    account_btn = None
            else:
                account_btn = MDCButton(
                    _('log in'),
                    tag='a',
                    href=reverse('two_factor:login'),
                    style=dict(width='120px')
                )

        super().__init__(
            html.Span(
                html.A(
                    _('Contest list'),
                    href='/',
                    title='Contest list',
                    cls='top-panel-sub top-panel-logo',
                ),
                cls='link',
                style='width:180px; height:50px',
            ),
            html.Span(
                html.Span(text, ' ', user, cls='top-panel-sub top-panel-msg'),
                cls='top-panel-elem over'),
            html.Span(PreferedLanguageWidget(request), account_btn, cls='top-panel-elem top-panel-btn'),
            html.Span(
                html.Span(text, cls='top-panel-sub top-panel-msg'),
                cls='top-panel-elem under'),
            cls='top-panel')


class Footer(html.Div):
    def __init__(self):
        super().__init__(
            html.Div(style='height:96px'),
            html.Div(
                html.Div(
                    html.Img(
                        src=Static('images/electis.svg'),
                        alt='electis-logo',
                        aria_hidden='true',
                        style=dict(
                            height='50px',
                            margin='0 auto',
                        )
                    ),
                    style=dict(
                        height='100px',
                        overflow='hidden',
                        display='block',
                        margin='auto',
                    ),
                    title='Electis website',
                    tag='a',
                    href=Site.objects.get_current().footer_url
                ),
                Br(),
                Div(
                    html.A(_('Privacy policy'), href='/policy/', cls='caption'),
                    html.A(_('legal notice'), href='/legal/', cls='caption'),
                    html.A(_('Cookies'), href='/cookies/', cls='caption'),
                    html.A(_('GTU'), href='/gtu/', cls='caption'),
                    html.A(_('Helpdesk'), href=settings.HELPDESK_URL, cls='caption'),
                ),
                cls='footer'
            )
        )


class BackLink(html.Div):
    def __init__(self, text, url):
        super().__init__(
            html.Div(
                MDCTextButton(
                    text,
                    'chevron_left',
                    tag='a',
                    href=url),
                cls='card backlink'),
            cls='card backlink')


class Messages(html.CList):
    def __init__(self, request):
        msgs = messages.get_messages(request)
        lvl = messages.DEFAULT_LEVELS
        rlvl = {lvl[k]: k.lower() for k in lvl.keys()}
        if msgs:
            super().__init__(*(
                MDCSnackBar(msg.message, delay=i * 5000)
                for i, msg in enumerate(msgs)
            ))
        else:
            super().__init__()


class Card(html.Main):
    def __init__(self, main_component, **context):
        self.main_component = main_component
        super().__init__(main_component, cls='card')

    def to_html(self, **context):
        main_html = self.main_component.to_html(**context)

        if backlink := getattr(self.main_component, 'backlink', None):
            self.backlink = backlink

        return super().to_html(main_html, **context)


class Body(html.Body):

    footer_class = Footer

    def __init__(self, main_component, **context):
        self.main_component = main_component
        super().__init__(**context)

    def to_html(self, **context):
        banner_url = '/static/branding.png'
        site = Site.objects.get_current()
        if site.banner_url:
            banner_url = site.banner_url.url

        self.content += [
            html.Style(
                ':root {',
                f'  --mdc-theme-primary: {site.primary_color};',
                f'  --mdc-theme-secondary: {site.primary_color};',
                f'  --black2: {site.secondary_color};',
                '}',
                '.top-panel {',
                f'  background-color: {site.banner_color};',
                '}',
                '.top-panel .top-panel-logo {',
                mark_safe(f'  background-image: url({banner_url});'),
                '}',
                '.top-panel .top-panel-msg {',
                f'   color: {site.banner_text_color};',
                '}',
                '.top-panel .top-panel-msg a {',
                f'   color: {site.banner_link_color};',
                '}',
                'body {',
                f'  background-color: {site.background_color};',
                '}',
                '.footer {',
                f'  background-color: {site.footer_color};',
                '}',
                '.footer a {',
                f'  color: {site.footer_link_color};',
                '}',

            ),
            TopPanel(**context)
        ]

        main_html = self.main_component.to_html(**context)

        if backlink := getattr(self.main_component, 'backlink', None):
            self.content += [backlink]

        messages = 'request' in context and Messages(context['request'])

        self.content += [
            main_html,
            messages or None,
            self.footer_class()
        ]
        return super().to_html(**context)


class Document(html.Html):
    title = _('Secure elections with homomorphic encryption')
    body_class = Body

    def to_html(self, head, body, **context):
        if settings.DEBUG:
            common_style_src = sass_processor('css/common.scss')
            style_src = sass_processor('css/style.scss')
        else:
            common_style_src = staticfiles_storage.url('css/common.css')
            style_src = staticfiles_storage.url('css/style.css')

        site = Site.objects.get_current()
        title = _('Secure elections with homomorphic encryption')
        head.addchild(html.Meta(name='description', content=str(title)))
        head.addchild(html.Meta(name='theme-color', content=site.banner_link_color))
        head.addchild(html.Stylesheet(href=common_style_src))
        head.addchild(html.Stylesheet(href=style_src))
        head.addchild(html.Stylesheet(href='https://fonts.googleapis.com/css?family=Rubik'))
        head.addchild(html.Script(src=staticfiles_storage.url('index-bundle.js')))

        return super().to_html(head, body, **context)


@template('policy', Document, Card)
class Policy(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Privacy policy'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(Markdown(str(_('POLICY_TEXT')))),
                cls='info-panel',
            )
        )


@template('cookies', Document, Card)
class Cookies(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Cookies'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(Markdown(str(_('COOKIES_TEXT')))),
                cls='info-panel',
            )
        )


@template('legal', Document, Card)
class Legal(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('Legal notice'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(Markdown(str(_('LEGAL_TEXT')))),
                cls='info-panel',
            )
        )


@template('gtu', Document, Card)
class GTU(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            Div(
                H4(
                    _('General Terms of Use'),
                    style=dict(
                        text_align='center'
                    )
                ),
                Div(Markdown(str(_('GTU_TEXT')))),
                cls='info-panel',
            )
        )


@template('400.html', Document, Card)
@template('403.html', Document, Card)
@template('403_csrf.html', Document, Card)
@template('404.html', Document, Card)
@template('500.html', Document, Card)
class ErrorTemplate(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            H4(context['message'], style=dict(text_align='center')),
            I(context['submessage'], style=dict(
                display='block',
                text_align='center'
            )),
            **context
        )


@template('403_csrf.html', Document, Card)
class CRSFErrorTemplate(Div):
    def to_html(self, *content, **context):
        return super().to_html(
            H4(_('An error occured'), style=dict(text_align='center')),
            I(_('Maybe you logged in in another window?'), style=dict(
                display='block',
                text_align='center'
            )),
            **context
        )
